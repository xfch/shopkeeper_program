//app.js
var md5 = require('./utils/MD5.js')
var config = require('./config.js')
App({
    shopno: '', //店铺编号
    shopnoMain: '',//总店铺编号
    tableId: '',//桌牌号
    pagesize: '10', // 每页条数
    filePath: config.FilePath, //图片路径
    openid: '',
    onLaunch: function() {
        // 获取用户信息
        wx.getSetting({
            success: res => {
                if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                        success: res => {
                            // 可以将 res 发送给后台解码出 unionId
                            this.globalData.userInfo = res.userInfo
                            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                            // 所以此处加入 callback 以防止这种情况
                            if (this.userInfoReadyCallback) {
                                this.userInfoReadyCallback(res)
                            }
                        }
                    })
                }
            }
        })
    },
    api(method, str, params, callback, errHandler, comBack) {
        const C_SHOP_NO = wx.getStorageSync('C_SHOP_NO')
        // shopNo
        if (!params) {
            params = {};
        }
        if (!params.ShopNo) {
            params.ShopNo = this.shopno || '100010'
        }
        if (params.ShopNo == ''){
            delete params.ShopNo
        }
        var md5m = "";
        if (str) {
            let arr = str.split("&");
            let va = []
            for (let i = 0; i < arr.length; i++) {
                va.push(params[arr[i]])
            }
            md5m = arr.join('&') + '=' + va.join('*')
        } else {
            var arr = []
            var va = []
            for (var k in params) {
                if (k = "bNo") continue;
                arr.push(k);
                var v = params[k];
                va.push(v ? v : '');
            }
            md5m = arr.join("&") + "=" + va.join("*");
        }
        // console.log(md5m)
        let md5s = md5.md5(md5m);
        params.MD5 = md5s.toUpperCase()
        let url = params.bNo == 'Inn011' ? config.httpUrlGetOpenid : config.requestUrl;
        wx.request({
            url: url,
            data: params,
            method: method,
            header: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'accept': 'application/json'
            },
            success: (res) => {
                // console.log('bNo-res: ' + params.bNo)
                // console.log(res)
                if (typeof res.data == "string" && res.data != "") {
                    res.data = res.data.replace("(", "");
                    res.data = res.data.replace(")", "");
                    res.data = JSON.parse(res.data)
                }
                if (res.data.code == '100' || res.data.code == '101') {
                    if (callback) {
                        callback(res.data);
                    }
                } else {
                    if (errHandler) {
                        errHandler(res.data);
                    }
                }

            },
            fail: err => {
                // console.log('bNo-err: ' + params.bNo)
                // console.log(err)
                if (err.errMsg == "request:fail timeout") {
                    Taro.hideLoading()
                    Taro.showToast({
                        title: '请求超时！',
                        icon: 'none'
                        // image: "/images/warn.png"
                    })
                }
            },
            complete: com => {
                if (comBack) {
                    comBack(com)
                }
            }
        })
    },
    apiPost(str, params, callback, errHandler, comBack){
        this.api('POST', str, params, callback, errHandler, comBack)
    },
    apiGet(str, params, callback, errHandler, comBack){
        this.api('GET', str, params, callback, errHandler, comBack)
    },
    globalData: {
        userInfo: null,
        scan: false,
        version: "2.3.044"
    }
})