var util = require("MD5.js");
var rqeuest = require("request.js");

var goodsControllor = {

  getGoodsDetail(config, call) {
    var str = `gid&ShopNo=${config.gid}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 4008,
      gid: config.gid,
      isGoodsImg: "0",
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    rqeuest(data, data => {
      call(data)
    })
  },

  getGoodsDetail701(config, call) {
    var str = `gid&ShopNo=${config.gid}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 701,
      gid: config.gid,
      isGoodsImg: "0",
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      isactive: 1,
      isgroupshop: config.isgroupshop || "",
      N_LPTCL: config.N_LPTC || "",
      MD5: md5.toUpperCase()
    };

    rqeuest(data, data => {
      call(data)
    })
  },

  getGoodsDetailAttr(config, call) {
    var str = `Spv1&GbId&ShopNo=${config.Spv1}*${config.GbId}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 4009,
      GbId: config.GbId,
      goodsattr: "",
      Spv1: config.Spv1,
      Spv2: config.Spv2,
      Spv3: config.Spv3,
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };
    rqeuest(data, data => {
      call(data)
    }, "GET")
  },

  getGoodsByCateid(config, call) {
    config.name = config.name ? config.name : "";
    var str = `pageindex&pagesize&cid&name&sort&desc=${config.pageindex}*${config.pagesize}*${config.cid}*${config.name}*t.ID desc*`;
    var md5 = util.md5(str);
    var data = {
      bNo: "702b",
      pageindex: config.pageindex,
      pagesize: config.pagesize,
      cid: config.cid,
      name: config.name,
      sort: "t.ID desc",
      desc: "",
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase(),
    }

    rqeuest(data, data => {
      call(data)
    })
  },

  /**
   * 获取服务员视角的商品列表
   */
  getGoodsByCateidWaiter(config, call) {
    var str = `ShopNo&pageindex&pagesize&CateId=${config.ShopNo}*${config.pageindex}*${config.pagesize}*${config.cid}`;
    var md5 = util.md5(str);
    var data = {
      bNo: "4005",
      pageindex: config.pageindex,
      pagesize: config.pagesize,
      mebno: config.mebno,
      CateId: config.cid,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    }

    rqeuest(data, data => {
      call(data)
    })
  },

  getZtInfo(config, call) {
    var str = `TID&ShopNo=${config.TID}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2212,
      TID: config.TID,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    rqeuest(data, data => {
      call(data)
    })
  },

  getGoodsOtherInfo(c, call) {
    var str = `GoodsID&ShopNo=${c.GoodsID}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2210,
      GoodsID: c.GoodsID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    rqeuest(data, data => {
      call(data)
    })
  },
};

module.exports = goodsControllor;