const util = require("MD5.js");
let url = "";
let ShopNo = "";

wx.getExtConfig({
  success: res => {
    url = res.extConfig.rquestUrl;
    ShopNo = res.extConfig.ShopNo;
  }
})

var req = {
  /**
   * 查询商品数据
   */
  getGoodsList(config, callback) {
    let pageindex = config.pageindex || "";
    let ShopNo = config.ShopNo;
    let pagesize = config.pagesize || "";
    let name = config.name || "";
    let cid = config.cids || "";
    let mebno = config.mebno || "";
    let str = `pageindex&pagesize&cid&name&sort&desc=${pageindex}*${pagesize}*${cid}*${name}*t.ID desc*`;
    console.log(str)
    let md5 = util.md5(str);
    let data = {
      bNo: "702b",
      pageindex,
      pagesize,
      cid,
      name: "",
      sort: "t.ID desc",
      desc: "",
      ShopNo,
      mebno,
      MD5: md5.toUpperCase(),
    }

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  /**
   * 添加购物车
   */
  addCart(config, callback) {
    let str = "";
    if (config.mkid && config.mkcid) {
      str = `gid&num&goodsattr&mkid&mkcid&mebno&ShopNo=${config.gid}*${config.num}**${config.mkid}*${config.mkcid}*${config.mebno}*${config.ShopNo}`;
    } else {
      str = `gid&num&goodsattr&mebno&ShopNo=${config.gid}*${config.num}**${config.mebno}*${config.ShopNo}`;
    }
    let md5 = util.md5(str);
    let data = {
      bNo: 800,
      goodsattr: "",
      gid: config.gid,
      num: config.num,
      mkid: config.mkid,
      mkcid: config.mkcid,
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  getGoodsDetail(config, callback) {
    let str = `gid&ShopNo=${config.gid}*${config.ShopNo}`;
    let md5 = util.md5(str);
    let data = {
      bNo: 701,
      gid: config.gid,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  getLvggGoodsDetail(config, callback) {
    let str = `Spv1&GbId&ShopNo=${config.Spv1}*${config.GbId}*${config.ShopNo}`;
    let md5 = util.md5(str);
    let data = {
      bNo: "701b",
      GbId: config.GbId,
      Spv1: config.Spv1,
      Spv2: config.Spv2,
      Spv3: config.Spv3,
      ShopNo: config.ShopNo,
      mebno: config.mebno,
      isGoodsImg: -1,
      MD5: md5.toUpperCase()
    }

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  /**
   * 编辑购物车
   */
  editCart(config, callback) {
    let str = `mebno&gid&num&mkid&mkcid&ShopNo=${config.mebno}*${config.gid}*${config.num}*${config.mkid}*${config.mkcid}*${config.ShopNo}`;
    let md5 = util.md5(str);
    let data = {
      bNo: 801,
      mebno: config.mebno,
      gid: config.gid,
      num: config.num,
      mkid: config.mkid,
      mkcid: config.mkcid,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  delCart(config, callback) {
    let str = `cartids&ShopNo=${config.cartids}*${config.ShopNo}`;
    let md5 = util.md5(str);
    let data = {
      bNo: 803,
      cartids: config.cartids,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data)
      }
    })
  },

  /**
   * 查询购物车
   * config.mebno => 会员编号
   * config.ShopNo => 店铺编号
   */
  queryCart(config, callback) {
    var str = `mebno&ShopNo=${config.mebno}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: "802",
      mebno: config.mebno,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  /**
   * 查询808中的所有购物车信息
   * 
   */
  queryTotalInfo(config, callback) {
    var str = `mebno&cids&ShopNo=${config.mebno}*${config.cids}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 808,
      mebno: config.mebno,
      cids: config.cids,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    }

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data);
      }
    })
  },

  getDefaultSend(config, callback) {
    let str = `ShopNo=${config.ShopNo}`;
    let md5 = util.md5(str);
    let data = {
      bNo:901,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.url,
      data,
      success: res => {
        callback(res.data)
      }
    })
  },
}

module.exports = req;