var config = require("../config.js")
var util = require("MD5.js");
var req = require("reqGet.js");

var url = config.requestUrl;

var shopNo = "";
var opInfo = {};
wx.getStorage({
  key: 'C_SHOP_NO',
  success: res => {
    shopNo = res.data
  },
})

wx.getStorage({
  key: 'OP_INFO',
  success: function(res) {
    opInfo = res.data;
  },
})

var coupon = {
  /**
   * 获取店铺优惠券信息
   * param mebno 会员编号
   * param call 执行请求之后的回调函数
   */
  getCoupon(mebno, call) {
    var str = `mebno&gids&state&istask&ShopNo=${mebno}**0*2*${shopNo}`;
    var md5 = util.md5(str);
    req({
      bNo: 603,
      mebno,
      gids: "",
      state: 0,
      istask: 2,
      ShopNo: shopNo,
      MD5: md5.toUpperCase()
    }, (data) => {
      call(data);
    })
  },

  /**
   * 赠送优惠券
   */
  sendCoupon(mebno, mcids, call) {
    req({
      bNo: 1217,
      mebno,
      mcids,
      opname: opInfo.c_name,
      opid: opInfo.n_ss_id,
      ShopNo: shopNo,
    }, (data) => {
      call(data);
    })
  }
}

module.exports = coupon;