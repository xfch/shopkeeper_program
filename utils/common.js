var config = require("../config.js")
var util = require("MD5.js")
var request = require("reqGet.js");

var url = config.requestUrl;

var shopNo = "";
wx.getStorage({
  key: 'C_SHOP_NO',
  success: res => {
    shopNo = res.data
  },
})

var func = {
  recharge(rechargeAmount, rechargeSendAmount, rechargeSendScore, rshopNo, mebno, call) {
    var str = `mebno&chargeamount&zsje&zsjf&ShopNo=${mebno}*${rechargeAmount}*${rechargeSendAmount}*${rechargeSendScore}*${rshopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 410,
      mebno,
      chargeamount: rechargeAmount,
      zsje: rechargeSendAmount,
      zsjf: rechargeSendScore,
      ShopNo: rshopNo,
      MD5: md5.toUpperCase()
    };
    console.log(str);
    console.log(data)
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.code === "200") {
          call(res.data);
        }
      }
    })
  },

  resetPassword(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        }
      }
    })
  },

  clearCard(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        } else {
          wx.showModal({
            title: '提示',
            content: '清卡失败，请重试',
          })
        }
      }
    })
  },

  delMember(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        } else {
          wx.showModal({
            title: '提示',
            content: '删除失败，请重试',
          })
        }
      }
    })
  },

  lossReport(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        } else {
          wx.showModal({
            title: '提示',
            content: '挂失失败，请重试',
          })
        }
      }
    })
  },

  createCard(con, call) {
    wx.request({
      url,
      data: con,
      success: res => {
        if (res.data.Result != "") {
          call(res.data.Result);
        } else {
          wx.showModal({
            title: '提示',
            content: '生成失败，请重试',
          })
        }
      }
    })
  },

  issueCard(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        } else {
          wx.showModal({
            title: '提示',
            content: '补卡失败，请重试',
          })
        }
      }
    })
  },

  memberDelay(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.Result == 1) {
          call();
        } else {
          wx.showModal({
            title: '提示',
            content: '延期失败，请重试',
          })
        }
      }
    })
  },
}

module.exports = func;