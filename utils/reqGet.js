var config = require("../config.js")
var util = require("MD5.js")

var url = config.requestUrl;

var shopNo = "";
wx.getStorage({
  key: 'C_SHOP_NO',
  success: res => {
    shopNo = res.data
  },
})

var request = function(data, call) {
  wx.request({
    url,
    data,
    success: res => {
      if (res.data.code === "100") {
        call(res.data)
      } else {
        call(res.data)
      }
    }
  })
}

module.exports = request;