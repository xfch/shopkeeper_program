var util = require("MD5.js");
var con = require("../config.js")
var url = con.requestUrl;

var table = {

  /**
   * 获取桌台信息
   * 
   */
  getTableInfo(config, call) {
    config.RID = config.RID || 0; // 如果没有值，设置默认值，下同
    config.State = config.State || 10;
    var str = `RID&State&ShopNo=${config.RID}*${config.State}*${config.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2200,
      RID: config.RID,
      State: config.State,
      ShopNo: config.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  _Urging_vegetables(c, call) {
    var str = `OrderNo&ShopNo=${c.OrderNo}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2215,
      OrderNo: c.OrderNo,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  wake_up(c, call) {
    var str = `OrderNo&ShopNo=${c.OrderNo}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2211,
      OrderNo: c.OrderNo,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  preOrder(c, call) {
    var str = `OrderNo&ShopNo=${c.OrderNo}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2214,
      OrderNo: c.OrderNo,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  mimeograph(c, call) {
    var str = `OrderNo&ShopNo=${c.OrderNo}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2216,
      OrderNo: c.OrderNo,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  serve_up(c, call) {
    var str = `OrderNo&ShopNo=${c.OrderNo}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2213,
      OrderNo: c.OrderNo,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  clearTable(c, call) {
    var str = `TableID&ShopNo=${c.TableID}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2209,
      TableID: c.TableID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  getOtherTable(c, call) {
    var str = `tid&ShopNo=${c.tid}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2205,
      tid: c.tid,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };
    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  /**
   * 换桌
   */
  _change_table(c, call) {
    var str = `TableID&HZTableID&ShopNo=${c.TableID}*${c.HZTableID}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2208,
      TableID: c.TableID || "",
      HZTableID: c.HZTableID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };
    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  /**
   * 拼桌
   */
  _add_table(c, call) {
    var str = `OldTableID&TableIDs&ShopNo=${c.TableID}*${c.HZTableID}*${c.ShopNo}`;

    var md5 = util.md5(str);
    var data = {
      bNo: 2206,
      OldTableID: c.TableID || "",
      TableIDs: c.HZTableID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };
    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  _wake_up(c, call) {

  },

  /**
   * 查询桌台详细信息
   */
  queryTable(c, call) {
    var str = `TID&ShopNo=${c.TID}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2207,
      tid: c.TID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };
    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  },

  /**
  * 查询桌台详细信息
  * 用户扫码点餐时
  */
  queryTableUser(c, call) {
    var str = `tid&ShopNo=${c.TID}*${c.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2220,
      tid: c.TID,
      ShopNo: c.ShopNo,
      MD5: md5.toUpperCase()
    };
    wx.request({
      url,
      data,
      success: res => {
        call(res.data)
      }
    })
  }
}

module.exports = table;