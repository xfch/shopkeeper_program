var config = require("../config.js")
var util = require("MD5.js")

var url = config.requestUrl;

var shopNo = "";
wx.getStorage({
  key: 'C_SHOP_NO',
  success: res => {
    shopNo = res.data
  },
})

var goods = {
  getGoodsClassify(data, call) {
    requestCall(data, (res) => {
      call(res.Result);
    })
  },

  /**
   * 获取商品列表
   */
  getGoodsList(con, call) {
    requestCall(con, (res) => {
      call(res);
    })
  },

  filterPrice(str) {
    str = str.replace(/[^\d^\.]/g, "")
    if (str.indexOf(".") == 0) {
      str = "0."
      return str;
    } else {
      if (str.indexOf(".") == -1) {
        return str;
      } else {
        return str.match(/\d+\.\d{0,2}/g)[0]
      }
    }
  },

  goodsSetting(con, call) {
    requestCall(con, (res) => {
      call(res);
    })
  },

  delGoodsClassify(con, call) {
    requestCall(con, (res) => {
      call(res);
    })
  },

  requestCall(data, call) {
    wx.request({
      url,
      data,
      success: res => {
        if (res.data.code === "100") {
          call(res.data)
        } else {
          call(res.data)
        }
      }
    })
  },

  requestPost(data, call) {
    wx.request({
      url,
      data,
      method: "post",
      success: res => {
        if (res.data.code === "100") {
          call(res.data)
        } else {
          call(res.data)
        }
      }
    })
  }
}

/**
 * 请求的公用方法
 */
function requestCall(data, call) {
  wx.request({
    url,
    data,
    success: res => {
      if (res.data.code === "100") {
        call(res.data)
      } else {
        call(res.data)
      }
    }
  })
}

module.exports = goods;