var config = require("../config.js")
var url = config.requestUrl;

function req(data, call, method) {
  var header = {
    'content-type': 'application/json' // 默认值
  };
  if (method == "POST") {
    header = { 'content-type': 'application/x-www-form-urlencoded' };
  }
  wx.request({
    url,
    data,
    header: header || "",
    method: method || "GET",
    success: res => {
      call(res.data);
    }
  })
};

module.exports = req; 