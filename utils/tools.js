var ext = {};
wx.getExtConfig({
  success: function(res) {
    ext = res.extConfig;
  }
})

const tool = {
  filterImageSrc(src) {
    return ext.FilePath + src;
  }
}

module.exports = tool;