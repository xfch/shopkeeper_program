// pages/stock-about/set-info/set-info.js
var date = new Date();
var util = require("../../../utils/MD5.js")
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rkType: ["采购进货", "销售退货", "调拨入库", "其他入库"],
    ck: [],
    ghsList: [],
    rkt: {
      hasCalss: false,
      value: "采购进货"
    },
    rk: {
      hasClass: false,
      value: "请选择仓库"
    },
    rksj: {
      hasClass: false,
      value: "请选择入库时间"
    },
    ghs: {
      hasClass: true,
      value: "请选择供货商"
    },
    cgsj: {
      hasClass: false,
      value: "请选择采购时间"
    },
    zdr: {
      hasClass: true,
      value: "请选择制单人"
    },
    contact: "",
    contactTel: "",
    end: "",
    ShopNo: "",
    rank: [],
    staffdt: [],
    currentStaff: {},
    imp: {
      N_AMO_TYPE: "1",
      C_OP_NAME: "",
      N_SED_TYPE: "1",
      N_COUNT: "",
      N_ACOUNT: "",
      N_TOTAMOUNT: "",
      C_PHONE: "",
      C_DENAME: "",
      C_ADDRESS: "",
      N_GW_ID: "",
      C_DCSHOP: "",
      C_REMARK: "",
      D_OP_DATE: "",
      D_RK_TIME: "",
      N_KHIH: "",
    },
    remark: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var end = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    this.data.rksj.value = end;
    this.data.cgsj.value = end;
    this.setData({
      end,
      rksj: this.data.rksj,
      cgsj: this.data.cgsj,
    })
  },

  setRemark(ev) {
    this.data.remark = ev.detail.value
  },

  setContact(ev) {
    this.data.contact = ev.detail.value;
  },

  setContactTel(ev) {
    this.data.contactTel = ev.detail.value
  },

  setRkType(ev) {
    this.data.rkt.value = this.data.rkType[ev.detail.value];
    var types = 1;
    switch (ev.detail.value) {
      case 0:
        types = 1
        break;
      case 1:
        types = 2
        break;
      case 2:
        types = 3
        break;
    }
    this.data.imp.N_SED_TYPE = types;
    this.setData({
      rkt: this.data.rkt
    })
  },

  setRktime(ev) {
    this.data.rksj.value = ev.detail.value;
    this.data.rksj.hasClass = false;
    this.setData({
      rksj: this.data.rksj
    })
  },

  setCgtime(ev) {
    this.data.cgsj.value = ev.detail.value;
    this.data.cgsj.hasClass = false;
    this.setData({
      cgsj: this.data.cgsj
    })
  },

  setGhs(ev) {
    this.data.ghs.value = this.data.ghsList[ev.detail.value];
    this.data.imp.N_KHIH = this.data.goodsuper[ev.detail.value].N_Id;
    this.data.ghs.hasClass = false;
    this.setData({
      ghs: this.data.ghs
    })
  },

  setRank(ev) {
    this.data.zdr.value = this.data.rank[ev.detail.value];
    this.data.imp.C_OP_NAME = this.data.rank[ev.detail.value];
    this.data.zdr.hasClass = false;
    this.setData({
      zdr: this.data.zdr
    })
  },

  createSubmitData(ev) {
    this.data.imp.N_COUNT = this.data.stockGoodsList.length;
    var tNum = 0;
    var tPrice = 0;
    for (var i = 0; i < this.data.stockGoodsList.length; i++) {
      tNum += parseFloat(this.data.stockGoodsList[i].num)
      tPrice += parseFloat(this.data.stockGoodsList[i].total)
    }
    this.data.imp.N_ACOUNT = tNum;
    this.data.imp.N_TOTAMOUNT = tPrice;
    this.data.imp.C_PHONE = this.data.contactTel;
    this.data.imp.C_DENAME = this.data.contact;
    this.data.imp.N_GW_ID = "10";
    this.data.imp.C_DCSHOP = "12";
    this.data.imp.C_REMARK = this.data.remark;
    this.data.imp.D_OP_DATE = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    this.data.imp.D_RK_TIME = this.data.rksj.value;

    var list = [];
    for (var i = 0; i < this.data.stockGoodsList.length; i++) {
      var data = {
        C_GOODS_CODE: this.data.stockGoodsList[i].GOODS_CODE,
        C_GOODS_NAME: this.data.stockGoodsList[i].GOODS_NAME,
        N_SUPPLIER_ID: this.data.stockGoodsList[i].N_SuperId,
        C_SupName: this.data.contact,
        N_INPRICE: this.data.stockGoodsList[i].SALE_PRICE,
        N_GYPRICE: this.data.stockGoodsList[i].cbj,
        N_COUNT: this.data.stockGoodsList[i].num,
        N_CHARGE_NUM: this.data.stockGoodsList[i].STOCK,
        N_SJCOUNT: parseFloat(this.data.stockGoodsList[i].STOCK) + parseFloat(this.data.stockGoodsList[i].num),
        N_LX: 1,
        N_UNIT: 0,
        C_UNIT: this.data.stockGoodsList[i].dw.C_UnitName,
        N_AMOUNT: this.data.stockGoodsList[i].total,
        C_Remark: "",
        N_GID: this.data.stockGoodsList[i].ID,
        N_LastInPrice: "0.00",
        N_GBID: this.data.stockGoodsList[i].N_GB_ID,
      }
      list.push(data)
    };

    var MD5 = util.md5(`ShopNo&lcid=${this.data.ShopNo}*${ev.currentTarget.dataset.i}`).toUpperCase();
    var data = {
      bNo: "5405",
      lcid: ev.currentTarget.dataset.i,
      opname: this.data.opInfo.c_name,
      ShopNo: this.data.ShopNo,
      opid: this.data.opInfo.n_ss_id,
      imp: JSON.stringify(this.data.imp),
      impdetal: JSON.stringify(list),
      MD5 
    }

    wx.request({
      url: config.requestUrl,
      data,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: "POST",
      success: res => {
        if (res.data.code === "100") {
          wx.showToast({
            title: '入库成功',
            mask: true,
            duration: 1500,
            success: res => {
              setTimeout(() => {
                wx.reLaunch({
                  url: '/pages/functionPage/functionPage',
                })
              }, 1500)
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: res.data.msg,
            showCancel: false
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getGhsList();
        this.getRankData();
      },
    })

    wx.getStorage({
      key: 'stock-goods-list',
      success: res => {
        this.data.stockGoodsList = res.data;
      },
    })

    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.setData({
          opInfo: res.data,
        })
      },
    })
  },

  getGhsList() {
    var MD5 = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 5106,
      ShopNo: this.data.ShopNo,
      MD5,
    };

    wx.request({
      url: config.requestUrl,
      data,
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: res => {
        if (res.data.code === "100") {
          var ghsList = [];
          for (var i = 0; i < res.data.goodsuper.length; i++) {
            ghsList.push(res.data.goodsuper[i].C_SupName);
          }

          this.setData({
            ghsList,
            goodsuper: res.data.goodsuper,
          })
        }
      }
    })
  },

  getRankData() {
    var str = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7012,
      ShopNo: this.data.ShopNo,
      MD5: str,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var rank = [];
        for (var i = 0; i < res.data.staffdt.length; i++) {
          rank.push(res.data.staffdt[i].c_name)
        }
        this.setData({
          rank,
          staffdt: res.data.staffdt,
        })
      }
    })
  },
})