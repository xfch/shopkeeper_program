// pages/stock-about/set-goods/set-goods.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'stock-goods-list',
      success: res => {
        var totalNumber = 0, totalPrice = 0;
        for (var i = 0; i < res.data.length; i++) {
          totalNumber += parseFloat(res.data[i].num);
          totalPrice += parseFloat(res.data[i].total)
        }
        totalPrice = totalPrice.toFixed(2)
        this.setData({
          totalNumber,
          totalPrice,
          goodsList: res.data
        })
      },
    })
  },

  scanAdd() {
    wx.scanCode({
      success: res => {
        wx.navigateTo({
          url: '/pages/stock-about/scan-result/scan-result?code=' + res.result,
        })
      }
    })
  },

  searchAdd() {
    wx.navigateTo({
      url: '/pages/stock-about/choose-goods/choose-goods',
    })
  },

  nextStep() {
    wx.navigateTo({
      url: '/pages/stock-about/set-info/set-info',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})