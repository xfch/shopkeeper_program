// pages/stock-about/choose-goods/choose-goods.js
var g = require('../../../utils/goodsCommon.js')
var util = require("../../../utils/MD5.js")
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classify: [1, 2, 3, 4, 5],
    goodsList: [],
    cGoods: [1, 2, 3, 4, 55, 5, 5, 5, 5, ],
    cIndex: 0,
    goodsIndex: 0,
    showSetting: false,
    showChoose: false,
    getStatus: true,
    pageindex: 1,
    pagesize: 1,
    amount: "",
    num: "",
    kword: "",
    currentGoods: {},
    dwList: [],
    stockGoodsList: [],
    dwText: "请选择单位",
    dw: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  showChoose() {
    this.setData({
      showChoose: !this.data.showChoose
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  showGoodsSet(ev) {
    this.data.currentGoods = ev.currentTarget.dataset.d;
    var dwText = "";
    var goodsIndex = 0;
    if (ev.currentTarget.dataset.d.dw) {
      dwText = ev.currentTarget.dataset.d.dw.C_UnitName
    }
    if (ev.currentTarget.dataset.i) {
      goodsIndex = ev.currentTarget.dataset.i;
    }
    this.setData({
      amount: ev.currentTarget.dataset.d.cbj || "",
      num: ev.currentTarget.dataset.d.num || "",
      dwText,
      showSetting: true,
      showChoose: false,
      goodsIndex,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getStockList();
        this.getShopUnit();
      },
    });
  },

  getStockList() {
    wx.getStorage({
      key: 'stock-goods-list',
      success: res => {
        this.setData({
          stockGoodsList: res.data
        })
        
      },
      complete: () => {
        this.getGoodsClassify();
      }
    })
  },

  sure(ev) {
    if (!this.data.amount) {
      wx.showModal({
        title: '提示',
        content: '请填写成本价',
        showCancel: false,
      })

      return;
    }
    if (!this.data.num) {
      wx.showModal({
        title: '提示',
        content: '请填写数量',
        showCancel: false,
      })

      return;
    }
    var flag = false;
    var idx = 0;
    for (var i = 0; i < this.data.stockGoodsList.length; i++) {
      if (this.data.stockGoodsList[i].ID == this.data.currentGoods.ID) {
        idx = i;
        flag = true;
        break;
      }
    }

    this.data.currentGoods.cbj = this.data.amount;
    this.data.currentGoods.num = this.data.num;
    this.data.currentGoods.total = parseFloat(this.data.amount) * parseFloat(this.data.num)
    if (flag) {
      wx.showModal({
        title: '提示',
        content: '当前清单中已存在该项设置，是否将其覆盖？',
        success: res => {
          if (res.confirm) {
            this.data.stockGoodsList[idx] = this.data.currentGoods;
            wx.showToast({
              title: '已覆盖',
              success: res => {
                this.hideSetting();
                this.setLocalInfo();
              }
            })
          } else {
            this.hideSetting();
          }
        }
      })
    } else {
      this.data.stockGoodsList.push(this.data.currentGoods);
      wx.showToast({
        title: '设置成功',
        success: res => {
          this.hideSetting();
          this.setLocalInfo();
        }
      })
    }
    
  },

  hideChoose() {
    this.setData({
      showChoose: false
    })
  },

  setLocalInfo() {
    wx.setStorage({
      key: 'stock-goods-list',
      data: this.data.stockGoodsList,
    })
    this.data.goodsList[this.data.goodsIndex].checked = true;
    this.setData({
      goodsList: this.data.goodsList,
      stockGoodsList: this.data.stockGoodsList
    })
  },

  isok() {
    wx.navigateBack({
      delta: 1
    })
  },

  /**
   * 获取店铺单位
   */
  getShopUnit() {
    var MD5 = util.md5(`ShopNo=${this.data.shopNo}`).toUpperCase();
    var data = {
      bNo: 5109,
      ShopNo: this.data.shopNo,
      MD5,
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          var dw = [];
          for (var i = 0; i < res.data.data.length; i++) {
            dw.push(res.data.data[i].C_UnitName);
          }
          this.data.dwList = res.data.data;
          this.setData({
            dw
          })
        }
      }
    })
  },

  setAmount(ev) {
    var amount = 0;
    if (/\./g.test(ev.detail.value)) {
      amount = ev.detail.value.match(/\d+\.\d{0,2}/g)[0]
    } else {
      amount = ev.detail.value;
    }
    this.setData({
      amount
    })
  },

  setNumber(ev) {
    var num = 0;
    if (/\./g.test(ev.detail.value)) {
      num = ev.detail.value.match(/\d+\.\d{0,2}/g)[0]
    } else {
      num = ev.detail.value;
    }
    this.setData({
      num
    })
  },

  setDw(ev) {
    this.data.currentGoods.dw = this.data.dwList[ev.detail.value]
    this.setData({
      dwText: this.data.dw[ev.detail.value]
    })
  },

  changeClassify(ev) {
    this.data.CateId = ev.currentTarget.dataset.d;
    this.setData({
      goodsList: [],
      cIndex: ev.currentTarget.dataset.i
    })
    this.getGoodsList();
  },

  getGoodsClassify() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    
    g.getGoodsClassify({
      bNo: 3100,
      ShopNo: this.data.shopNo,
    }, (data) => {
      for (var i = 0; i < data.length; i++) {
        if (data[i].CATEGORY_NAME.length > 5) {
          data[i].CATEGORY_NAME = data[i].CATEGORY_NAME.substr(0, 5)
        }
      }
      
      this.setData({
        CateId: data[0].ID,
        classify: data,
      })
      wx.hideLoading()
      this.getGoodsList();
    })
  },

  getGoodsList() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    if (this.data.getStatus) {
      this.data.getStatus = false;
    } else {
      return;
    }
    var MD5 = util.md5(`shopno&cid&gcode&superid&iskc=${this.data.shopNo}*${this.data.CateId}***0`).toUpperCase();
    var data = {
      bNo: 5406,
      shopno: this.data.shopNo,
      cid: this.data.CateId,
      pageindex: 1,
      pagesize: 10,
      gcode: "",
      superid: "",
      iskc: 0,
      MD5
    }
    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        for (var i = 0; i < res.data.infoList.length; i++) {
          if (res.data.infoList[i].GOODS_NAME.length > 16) {
            res.data.infoList[i].GOODS_NAME = res.data.infoList[i].GOODS_NAME.substr(0, 16) + "..."
          }
        }
        this.setData({
          goodsList: res.data.infoList
        })
        wx.hideLoading()
      }
    })
  },

  hideSetting() {
    this.setData({
      showSetting: false,
    })
  },

  def() {},

  checkGoods(data) {
    for (var i = 0; i < data.dtResult.length; i++) {
      if (data.dtResult[i].C_MAINPICTURE) {
        data.dtResult[i].C_MAINPICTURE = config.FilePath + data.dtResult[i].C_MAINPICTURE;
      }
      for (var j = 0; j < this.data.stockGoodsList.length; j++) {
        if (this.data.stockGoodsList[j].ID == data.dtResult[i].ID) {
          data.dtResult[i].cbj = this.data.stockGoodsList[j].cbj;
          data.dtResult[i].num = this.data.stockGoodsList[j].num;
          data.dtResult[i].dw = this.data.stockGoodsList[j].dw;
          data.dtResult[i].checked = true;
        }
      }
      this.data.goodsList.push(data.dtResult[i])
    }
    this.setData({
      goodsList: this.data.goodsList,
    })
  },
})