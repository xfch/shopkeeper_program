// pages/stock-about/scan-result/scan-result.js
// pages/goodsManage/goodsManage.js
var g = require('../../../utils/goodsCommon.js')
var config = require("../../../config.js")
var util = require("../../../utils/MD5.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getStatus: true,
    pageindex: 1,
    pagesize: 10,
    kword: "6901236341582",
    stockGoodsList: [],
    dw: ["无", "件", "个", "支", "盒", "瓶", "kg", "千克", "测试"],
    dwList: [],
    amount: "0.00",
    num: "0.00",
    dwText: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.code) {
      this.data.kword = options.code;
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getShopUnit();
        if (this.data.kword) {
          this.getGoodsList();
        }
      },
    });

    this.setData({
      dwText: this.data.dw[0]
    })

    wx.getStorage({
      key: 'stock-goods-list',
      success: res => {
        this.data.stockGoodsList = res.data;
      },
    })
  },

  /**
   * 获取店铺单位
   */
  getShopUnit() {
    var MD5 = util.md5(`ShopNo=${this.data.shopNo}`).toUpperCase();

    var data = {
      bNo: 5109,
      ShopNo: this.data.shopNo,
      MD5,
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          var dw = [];
          for (var i = 0; i < res.data.data.length; i++) {
            dw.push(res.data.data[i].C_UnitName);
          }
          this.data.dwList = res.data.data;
          this.setData({
            dw
          })
        }
      }
    })
  },

  setDw(ev) {
    this.data.goods.dw = this.data.dwList[ev.detail.value]
    this.setData({
      dwText: this.data.dw[ev.detail.value]
    })
  },

  setAmount(ev) {
    var amount = 0;
    if (/\./g.test(ev.detail.value)) {
      amount = ev.detail.value.match(/\d+\.\d{0,2}/g)[0]
    } else {
      amount = ev.detail.value;
    }
    this.setData({
      amount
    })
  },

  setNumber(ev) {
    var num = 0;
    if (/\./g.test(ev.detail.value)) {
      num = ev.detail.value.match(/\d+\.\d{0,2}/g)[0]
    } else {
      num = ev.detail.value;
    }
    this.setData({
      num
    })
  },

  /**
   * 发起扫码
   */
  scan() {
    wx.scanCode({
      success: res => {
        this.data.kword = res.result;
        this.getGoodsList();
      }
    })
  },

  save(ev) {
    var flag = false;
    var idx = -1;
    this.data.goods.cbj = parseFlaot(this.data.amount).toFixed(2);
    this.data.goods.num = this.data.num;
    this.data.goods.total = parseFloat(this.data.num) * parseFloat(this.data.amount)
    this.data.goods.total = this.data.goods.total.toFixed(2);
    for (var i = 0; i < this.data.stockGoodsList.length; i++) {
      if (this.data.stockGoodsList[i].ID == this.data.goods.ID) {
        flag = true;
        idx = i;
        break
      }
    }
    
    if (!flag) {
      this.data.stockGoodsList.push(this.data.goods);
    } else {
      this.data.stockGoodsList[i] = this.data.goods;
    }

    wx.setStorage({
      key: 'stock-goods-list',
      data: this.data.stockGoodsList,
      success: res => {
        if (ev.currentTarget.dataset.i == 1) {
          this.scan();
        } else {
          wx.navigateBack({
            delta: 1
          })
        }
      }
    })
  },

  /**
   * 根据关键词搜索商品
   */
  getGoodsList() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    if (this.data.getStatus) {
      this.data.getStatus = false;
    } else {
      return;
    }

    var MD5 = util.md5(`shopno&cid&gcode&superid&iskc=${this.data.shopNo}**${this.data.kword}**0`).toUpperCase();
    var data = {
      bNo: 5406,
      shopno: this.data.shopNo,
      cid: "",
      pageindex: 1,
      pagesize: 10,
      gcode: this.data.kword,
      superid: "",
      iskc: 0,
      MD5
    }
    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        for (var i = 0; i < res.data.infoList.length; i++) {
          if (res.data.infoList[i].GOODS_NAME.length > 16) {
            res.data.infoList[i].GOODS_NAME = res.data.infoList[i].GOODS_NAME.substr(0, 16) + "..."
          }
        }
        for (var i = 0; i < res.data.infoList.length; i++) {
          if (res.data.infoList[i].C_MAINPICTURE) {
            res.data.infoList[i].C_MAINPICTURE = config.FilePath + res.data.infoList[i].C_MAINPICTURE;
          }
          res.data.infoList[i].dw = {};
        }

        var flag = false;
        for (var i = 0; i < res.data.infoList.length; i++) {
          for (var j = 0; j < this.data.stockGoodsList.length; j++) {
            if (this.data.stockGoodsList[j].ID == res.data.infoList[i].ID) {
              flag = true;
              break;
            }
          }
          if (flag) {
            break;
          }
        }

        if (flag) {
          wx.showModal({
            title: '提示',
            content: '该商品已被设置过，是否继续设置并覆盖之前的设置？',
            success: res => {
              if (res.cancel) {
                wx.navigateBack({
                  delta: 1
                })
              }
            }
          })
        }

        this.setData({
          goods: data.dtResult[0],
        })
        if (data.dtResult.length == 0 && data.totalPage == 0) {
          this.setData({
            err: {
              status: true,
              msg: "没找到该商品~"
            }
          })
        }
        this.data.getStatus = true;
      }
    })
  }
})