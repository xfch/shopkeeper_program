// pages/stock-about/rk/rk.js
var util = require("../../../utils/MD5.js")
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showHighSearch: false,
    searchType: 0,
    searchStatus: 0,
    list: [1, 2, 3],
    starttime: "",
    endtime: "",
    kword: "",
    sxid: "",
    impzt: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  def() {},

  /**
   * 展示高级搜索框
   */
  showHighSearch() {
    this.setData({
      showHighSearch: true
    })
  },

  getRkList() {
    var MD5 = util.md5(`shopno&starttime&endtime&kword&sxid&impzt=${this.data.ShopNo}*${this.data.starttime}*${this.data.endtime}*${this.data.kword}*${this.data.sxid}*${this.data.impzt}`).toUpperCase();

    var data = {
      bNo: 5400,
      shopno: this.data.ShopNo,
      starttime: this.data.starttime,
      endtime: this.data.endtime,
      kword: this.data.kword,
      sxid: this.data.sxid,
      impzt: this.data.impzt,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {

      }
    })
  },

  hideHighSearch() {
    this.setData({
      showHighSearch: false
    })
  },

  setFilterType(ev) {
    this.setData({
      searchType: ev.currentTarget.dataset.i
    })
    this.hideHighSearch();
  },

  setFilterStatus(ev) {
    this.setData({
      searchStatus: ev.currentTarget.dataset.i
    })
    this.hideHighSearch();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getRkList();
      },
    })
  },

  add() {
    wx.navigateTo({
      url: '/pages/stock-about/set-goods/set-goods',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }
})