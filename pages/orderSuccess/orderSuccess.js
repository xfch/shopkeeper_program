// pages/orderSuccess/orderSuccess.js
var util = require("../../utils/MD5.js")
var config = require("../../config.js");
var timer = null;
var request = require("../../utils/request.js");
var timer2 = null;
var time = 60;
var g = getApp().globalData;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goodsList: [],
        callStatus: true,
        callText: "呼叫",
        sucText: "点菜",
        Examine: 0
    },

    onLoad: function (options) {
        console.log(options)
        var pages = getCurrentPages();//页面 
        var prevpage = pages[pages.length - 2];//上一页
        // console.log(prevpage.data)
        this.data.ShopNo = g.SHOP_NO || "100010";
        if (options.t != "o") {
            this.setData({
                sucText: "加菜"
            })
        }
        this.setData({
            goodsList: prevpage.data.resultList,
            totalPrice: prevpage.data.totalPrice,
            totalNum: prevpage.data.totalNum,
            // ztInfo: prevpage.data.ztInfo
        })
    },

    showCallOptions() {
        if (!this.data.callStatus) {
            return;
        }
        if (this.data.Examine != 1) {
            this.callWaiter(1);
        } else {
            wx.showActionSheet({
                itemList: ["呼叫服务员", "催菜", "呼叫结账"],
                success: res => {
                    this.callWaiter(res.tapIndex + 1);
                }
            })
        }
    },

    callWaiter(CallType) {
        var MD5 = util.md5(`OrderNo&TID&CallType&ShopNo=${this.data.ztInfo.C_ORD_NO}*${this.data.ztInfo.ID}*${CallType}*${this.data.ShopNo}`).toUpperCase();
        var data = {
            bNo: 2223,
            OrderNo: this.data.ztInfo.C_ORD_NO,
            TID: this.data.ztInfo.ID,
            CallType: CallType,
            ShopNo: this.data.ShopNo,
            MD5,
        }

        if (this.data.callStatus) {
            this.setData({
                callStatus: false,
            })
            wx.request({
                url: config.requestUrl,
                data,
                success: res => {
                    if (res.data.code === "100") {
                        timer = setInterval(() => {
                            if (time > 1) {
                                time--;
                                this.setData({
                                    callText: time + "s",
                                })
                            } else {
                                this.setData({
                                    callText: "呼叫",
                                    callStatus: true,
                                })
                                time = 60;
                                clearInterval(timer)
                            }
                        }, 1000)
                    }
                }
            })
        }
    },
    onShow: function () {
        wx.getStorage({
            key: 'result-list',
            success: res => {
                console.log('result-list----')
                console.log(res)
                this.setData({
                    goodsList: res.data.gList,
                    totalPrice: res.data.totalPrice,
                    totalNum: res.data.totalNum
                })
            },
        })

        wx.getStorage({
            key: 'zt-info',
            success: res => {
                this.setData({
                    ztInfo: res.data,
                })
            },
        })

        timer2 = setInterval(() => {
            this.checkOrderStatus();
        }, 3000)
    },

    checkOrderStatus(pull) {
        var str = `ShopNo&OrderNo=${this.data.ShopNo}*${this.data.ztInfo.C_ORD_NO}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4004,
            ShopNo: this.data.ShopNo,
            OrderNo: this.data.ztInfo.C_ORD_NO,
            MD5: md5.toUpperCase()
        };

        request(data, (res) => {
            var Examine = 0;
            if (res.Examine == undefined) {
                Examine = 0;
            } else {
                Examine = res.Examine;
            }
            if (Examine != 0) {
                var flag = false;
                for (var i = 1; i < res.gbase.length; i++) {
                    if (res.gbase[i].GZT == 3) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    this.setData({
                        Examine: Examine,
                    })
                    clearInterval(timer2);
                    wx.setStorage({
                        key: 'scan-order-status',
                        data: Examine,
                    })
                } else {
                    wx.setStorage({
                        key: 'scan-order-status',
                        data: 0,
                    })
                }
            } else {
                wx.setStorage({
                    key: 'scan-order-status',
                    data: 0,
                })
            }

            wx.removeStorage({
                key: 'result-list',
            })

            if (pull) {
                wx.stopPullDownRefresh()
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearInterval(timer)
        clearInterval(timer2)
    },


    onPullDownRefresh() {
        this.checkOrderStatus("1");
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(timer)
        clearInterval(timer2)
    },
})