// pages/editMemberClassify/editMemberClassify.js
var config = require("../../config.js");
var util = require("../../utils/MD5.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    vipPrice: true,
    allow: false,
    term: 1,
    vipPriceType: 2,
    shopNo: "",
    date: [{
      name: '1',
      value: '周一'
    }, {
      name: '2',
      value: '周二',
    }, {
      name: '3',
      value: '周三'
    }, {
      name: '4',
      value: '周四'
    }, {
      name: '5',
      value: '周五'
    }, {
      name: '6',
      value: '周六'
    }, {
      name: '7',
      value: '周日'
    }],
    opInfo: {},
    priceType1: 1.00,
    priceType2: 0,
    mgInfo: {
      N_MG_ID: 0,
      C_MG_NAME: "",
      N_DIS_TYPE: 1,
      N_MG_DISCOUNT: "",
      N_DIS_LX: 0,
      C_DISTIME: "",
      N_MG_LOCK: 1,
      N_MG_AMOUNT: 0
    }
  },

  submitInfo() {

    if (!this.data.mgInfo.C_MG_NAME) {
      wx.showModal({
        title: '提示',
        content: "请填写会员类型名/等级名",
        showCancel: false
      })
    }

    var N_MG_DISCOUNT = 0;

    if (this.data.mgInfo.N_DIS_TYPE == 1) {
      N_MG_DISCOUNT = this.data.priceType1;
    } else {
      N_MG_DISCOUNT = this.data.priceType2;
    }

    var gradedata = {
      N_MG_ID: this.data.mgInfo.N_MG_ID,
      C_SHOP_NO: this.data.shopNo,
      C_MG_NAME: this.data.mgInfo.C_MG_NAME,
      N_DIS_TYPE: this.data.mgInfo.N_DIS_TYPE,
      N_MG_DISCOUNT: N_MG_DISCOUNT,
      N_DIS_LX: this.data.mgInfo.N_DIS_LX,
      C_DISTIME: this.data.mgInfo.N_DIS_LX != 0 ? this.data.mgInfo.C_DISTIME : "",
      N_MG_LOCK: this.data.mgInfo.N_MG_LOCK,
      N_OP_ID: this.data.opInfo.n_ss_id,
      N_MG_AMOUNT: this.data.mgInfo.N_MG_LOCK == 2 ? this.data.mgInfo.N_MG_AMOUNT : 0
    }

    // JSON.stringify(gradedata)
    var data = {
      bNo: 1208,
      opname: this.data.opInfo.c_name,
      gradedata: JSON.stringify(gradedata),
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var title = "";
        if (this.data.editType == 1) {
          title = "修改成功"
        } else {
          title = "添加成功"
        }
        if (res.data.code === "100" && res.data.Result != 0) {
          wx.showToast({
            title,
            icon: "success",
            duration: 1000,
            success: res => {
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1
                })
              }, 1000);
            }
          })
        } else {
          var content = "";
          if (this.data.editType == 1) {
            content = "失败成功，请重试"
          } else {
            content = "添加成功，请重试"
          }
          wx.showModal({
            title: '提示',
            content,
            showCancel: false
          })
        }
      }
    })
  },

  /**
   * 设置会员等级名称
   */
  setMgname(ev) {
    this.data.mgInfo.C_MG_NAME = ev.detail.value
  },

  /**
   * 设置折扣具体数值
   */
  setDiscount(ev) {
    if (ev.detail.value != 1) {
      var priceType1 = ev.detail.value.replace("0.", "").replace(/[^\d]/g, "");
      if (priceType1.indexOf(".") == -1) {
        priceType1 = "0." + priceType1;
        priceType1 = priceType1.match(/\d\.\d{0,2}/g)[0]
      }
    }

    this.setData({
      priceType1,
    })
  },

  setDiscount2(ev) {
    var priceType2 = ev.detail.value.replace(/[^\d]/g, "")
    if (parseInt(priceType2) > 100) {
      priceType2 = 100;
    }
    this.setData({
      priceType2,
    })
  },

  /**
   * 设置成为会员的最低金额
   */
  setMgamount(ev) {
    this.data.mgInfo.N_MG_AMOUNT = parseInt(ev.detail.value.replace(/[^\d]/g, ""));
    this.setData({
      mgInfo: this.data.mgInfo
    })
  },

  /**
   * 如果打折期限为每周
   * 设置打折天数
   */
  checkboxChange(ev) {
    this.data.mgInfo.C_DISTIME = ev.detail.value.join(",");
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        if (options.mgid) {
          this.data.editType = 1
          this.getMgdetail(options.mgid);
        } else {
          this.data.editType = 2
        }
      },
    })

    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;
      },
    })
  },

  /**
   * 获取会员等级详情
   */
  getMgdetail(mgid) {
    var data = {
      bNo: 1209,
      mgid,
      ShopNo: this.data.shopNo,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          var date = res.data.Result.C_DISTIME.split(",");
          if (res.data.Result.N_DIS_TYPE == 1) {
            this.data.priceType1 = res.data.Result.N_MG_DISCOUNT;
          } else {
            this.data.priceType2 = res.data.Result.N_MG_DISCOUNT;
          }
          for (var i = 0; i < this.data.date.length; i++) {
            this.data.date[i].checked = false;
            for (var j = 0; j < date.length; j++) {
              if (date[j] == this.data.date[i].name) {
                this.data.date[i].checked = true;
              }
            }
          }
          this.setData({
            priceType1: this.data.priceType1,
            priceType2: this.data.priceType2,
            date: this.data.date,
            mgInfo: res.data.Result
          })
        }
      }
    })
  },

  /**
   * 取消编辑
   */
  cancel() {
    wx.navigateBack({
      delta: 1
    })
  },

  /**
   * 是否禁止会员自动升级
   */
  allowAmount() {
    this.data.mgInfo.N_MG_LOCK = this.data.mgInfo.N_MG_LOCK == 2 ? 1 : 2,
      this.setData({
        mgInfo: this.data.mgInfo
      })
  },

  /**
   * 设置会员打折方式
   */
  changeVipType(ev) {
    this.data.mgInfo.N_DIS_TYPE = ev.currentTarget.dataset.i
    this.data.mgInfo.N_MG_DISCOUNT = "";
    this.setData({
      mgInfo: this.data.mgInfo
    })
  },

  /**
   * 设置打折期限
   */
  changeShop(ev) {
    this.data.mgInfo.N_DIS_LX = ev.currentTarget.dataset.i
    this.setData({
      mgInfo: this.data.mgInfo
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})