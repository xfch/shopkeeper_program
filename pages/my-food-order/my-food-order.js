// pages/my-food-order/my-food-order.js
var g = getApp().globalData;
var config = require("../../config.js")
var util = require("../../utils/MD5.js")
var request = require("../../utils/request.js");

// ****
let app = getApp();
const common = require('../../common/js/common.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    Examine: 0,
  },
  onLoad: function (opts) {
    this.data.ShopNo = g.SHOP_NO || "100010";
    //   console.log(common.prevPage().data)
    //   this.setData({
    //       ztInfo: common.prevPage().data.ztInfo
    //   })
    //   wx.setNavigationBarTitle({
    //       title: common.prevPage().data.ztInfo.NAME,
    //   })
    //   common.getOrderStatus(this, common.prevPage().data.ztInfo.C_ORD_NO)
  },
  onShow: function () {
    wx.getStorage({
      key: 'zt-info',
      success: res => {
        res.data.STARTTIME = res.data.STARTTIME.replace("T", " ");
        this.setData({
          ztInfo: res.data
        })
        wx.setNavigationBarTitle({
          title: res.data.NAME,
        })

        if (res.data.C_ORD_NO) {
          this.getOrderInfo();
        }
      },
    })
  },

  getOrderInfo(again) {
    if (!again) {
      wx.showLoading({
        title: '加载中',
      })
    }
    var str = `ShopNo&OrderNo=${this.data.ShopNo}*${this.data.ztInfo.C_ORD_NO}`;
    var md5 = util.md5(str);
    var gdata = {
      bNo: 4004,
      ShopNo: this.data.ShopNo,
      OrderNo: this.data.ztInfo.C_ORD_NO,
      MD5: md5.toUpperCase()
    };

    request(gdata, (data) => {
      var Examine = 0;
      if (data.Examine == undefined) {
        Examine = 0
      } else {
        Examine = data.Examine
      }
      if (Examine == 0) {
        setTimeout(() => {
          this.getOrderInfo("1");
        }, 2000)
      }
      if (!again) {
        wx.hideLoading()
      }
      var flag = false;
      for (var i = 0; i < data.gbase.length; i++) {
        if (data.gbase[i].ZFAmount) {
          data.gbase[i].ZFAmount = parseFloat(data.gbase[i].ZFAmount).toFixed(2);
        }
        if (data.gbase[i].N_GoodsFeedJe) {
          data.gbase[i].N_GoodsFeedJe = parseFloat(data.gbase[i].N_GoodsFeedJe).toFixed(2);
        }
        data.gbase[i].N_REALPRICE = parseFloat(data.gbase[i].N_REALPRICE).toFixed(2);
        
      }
      for (var i = 0; i < data.gbase.length; i++) {
        if (data.gbase[i].GZT == 3) {
          flag = true;
          break;
        }
      }
      
      var totalPrice = parseFloat(data.PayAmount).toFixed(2);
      if (!flag) {
        wx.setStorage({
          key: 'scan-order-status',
          data: Examine,
        })
      } else {
        Examine = 0;
      }

      this.setData({
        totalPrice,
        Examine: Examine,
        goodsList: data.gbase
      })
    })
  },

  addFood() {
    wx.navigateBack({
      delta: 1
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  }
})