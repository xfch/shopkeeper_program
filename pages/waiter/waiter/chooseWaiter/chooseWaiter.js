// pages/waiter/chooseWaiter/chooseWaiter.js
var request = require("../../../utils/request.js")
var util = require("../../../utils/MD5.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ShopNo: "", // 店铺编号
    Waiterlist: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getWaiterInfo()
      },
    })
  },

  getWaiterInfo() {
    var str = `ShopNo=${this.data.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 2201,
      ShopNo: this.data.ShopNo,
      MD5: md5.toUpperCase()
    };

    request(data, data => {     
      wx.getStorage({
        key: 'waiter',
        success: res => {
          for (var i = 0; i < data.Waiterlist.length; i++) {
            if (res.data.n_ss_id == data.Waiterlist[i].n_ss_id) {
              data.Waiterlist[i].checked = true;
            } else {
              data.Waiterlist[i].checked = false;
            }
          }
          this.setData({
            Waiterlist: data.Waiterlist,
          })
        },
        fail: () => {
          for (var i = 0; i < data.Waiterlist.length; i++) {
            data.Waiterlist[i].checked = false;
          }
          this.setData({
            Waiterlist: data.Waiterlist,
          })
        }
      })
    })
  },

  chooseWaiter(ev) {
    for (var i = 0; i < this.data.Waiterlist.length; i++) {
      this.data.Waiterlist[i].checked = false;
    }
    var index = ev.currentTarget.dataset.i;
    this.data.Waiterlist[index].checked = !this.data.Waiterlist[index].checked;
    wx.setStorage({
      key: 'waiter',
      data: this.data.Waiterlist[index],
    })
    this.setData({
      Waiterlist: this.data.Waiterlist,
    })
  },

  sure() {
    wx.navigateBack({
      delta: 1
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
})