// pages/message/message.js
var util = require("../../../utils/MD5.js");
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    C_SHOP_NO: "",
    messageList: [],
    filePath: config.FilePath
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  getMessageList() {
    var str = "ShopNo=" + this.data.C_SHOP_NO;
    var md5 = util.md5(str);
    var data = {
      bNo: 2000,
      ShopNo: this.data.C_SHOP_NO,
      MD5: md5.toUpperCase()
    }
    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          // 1未读 2已读 3删除 N_MsgState 
          for (var i = 0; i < res.data.msgList.length; i++) {

            res.data.msgList[i].D_opdate = res.data.msgList[i].D_opdate.substr(5, 5).replace("-", ".");
            res.data.msgList[i].C_Set_Content = res.data.msgList[i].C_Set_Content.replace("[messagenum]", res.data.msgList[i].N_MsgCount)
            if (res.data.msgList[i].C_Set_Content.length > 25) {
              res.data.msgList[i].C_Set_Content2 = res.data.msgList[i].C_Set_Content.substr(0, 25) + "..."
            } else {
              res.data.msgList[i].C_Set_Content2 = res.data.msgList[i].C_Set_Content
            }
          }
          this.setData({
            messageList: res.data.msgList
          })
        }
      }
    })
  },

  viewMssage(ev) {
    var msg = ev.currentTarget.dataset.m;
    wx.setStorage({
      key: 'message_content',
      data: msg,
    })
    wx.navigateTo({
      url: '/pages/messageDetail/messageDetail',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.setData({
          C_SHOP_NO: res.data
        });
        this.getMessageList();
      },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})