// pages/waiter/index/index.js
var tableControllor = require("../../../utils/tableControllor.js");
var config = require("../../../config.js")
var util = require("../../../utils/MD5.js")
var request = require("../../../utils/request.js");
var timer = null;

/**
 * 关于弹窗字段dType的参数说明
 * 0 查看已开桌但未点菜的桌台
 * 1 选择桌台
 * 2 查看已开桌已点菜的桌台
 * 3 查看待清理的桌台
 */

Page({

    /**
     * 页面的初始数据
     */
    data: {
        opInfo: {},
        activeId: 0,
        useStatus: -1,
        inputAmount: 0,
        zlAmount: 0,
        ztStatus: "",
        moreTable: [], // 多桌台开桌的记录
        tableOptRecord: [],
        cashPay: false,
        useText: "全部状态",
        ShopNo: "", // 店铺编号
        RorTlist: [], // 桌台以及区域信息
        Regions: [], // 区域信息
        tabWidth: 0,
        showDialog: false,
        dType: 3, // 展示弹窗类型
        check: false,
        TableID: "",
        currentTable: {}, // 当前桌台信息
        zt: {},
        Tablesinfo: {}, // 桌台数据
    },

    /**
     * 
     */
    getZtInfo(types) {
        tableControllor.getTableInfo({
            ShopNo: this.data.ShopNo
        }, data => {
            var clearList = [];
            var clearRecord = [];
            for (var i = 0; i < data.RorTlist.length; i++) {
                this.data.StoreCWF = data.StoreCWF
                if (data.RorTlist[i].TableInfos) {
                    for (var j = 0; j < data.RorTlist[i].TableInfos.length; j++) {

                        data.RorTlist[i].TableInfos[j].info = `${data.RorTlist[i].TableInfos[j].PEOPLECOUNT}人/${data.RorTlist[i].TableInfos[j].PEOPLESIZE}人`
                        // 记录桌台列表里不可用的桌台
                        if (data.RorTlist[i].TableInfos[j].TABLE_STATE != 2 && data.RorTlist[i].TableInfos[j].TABLE_STATE != 1) {
                            clearList.push(data.RorTlist[i].TableInfos[j].ID);
                        }

                        if (data.RorTlist[i].TableInfos[j].TABLE_STATE != 2) {
                            clearRecord.push(data.RorTlist[i].TableInfos[j].ID)
                        }
                    }
                    data.RorTlist[i].noData = false
                } else {
                    data.RorTlist[i].noData = true
                }
            }

            for (var j = 0; j < clearList.length; j++) {
                for (var i = 0; i < this.data.moreTable.length; i++) {
                    if (this.data.moreTable[i].ids.indexOf(clearList[j]) != -1) {
                        this.data.moreTable[i].remove = true;
                    }
                }
            };

            for (var i = 0; i < clearRecord.length; i++) {
                for (var k = 0; k < this.data.tableOptRecord.length; k++) {
                    if (this.data.tableOptRecord[k].ID == clearRecord[i]) {
                        this.data.tableOptRecord[k].remove = true;
                    }
                }
            }

            for (var k = 0; k < this.data.moreTable.length; k++) {
                if (this.data.moreTable[k].remove) {
                    this.data.moreTable.splice(k, 1);
                    k--
                }
            }
            for (var k = 0; k < this.data.tableOptRecord.length; k++) {
                if (this.data.tableOptRecord[k].remove) {
                    this.data.tableOptRecord.splice(k, 1);
                    k--
                }
            }

            wx.setStorageSync("order-table", this.data.moreTable);
            wx.setStorageSync("Tablesinfo", this.data.tableOptRecord);

            data.Regions.unshift({
                ID: 0,
                Name: "全部"
            })
            var width = 0;
            for (var i = 0; i < data.Regions.length; i++) {
                width += data.Regions[i].Name.length * 28 + 40
            }

            this.setData({
                tabWidth: width + 20,
                Regions: data.Regions,
                RorTlist: data.RorTlist
            })
        })
    },

    filterTable(ev) {
        var index = ev.currentTarget.dataset.idx;
        if (index != 0) {
            // this.data.RorTlist[index - 1].noData = false;
        }
        this.setData({
            RorTlist: this.data.RorTlist,
            activeId: ev.currentTarget.dataset.i
        })
    },

    changeStatus() {
        var arr = ["全部状态", "空闲", "已开桌", "使用中", "待清理", "预结账"];
        wx.showActionSheet({
            itemList: arr,
            success: res => {
                var id = res.tapIndex - 1;
                if (id != -1) {
                    for (var i = 0; i < this.data.RorTlist.length; i++) {
                        var flag = false;
                        if (this.data.RorTlist[i].TableInfos) {
                            for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                                if (this.data.RorTlist[i].TableInfos[j].TABLE_STATE == id) {
                                    flag = true;
                                    break;
                                }
                            }
                        }

                        if (!flag) {
                            this.data.RorTlist[i].noData = true;
                        } else {
                            this.data.RorTlist[i].noData = false;
                        }
                    }
                } else {
                    for (var i = 0; i < this.data.RorTlist.length; i++) {
                        this.data.RorTlist[i].noData = false;
                    }
                }

                this.setData({
                    RorTlist: this.data.RorTlist,
                    useText: arr[res.tapIndex],
                    useStatus: id
                })
            }
        })
    },

    _Urging_vegetables() {

        tableControllor._Urging_vegetables({
            OrderNo: this.data.Tablesinfo.C_ORD_NO,
            ShopNo: this.data.ShopNo
        }, data => {
            if (data.code === "100") {
                this.setData({
                    showDialog: false,
                })
                wx.showToast({
                    title: '已催菜',
                    duration: 1000,
                })
            }
        })
    },

    cancel() {
        for (var i = 0; i < this.data.RorTlist.length; i++) {
            if (this.data.RorTlist[i].TableInfos) {
                for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                    this.data.RorTlist[i].TableInfos[j].checked = false;
                }
            }
        }

        this.setData({
            RorTlist: this.data.RorTlist,
            check: false,
        })
    },

    showClearTips() {
        wx.showModal({
            title: '提示',
            content: '确定要清桌吗',
            success: res => {
                if (res.confirm) {
                    this.clearTable();
                    wx.getStorage({
                        key: 'Tablesinfo',
                        success: res => {
                            this.removeLocalItem(res.data);
                        },
                    })
                }
            }
        })
    },

    removeLocalItem(data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].ID == this.data.Tablesinfo.ID) {
                data.splice(i, 1);
                wx.setStorage({
                    key: 'Tablesinfo',
                    data: data,
                })
            }
        }
    },

    clearTable() {
        tableControllor.clearTable({
            TableID: this.data.TableID,
            ShopNo: this.data.ShopNo,
        }, data => {
            if (data.code === "100") {
                this.setData({
                    showDialog: false,
                })
                this.getZtInfo();
                wx.showToast({
                    title: '已清桌',
                    duration: 1000
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: data.msg,
                    showCancel: false,
                })
            }
        })
    },

    billing() {
        // 清除服务员信息
        wx.setStorage({
            key: 'waiter',
            data: {},
        })
        // 清楚标签信息
        wx.setStorage({
            key: 'label',
            data: {},
        })
        // 拿到被选中的桌台
        var arr = [];
        for (var i = 0; i < this.data.RorTlist.length; i++) {
            if (this.data.RorTlist[i].TableInfos) {
                for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                    // 如果已经选中了桌台
                    if (this.data.RorTlist[i].TableInfos[j].checked) {
                        // 如果桌台有餐位费，带上餐位费
                        this.data.RorTlist[i].TableInfos[j].StoreCWF = this.data.StoreCWF || 0;
                        // 记录被选中的桌台
                        arr.push(this.data.RorTlist[i].TableInfos[j]);
                    };
                }
            }
        };

        // 记录选中的桌台的id
        var id = [];
        for (var i = 0; i < arr.length; i++) {
            id.push(arr[i].ID);
        }
        // 添加桌台操作记录
        this.data.moreTable.push({
            ids: id.join(","),
            tables: arr,
        })
        // 保存到缓存
        wx.setStorageSync("order-table", this.data.moreTable);
        // 之前的操作不变
        wx.setStorage({
            key: 'zt_info',
            data: arr,
            success: res => {
                wx.navigateTo({
                    url: '/pages/waiter/billing/billing',
                })
            }
        })
    },

    toOrder(ev) {
        var arr = [];
        wx.getStorage({
            key: 'Tablesinfo',
            success: res => {
                var flag = false;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].ID == this.data.Tablesinfo.ID) {
                        var gList = [];
                        var order = [];
                        var sublist = [];
                        if (res.data[i].gList) {
                            gList = res.data[i].gList;
                            order = res.data[i].order;
                            sublist = res.data[i].sublist;
                        }
                        this.data.Tablesinfo.gList = gList;
                        this.data.Tablesinfo.order = order;
                        this.data.Tablesinfo.sublist = sublist;
                        res.data[i] = this.data.Tablesinfo;
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    res.data.push(this.data.Tablesinfo)
                }

                this.linkToOrder(res.data);
            },
            fail: () => {
                arr.push(this.data.Tablesinfo);
                this.linkToOrder(arr);
            }
        })
    },

    linkToOrder(arr) {
        var url = "";
        if (this.data.Tablesinfo.C_MB_NO) {
            url = "&mebno=" + this.data.Tablesinfo.C_MB_NO;
        }
        wx.setStorage({
            key: 'Tablesinfo',
            data: arr,
            success: res => {
                wx.navigateTo({
                    url: '/pages/waiter/order/order?TableID=' + this.data.TableID + "&peopleSize=" + this.data.currentTable.PEOPLECOUNT + url,
                })
            }
        })
    },

    showTableChoose(ev) {
        // this.data._opt_table_type = ;
        tableControllor.getOtherTable({
            tid: this.data.currentTable.ID,
            ShopNo: this.data.ShopNo
        }, data => {
            for (var i = 0; i < data.PTables.length; i++) {
                if (data.PTables[i].NAME.length > 5) {
                    data.PTables[i].NAME2 = data.PTables[i].NAME.substr(0, 5) + ".."
                } else {
                    data.PTables[i].NAME2 = data.PTables[i].NAME
                }
                data.PTables[i].checked = false;
            }
            for (var i = 0; i < data.YTables.length; i++) {
                if (data.YTables[i].NAME.length > 5) {
                    data.YTables[i].NAME2 = data.YTables[i].NAME.substr(0, 5) + ".."
                } else {
                    data.YTables[i].NAME2 = data.YTables[i].NAME
                }
            }
            data.YTables[0].checked = true;
            this.data._old_table = data.YTables[0]
            this.setData({
                _opt_table_type: ev.currentTarget.dataset.t,
                currentTableList: data.YTables,
                changeTableList: data.PTables,
                showDialog: true,
                dType: 1,
            })
        })
    },

    /**
     * 
     */
    _set_opt_table(ev) {
        // 如果_opt_table_type等于1表示换桌，其他表示拼桌
        if (this.data._opt_table_type == 1) {
            for (var i = 0; i < this.data.changeTableList.length; i++) {
                this.data.changeTableList[i].checked = false;
            }
            this.data.changeTableList[ev.currentTarget.dataset.i].checked = true;
            this._opt_table = this.data.changeTableList[ev.currentTarget.dataset.i];
        } else {
            this.data.changeTableList[ev.currentTarget.dataset.i].checked = !this.data.changeTableList[ev.currentTarget.dataset.i].checked
        }
        this.setData({
            changeTableList: this.data.changeTableList
        })
    },

    opt_table() {
        if (this.data._opt_table_type == 1) {
            this._change_table(2208);
        } else {
            this._add_table(2206);
        }
    },

    /**
     * 换桌
     */
    _change_table(bNo) {
        var id = [];
        for (var i = 0; i < this.data.changeTableList.length; i++) {
            if (this.data.changeTableList[i].checked) {
                id.push(this.data.changeTableList[i].ID)
            }
        }
        if (id.length == 0) {
            wx.showModal({
                title: '提示',
                content: '请选择更换的桌台',
                showCancel: false,
            })
            return
        }
        wx.showLoading({
            title: '',
        })
        tableControllor._change_table({
            ShopNo: this.data.ShopNo,
            TableID: this.data._old_table.ID,
            HZTableID: id.join(","),
        }, data => {
            if (data.code === "100") {
                wx.showToast({
                    title: "已换桌",
                    duration: 1000,
                })
                this.setData({
                    showDialog: false
                })
                this.getZtInfo();
            }
        })
    },

    _add_table() {
        var id = [];
        for (var i = 0; i < this.data.changeTableList.length; i++) {
            if (this.data.changeTableList[i].checked) {
                id.push(this.data.changeTableList[i].ID)
            }
        }
        if (id.length == 0) {
            wx.showModal({
                title: '提示',
                content: '请选择需要拼的桌台',
                showCancel: false,
            })
            return
        }
        wx.showLoading({
            title: '',
        })
        tableControllor._add_table({
            ShopNo: this.data.ShopNo,
            TableID: this.data._old_table.ID,
            HZTableID: id.join(","),
        }, data => {
            if (data.code === "100") {
                wx.showToast({
                    title: "已拼桌",
                    duration: 1000,
                })
                this.setData({
                    showDialog: false
                })
                this.getZtInfo();
            }
        })
    },

    /**
     * 设置原桌台
     */
    _set_old_table(ev) {
        for (var i = 0; i < this.data.currentTableList.length; i++) {
            this.data.currentTableList[i].checked = false;
        }
        this.data.currentTableList[ev.currentTarget.dataset.i].checked = true;

        if (this.data._opt_table_type == 1) {
            this.data._old_table = ev.currentTarget.dataset.d
        }

        this.setData({
            currentTableList: this.data.currentTableList
        })
    },

    _wake_up() {
        tableControllor.wake_up({
            OrderNo: this.data.Tablesinfo.C_ORD_NO,
            ShopNo: this.data.ShopNo,
        }, data => {
            if (data.code === "100") {
                wx.showToast({
                    title: "已叫起",
                    duration: 1000,
                })
                this.setData({
                    showDialog: false
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: '叫起失败，请重试',
                    showCancel: false,
                })
            }
        })
    },

    preOrder() {
        wx.showModal({
            title: '提示',
            content: '确认要将该桌台订单设为预结账状态吗？',
            success: res => {
                if (res.confirm) {
                    tableControllor.preOrder({
                        OrderNo: this.data.Tablesinfo.C_ORD_NO,
                        ShopNo: this.data.ShopNo,
                    }, data => {
                        if (data.code === "100") {
                            this.getZtInfo()
                            wx.showToast({
                                title: "已预结帐",
                                duration: 1000,
                            })
                            this.setData({
                                showDialog: false
                            })
                        } else {
                            wx.showModal({
                                title: '提示',
                                content: '设置为预结账失败，请重试',
                                showCancel: false,
                            })
                        }
                    })
                }
            }
        })
    },

    serve_up() {
        tableControllor.serve_up({
            OrderNo: this.data.Tablesinfo.C_ORD_NO,
            ShopNo: this.data.ShopNo,
        }, data => {
            if (data.code === "100") {
                wx.showToast({
                    title: "提醒起菜成功",
                    duration: 1000,
                })
                this.setData({
                    showDialog: false
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: '提醒起菜失败，请重试',
                    showCancel: false,
                })
            }
        })
    },

    mimeograph() {
        tableControllor.mimeograph({
            OrderNo: this.data.Tablesinfo.C_ORD_NO,
            ShopNo: this.data.ShopNo,
        }, data => {
            if (data.code === "100") {
                wx.showToast({
                    title: "已补打",
                    duration: 1000,
                })
                this.setData({
                    showDialog: false
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: '补打失败，请重试',
                    showCancel: false,
                })
            }
        })
    },

    showOrderDetail() {
        wx.navigateTo({
            url: '/pages/waiter/orderDetail/orderDetail?TableId=' + this.data.Tablesinfo.ID + "&OrderNo=" + this.data.Tablesinfo.C_ORD_NO + "&tname=" + this.data.Tablesinfo.NAME,
        })
    },

    queryTableInfo(ev) {
        wx.showLoading({
            title: '加载中',
        })
        var status = ev.currentTarget.dataset.s;
        var aIndex = ev.currentTarget.dataset.a;
        var zIndex = ev.currentTarget.dataset.z;
        var id = ev.currentTarget.dataset.i

        if (status != 0) {
            tableControllor.queryTable({
                TID: id,
                ShopNo: this.data.ShopNo,
            }, data => {
                data.status = status;
                data.aIndex = aIndex;
                data.zIndex = zIndex;
                data.zt = ev.currentTarget.dataset.de;
                console.log(data.zt);
                wx.hideLoading()
                this.chooseThisZt(data);
            })
        } else {
            wx.hideLoading()
            this.chooseThisZt({
                zt: ev.currentTarget.dataset.de,
                aIndex,
                zIndex,
                status,
            });
        }
    },

    chooseThisZt(data) {
        var status = data.status;
        var aIndex = data.aIndex;
        var zIndex = data.zIndex;
        var ztStatus = "已开桌";
        if (status != 0) {
            if (data.Tnames && data.Tnames.indexOf(",") != -1) {
                ztStatus = "拼桌中"
            }
            data.Tablesinfo.STARTTIME = data.Tablesinfo.STARTTIME.replace("T", " ");
        } else {
            data.Tablesinfo = {};
        }

        // 设置当前桌台信息
        this.setData({
            currentTable: data.zt,
            zt: data,
            Tablesinfo: data.Tablesinfo,
            ztStatus,
        })
        this.data.TableID = data.zt.ID;
        var check = false;
        var arr = [];
        if (status == 0) {
            this.data.RorTlist[aIndex].TableInfos[zIndex].checked = !this.data.RorTlist[aIndex].TableInfos[zIndex].checked;
            var flag = false;
            // 判定是否有被选择的桌台
            // 如果有任何一个桌台被选中，则显示开单菜单
            for (var i = 0; i < this.data.RorTlist.length; i++) {
                if (this.data.RorTlist[i].TableInfos) {
                    for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                        if (this.data.RorTlist[i].TableInfos[j].checked) {
                            flag = true;
                            break;
                        };
                    }
                }
            };
            for (var i = 0; i < this.data.RorTlist.length; i++) {
                if (this.data.RorTlist[i].TableInfos) {
                    for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                        if (this.data.RorTlist[i].TableInfos[j].checked) {
                            arr.push(this.data.RorTlist[i].TableInfos[j]);
                        };
                    }
                }
            };
            if (!flag) {
                // 如果没有一个桌台被选中，则隐藏开单菜单
                wx.setStorage({
                    key: 'zt_info',
                    data: [],
                })
                check = false;
            } else {
                // wx.setStorage({
                //   key: 'zt_info',
                //   data: arr,
                // })
                check = true;
            }
        } else {
            if (this.data.check) {
                wx.setStorage({
                    key: 'zt_info',
                    data: [],
                })
                for (var i = 0; i < this.data.RorTlist.length; i++) {
                    if (this.data.RorTlist[i].TableInfos) {
                        for (var j = 0; j < this.data.RorTlist[i].TableInfos.length; j++) {
                            this.data.RorTlist[i].TableInfos[j].checked = false;
                        }
                    }
                };
            }

            switch (status) {
                case 1:
                    this.setData({
                        showDialog: true,
                        dType: 0,
                    })
                    break;
                case 2:
                    this.setData({
                        showDialog: true,
                        dType: 2,
                    })
                    break;
                case 3:
                    this.setData({
                        showDialog: true,
                        dType: 3,
                    })
                    break;
                case 4:
                    this.setData({
                        showDialog: true,
                        dType: 2,
                    })
                    break;
                case 5:
                    this.setData({
                        showDialog: true,
                        dType: 3,
                    })
                    break;
            }
        }
        this.setData({
            check,
            RorTlist: this.data.RorTlist,
        })
    },

    /**
     * 阻止冒泡的空函数
     */
    def() {

    },

    hideDialog() {
        this.setData({
            showDialog: false,
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        wx.getStorage({
            key: 'C_SHOP_NO',
            success: res => {
                this.data.ShopNo = res.data;
                this.getZtInfo();
            },
        })

        wx.getStorage({
            key: 'OP_INFO',
            success: res => {
                this.setData({
                    opInfo: res.data,
                })
            },
        })

        /**
         * 获取开了多个桌台但是没点菜的桌台数据
         */
        wx.getStorage({
            key: 'order-table',
            success: res => {
                // 存储之前的操作记录
                this.data.moreTable = res.data;
            },
        })

        wx.getStorage({
            key: 'Tablesinfo',
            success: res => {
                this.data.tableOptRecord = res.data;
            },
        })
    },

    onPullDownRefresh() {
        this.getZtInfo("pull-down");
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 500)
    },

    showCheckChoose() {
        wx.showActionSheet({
            itemList: ["微信支付", "支付宝支付", "现金支付", "收银台预结账"],
            success: res => {
                switch (res.tapIndex) {
                    case 0:
                        this.getOrderInfo(1);
                        this.setData({
                            payType: 1,
                        })
                        break;
                    case 1:
                        this.getOrderInfo(3);
                        this.setData({
                            payType: 2,
                        })
                        break;
                    case 2:
                        this.getOrderInfo(2);
                        this.setData({
                            zlAmount: 0,
                            inputAmount: "",
                            cashPay: true,
                        })
                        break;
                    case 3:
                        this.preOrder();
                        break;
                    default:
                        break
                }
            }
        })
    },

    setPayByCash() {
        if (!this.data.inputAmount) {
            wx.showModal({
                title: '提示',
                content: '请填写收款金额',
                showCancel: false
            })
            return;
        }
        if (parseFloat(this.data.inputAmount) < parseFloat(this.data.rechargeAmount)) {
            wx.showModal({
                title: '提示',
                content: '收款金额不能小于订单金额',
                showCancel: false,
            })

            return;
        }
        // wx.showLoading({
        //     title: '',
        // })
        var str = `orderNo&payWay&zfls&payAmount=${this.data.Tablesinfo.C_ORD_NO}*11**${this.data.rechargeAmount}`;
        var md5 = util.md5(str);
        var reqData = {
            bNo: 412,
            orderNo: this.data.Tablesinfo.C_ORD_NO,
            payWay: 11,
            zfls: "",
            payAmount: this.data.rechargeAmount,
            opid: this.data.opInfo.n_ss_id,
            MD5: md5.toUpperCase()
        };

        wx.request({
            url: config.requestUrl,
            data: reqData,
            success: res => {
                var data2 = JSON.parse(res.data.replace(/\(|\)/g, ""));
                if (data2.code === "100") {
                    this.cancelCash();
                    wx.showToast({
                        title: '收款成功',
                        mask: true,
                        success: res => {
                            setTimeout(() => {
                                this.changeTableStatus(this.data.Tablesinfo.C_ORD_NO);
                            }, 1000)
                        }
                    })
                } else {
                    wx.showToast({
                        title: '收款失败',
                        duration: 1000,
                    })
                }
                wx.hideLoading()
            }
        })
    },

    setCashAmount(ev) {
        var val = ev.detail.value;
        if (/\./.test(val)) {
            try {
                val = val.match(/\d+\.\d{0,2}/g)[0]
            } catch(e) {
                // val = parseFloat(val)
            } 
        }

        var zlAmount = parseFloat(ev.detail.value) - this.data.rechargeAmount;
 
        this.setData({
            inputAmount: val,
            zlAmount: zlAmount.toFixed(2)
        })
    },

    getOrderInfo(ptype) {
        var str = `ShopNo&OrderNo=${this.data.ShopNo}*${this.data.Tablesinfo.C_ORD_NO}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4004,
            ShopNo: this.data.ShopNo,
            OrderNo: this.data.Tablesinfo.C_ORD_NO,
            MD5: md5.toUpperCase()
        };

        request(data, (data) => {
            if (data.code === "100") {
                this.setData({
                    rechargeAmount: data.PayAmount,
                })
                if (ptype != 2) {
                    this.checkOut(ptype, data.data);
                }
            }
        })
    },

    hideEwmDialog() {
        wx.showModal({
            title: '提示',
            content: '确认放弃收款吗？',
            success: res => {
                if (res.confirm) {
                    this.setData({
                        showDialog: false,
                        showEwm: false
                    })
                    clearInterval(timer);
                }
            }
        })
    },

    cancelCash() {
        this.setData({
            cashPay: false,
        })
    },

    checkOut(ptype, data) {
        wx.showLoading({
            title: '获取中',
        })
        var data = {
            bNo: 3206,
            PayAmount: this.data.rechargeAmount,
            PayWay: ptype,
            ShopNo: this.data.ShopNo,
            OrderNo: this.data.Tablesinfo.C_ORD_NO
        }
        request(data, (data) => {
            if (data.code === "100") {
                if (data.imgurl) {
                    this.setData({
                        showEwm: true,
                        payPic: data.imgurl,
                    })
                    this.checkPayResult(this.data.Tablesinfo.C_ORD_NO, data.CxONo, ptype)
                } else {
                    wx.showModal({
                        title: '提示',
                        content: data.Result,
                        showCancel: false,
                    })
                };
            } else {
                wx.showModal({
                    title: '提示',
                    content: '获取支付信息失败',
                    showCancel: false,
                })
            }
            wx.hideLoading();
        })
    },

    checkPayResult(orderNo, CxONo, type) {
        var data = {
            bNo: 3207,
            OrderNo: orderNo,
            CxONo,
            PayLy: 1,
            PayAmount: 0.01,
            PayWay: type,
            opid: this.data.opInfo.n_ss_id,
        }
        timer = setInterval(() => {
            request(data, msg => {
                if (msg.code === '100') {
                    clearInterval(timer);
                    this.changeTableStatus(orderNo);
                }
            })

        }, 2000);
    },

    changeTableStatus(orderNo) {
        var str = `OrderNo&TID&ShopNo=${orderNo}*${this.data.Tablesinfo.ID}*${this.data.ShopNo}`
        var data = {
            bNo: 2218,
            OrderNo: orderNo,
            TID: this.data.Tablesinfo.ID,
            ShopNo: this.data.ShopNo,
            MD5: util.md5(str).toUpperCase()
        }

        request(data, suc => {
            if (suc.code === "100") {
                wx.showToast({
                    title: '已收款',
                    success: res => {
                        this.getZtInfo();
                        this.setData({
                            showEwm: false,
                            showDialog: false,
                        })
                    }
                })
            }
        })
    },

    onHide() {
        this.setData({
            showDialog: false,
            check: false,
        })
    },
})