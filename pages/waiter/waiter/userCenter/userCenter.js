var g = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    shopInfo: {},
    err: false,
    errMessage: "",
    navIndex: 4,
    showChange: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      version: g.version
    })
    wx.getUserInfo({
      success: res=> {
        this.setData({
          userInfo: res.userInfo
        })
      }
    })

    wx.getStorage({
      key: 'shop_data',
      success: res => {
        this.setData({
          shopInfo: res.data
        })
      },
    })
  },

  myAchievement() {
    wx.navigateTo({
      url: '/pages/staffManage/myAchievement/myAchievement',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  changeRole() {
    wx.navigateTo({
      url: '/pages/roleControllor/roleControllor',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'USER_DATA',
      success: res => {
        this.setData({
          userInfo: res.data,
        })
        console.log(res.data);
      },
      complete: msg => {
        console.log(msg);
      }
    })

    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        if (res.data.n_isservice == 0) {
          this.setData({
            showChange: true,
          })
        }
      },
    })
  },

  logOut () {
    wx.showModal({
      title: '提示',
      content: '确认退出当前账号吗？',
      success: res => {
        if (res.confirm) {
          wx.removeStorage({
            key: 'OP_INFO',
            success: function (res) { },
          })
          wx.redirectTo({
            url: '/pages/login/login',
          })
        }
      }
    })
  },

  hideDialog() {
    this.setData({
      err: false,
    })
  },

  sureOpt () {
    
  },

  /**
   * 修改密码
   */
  editPassword () {
    // var str = "ShopNo&StaffId&NewPwd=" + shopno + "*" + staffid + "*" + NewPwd,
    //     md5 = util.md5(str),
    //     _this = this,
    //     data = {

    //     };
    wx.navigateTo({
      url: '/pages/editPassword/editPassword',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})