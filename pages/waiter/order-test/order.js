// pages/waiter/order/order.js
var util = require("../../../utils/MD5.js")
var request = require("../../../utils/request.js")
var g = require("../../../utils/goodsCommon.js");
var config = require("../../../config.js");
var goodsControllor = require("../../../utils/goodsControllor.js");
var timer = null;
Page({
    data: {
        noData: false,
        ztGoods: [],
        cSublist: [],
        lptcdt: null,
        showCancelMsg: false, // 是否显示退菜窗口
        firstGlist: [], // 商品列表的初始值
        // spec: 5, // 当前展示的弹窗选项卡
        spec: 4,
        getStatus: true, // 商品的请求状态
        shopNo: "", // 店铺编号
        opInfo: {}, // 操作人员信息
        CateId: "", // 当前商品分类的id
        classify: [], // 商品分类数据
        gList: [], // 全局商品数据
        pageindex: 1, // 商品分页当前页
        pagesize: 20, // 商品分页具体数量
        goodsList: [], // 商品列表
        currentGoodsNum: 0, // 当前商品的数量
        currentGoodsIndex: 0, // 当前编辑的商品的下标
        mebno: 0, // 会员编号
        detailCover: false, // 是否显示弹窗
        dialogType: 0, // 弹窗类型
        Batchings: [], // 加料
        PracticeOrType: [], // 做法
        ysdf: false, // 是否开启一式多份
        Remarks: [], // 备注
        Tablesinfo: [], // 本地的桌台数据
        currentTable: {}, // 当前桌的信息
        subGoods: [], // 需要提交的商品信息
        sublist: [], // 需要提交的做法信息
        deleteReson: ["服务员误操作", "等待时间过长", "商品质量问题", "多点的，不需要了"],
        deteleIndex: -1,
        tableInfoList: {},
        tableOrderInfo: [],
        goodsWithTableId: [],
        OrderNo: "",
        TableID: "",
        Spv1: 0,
        Spv2: 0,
        Spv3: 0,
        allowSale: true,
        totalPrice: 0, // 总价
        showMoreType: false,
        totalNum: 0, // 总量
        cTotalPrice: 0, // 总价
        cTotalNum: 0, // 总量
        orderGoods: [],
        gbase: [],
        PeopleCount: 0,
        delIndex: [], // 删菜的下标
        totalPage: 0,
        noTable: false,
        showEwm: false,
        previewImage: false
    },
    onLoad: function(options) {
        console.log(options)
        if (options.peopleSize) {
            this.data.PeopleCount = options.peopleSize;
        }
        if (options.mebno) {
            this.data.mebno = options.mebno;
        }
        this.data.TableId = options.TableID ? options.TableID : "";
        wx.getSystemInfo({
            success: res => {
                this.setData({
                    CH: res.windowHeight - 150
                })
            },
        })
        if (options.order) {
            this.data.noTable = true;
        }
    },
    onShow: function () {
        if (!this.data.previewImage) {
            this.setData({
                tableOrderInfo: [],
            })
            wx.getStorage({
                key: 'C_SHOP_NO',
                success: res => {
                    this.data.shopNo = res.data;
                    this.getSureOrderConfig();
                    // 获取桌台的操作记录
                    wx.getStorage({
                        key: 'order-table',
                        success: rs => {
                            for (var i = 0; i < rs.data.length; i++) {
                                // 匹配操作记录中当前的操作桌台
                                if (rs.data[i].ids.indexOf(this.data.TableId) != -1) {
                                    // 把之前选中的桌台保存下来，方便之后下单的时候使用
                                    this.setData({
                                        billingTable: rs.data[i].tables
                                    })
                                }
                            }
                            this.getGoodsClassify();
                        },
                    })
                },
            });

            wx.getStorage({
                key: 'OP_INFO',
                success: res => {
                    this.setData({
                        opInfo: res.data,
                    })
                },
            })
        } else {
            this.data.previewImage = false
        }
    },

    hideEwmDialog() {
        wx.showModal({
            title: '提示',
            content: '确认放弃收款吗？',
            success: res => {
                if (res.confirm) {
                    this.setData({
                        showDialog: false,
                        showEwm: false
                    })
                    clearInterval(timer);
                }
            }
        })
    },

    getZtInfo() {
        goodsControllor.getZtInfo({
            TID: this.data.TableId,
            ShopNo: this.data.shopNo
        }, data => {
            this.setData({
                tableInfoList: data.GoodsList[0]
            })
        })
    },

    

    getSureOrderConfig() {
        var str = `Setlx&ShopNo=40*${this.data.shopNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 429,
            Setlx: 40,
            ShopNo: this.data.shopNo,
            MD5: md5.toUpperCase()
        }

        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                if (res.data.setValue) {
                    if (res.data.setValue != 1) {
                        this.setData({
                            allowSale: false,
                        })
                    }
                }
            }
        })
    },

    corder(opt, dt) {
        if (!this.data.subGoods || this.data.subGoods.length == 0) {
            wx.showModal({
                title: '提示',
                content: '请先选菜',
                showCancel: false,
            })
            return;
        }
        if (this.data.cart.length == 0) {
            for (var i = 0; i < this.data.subGoods.length; i++) {
                if (this.data.subGoods[i].Delstr == "") {
                    wx.showModal({
                        title: '提示',
                        content: '请先选菜',
                        showCancel: false,
                    })
                    break;
                    return;
                }
            }
        };
        if (this.data.selectNumIndex){
            this.data.subGoods[dt].N_NUM = this.data.selectNumArr[this.data.selectNumIndex]
        }
        for (var i = 0; i < this.data.subGoods.length; i++) {
            for (var j = 0; j < this.data.sublist.length; j++) {
                if (this.data.subGoods[i].N_GB_ID == this.data.sublist[j].GoodsID) {
                    this.data.sublist[j].CurIndex = j + 1;
                    this.data.sublist[j].MaxXh = this.data.subGoods.length;
                }
            }
        }

        // 如果需要一式多份（只有多桌台开桌点菜的时候才会出现一式多份）
        if (this.data.showMoreType) {
            // 如果开启一式多份
            if (this.data.ysdf) {
                // 记录带有桌台id的菜单
                var arrs = [];
                for (var i = 0; i < this.data.billingTable.length; i++) {
                    for (var j = 0; j < this.data.subGoods.length; j++) {
                        // 将原对象的值复制出来创建新对象，
                        // 并且把桌台id设置到新对象
                        var temp = Object.assign({}, this.data.subGoods[j], {
                            N_TableID: this.data.billingTable[i].ID
                        });
                        // 添加到菜单
                        arrs.push(temp);
                    }
                }
                // 重新设置提交列表
                this.data.subGoods = arrs;
                var zi = 1;
                var ci = 1;
                // 去掉提交列表中关于桌台与餐位费的信息
                for (var k = 0; k < this.data.subGoods.length; k++) {
                    if (this.data.subGoods[k].G_CODE == "thiszt") {
                        if (zi == 1) {
                            zi = 2;
                        } else {
                            this.data.subGoods.splice(k, 1);
                            k--
                        }
                    }
                    if (this.data.subGoods[k].G_CODE == "thiscwf") {
                        if (ci == 1) {
                            ci = 2;
                        } else {
                            this.data.subGoods.splice(k, 1);
                            k--
                        }
                    }
                }
            } else {
                // 如果不开启一式多份，那么只给当前桌台点菜，其他桌台不管
                for (var j = 0; j < this.data.subGoods.length; j++) {
                    this.data.subGoods[j].N_TableID = this.data.currentTable.ID
                }
            }
        }

        // 合并数组
        this.data.subGoods = this.data.subGoods.concat(this.data.ztGoods);

        wx.showLoading({
            title: '加载中',
            mask: true,
        })

        // 如果不是一式多份（可能是加菜）
        if (!this.data.showMoreType) {
            for (var j = 0; j < this.data.subGoods.length; j++) {
                // 如果新加的菜中没有桌台id，
                // 添加桌台id
                if (!this.data.subGoods[j].N_TableID) {
                    this.data.subGoods[j].N_TableID = this.data.currentTable.ID
                }
            }
        }
        for (var i = 0; i < this.data.subGoods.length; i++) {
            this.data.subGoods[i].MaxXh = i + 1;
        }
        var Addstr = [];
        if (opt && opt == "del-food") {
            Addstr = "";
            OrderNo = this.data.currentTable.C_ORD_NO;
        } else {
            Addstr = [];
            var OrderNo = "";
            if (this.data.currentTable.C_ORD_NO) {
                for (var i = this.data.orderGoods.length; i < this.data.subGoods.length; i++) {
                    Addstr.push(i + 1);
                }
                OrderNo = this.data.currentTable.C_ORD_NO;
            }
        };
        console.log('this.data.subGoods------------------')
        console.log(this.data.subGoods)
        // 如果已有订单 
        if (this.data.currentTable.C_ORD_NO) {
            var flag = false;
            for (var i = 0; i < this.data.subGoods.length; i++) {
                if (this.data.subGoods[i].G_CODE == "thiszt") {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                for (var i = 0; i < this.data.tableOrderInfo.length; i++) {
                    this.data.subGoods.push(this.data.tableOrderInfo[i]);
                }
            }
            // 如果不是取消退菜
            if (opt != "add-cancel-goods") {
                // if (opt && opt == "del-food") {
                // 给提交的商品列表加上桌台信息，
                // 取消退菜不加是因为退菜单独提交，本页面没有刷新之前列表已经存在桌台信息
            } else {
                Addstr = dt + 1
            }
        }
        console.log('glist --------------')
        console.log(this.data.subGoods)
        var str = `ShopNo&mebNo&sublist&opid&OrderNo=${this.data.shopNo}*${this.data.mebno}*${JSON.stringify(this.data.sublist)}*${this.data.opInfo.n_ss_id}*${OrderNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4003,
            opid: this.data.opInfo.n_ss_id,
            OrderNo,
            PeopleCount: this.data.PeopleCount,
            mebNo: this.data.mebno,
            glist: JSON.stringify(this.data.subGoods),
            sublist: JSON.stringify(this.data.sublist),
            Addstr,
            Upstr: "",
            Delstr: this.data.delIndex.join(","),
            ReDelstr: "",
            ShopNo: this.data.shopNo,
            TID: this.data.currentTable.ID || 0,
            MD5: md5.toUpperCase()
        };
        console.log('data-----------')
        console.log(data)
        request(data, (data) => {
            if (data.code === "100") {
                // 如果不是直接点餐，修改桌台相应信息
                if (!this.data.noTable) {
                    if (opt && opt == "del-food") {
                        var title = "已退菜"
                        wx.showToast({
                            title,
                            duration: 1000,
                            success: res => {
                                setTimeout(() => {
                                    wx.navigateBack({
                                        delta: 1,
                                    })
                                }, 1000)
                            }
                        })
                    } else {
                        // 暂时不用调用回写方法，
                        // 修改订单状态的接口与下单接口已经合并
                        // this.changeTableStatus(data.OrderNo);
                        for (var i = 0; i < this.data.Tablesinfo.length; i++) {
                            if (this.data.Tablesinfo[i].ID == this.data.TableId) {
                                this.data.Tablesinfo[i].gList = this.data.firstGlist;
                                this.data.Tablesinfo[i].order = this.data.subGoods;
                                this.data.Tablesinfo[i].sublist = this.data.sublist;
                            }
                        }
                        wx.setStorage({
                            key: 'Tablesinfo',
                            data: this.data.Tablesinfo,
                        })
                        var title = data.msg;
                        dt = dt + "";
                        if (dt && dt != 'undefined') {
                            title = "已取消退菜"
                        }
                        wx.showToast({
                            title,
                            duration: 1000,
                            success: res => {
                                if (opt != "add-cancel-goods") {
                                    setTimeout(() => {
                                        wx.navigateBack({
                                            delta: 1,
                                        })
                                    }, 1000)
                                }
                            }
                        })
                    }
                    // wx.hideLoading();
                } else {
                    // 如果是直接点餐，直接重置桌台以待下次使用
                    // wx.showToast({
                    //   title: data.msg,
                    // })
                    wx.hideLoading();
                    this.showCheckChoose(data.OrderNo);
                }
            } else {
                wx.showModal({
                    title: '提示',
                    content: data.msg,
                    showCancel: false,
                })
                wx.hideLoading();
            }
        }, "POST")
    },

    showCheckChoose(OrderNo) {
        wx.showActionSheet({
            itemList: ["微信支付", "支付宝支付", "现金支付"],
            success: res => {
                switch (res.tapIndex) {
                    case 0:
                        this.getOrderInfoPay(1, OrderNo);
                        this.setData({
                            payType: 1,
                        })
                        break;
                    case 1:
                        this.getOrderInfoPay(3, OrderNo);
                        this.setData({
                            payType: 2,
                        })
                        break
                    case 2:
                        this.getOrderInfoPay(2, OrderNo);
                        break;
                }
            }
        })
    },

    regCash(OrderNo) {
        wx.showLoading({
            title: '加载中',
        })
        var str = `orderNo&payWay&zfls&payAmount=${OrderNo}*11**${this.data.rechargeAmount}`;
        var md5 = util.md5(str);
        var reqData = {
            bNo: 412,
            orderNo: OrderNo,
            payWay: 11,
            zfls: "",
            payAmount: this.data.rechargeAmount,
            opid: this.data.opInfo.n_ss_id,
            MD5: md5.toUpperCase()
        };

        wx.request({
            url: config.requestUrl,
            data: reqData,
            success: res => {
                var data2 = JSON.parse(res.data.replace(/\(|\)/g, ""));
                if (data2.code === "100") {
                    wx.showToast({
                        title: '收款成功',
                        mask: true,
                        success: res => {
                            this.resetOrderSystem();
                        }
                    })
                } else {
                    wx.showToast({
                        title: '收款失败',
                        duration: 1000,
                    })
                }
                wx.hideLoading()
            }
        })
    },

    getOrderInfoPay(ptype, OrderNo) {
        var str = `ShopNo&OrderNo=${this.data.shopNo}*${OrderNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4004,
            ShopNo: this.data.shopNo,
            OrderNo: OrderNo,
            MD5: md5.toUpperCase()
        };

        request(data, (data) => {
            if (data.code === "100") {
                this.setData({
                    rechargeAmount: data.PayAmount
                })
                if (ptype != 2) {
                    this.checkOut(ptype, data.data, OrderNo);
                } else {
                    this.regCash(OrderNo)
                }
            }
        })
    },

    /**
     * 获取收款二维码
     */
    checkOut(ptype, data, OrderNo) {
        wx.showLoading({
            title: '获取中',
        })
        var data = {
            bNo: 3206,
            PayAmount: this.data.rechargeAmount,
            PayWay: ptype,
            ShopNo: this.data.shopNo,
            OrderNo: OrderNo
        }
        request(data, (data) => {
            if (data.code === "100") {
                if (data.imgurl) {
                    this.setData({
                        showEwm: true,
                        payPic: data.imgurl,
                    })
                    this.checkPayResult(OrderNo, data.CxONo, ptype)
                } else {
                    wx.showModal({
                        title: '提示',
                        content: data.Result,
                        showCancel: false,
                    })
                };
            } else {
                wx.showModal({
                    title: '提示',
                    content: '获取支付信息失败',
                    showCancel: false,
                })
            }
            wx.hideLoading();
        })
    },

    /**
     * 查询支付结果
     */
    checkPayResult(orderNo, CxONo, type) {
        var data = {
            bNo: 3207,
            OrderNo: orderNo,
            CxONo,
            PayAmount: 0.01,
            PayWay: type,
            opid: this.data.opInfo.n_ss_id,
        }
        timer = setInterval(() => {
            request(data, msg => {
                if (msg.code === '100') {
                    clearInterval(timer);
                    if (this.data.showEwm) {
                        wx.showToast({
                            title: '已收款',
                        })
                    }
                    this.setData({
                        showEwm: false,
                    })
                    this.resetOrderSystem();
                }
            })
        }, 2000);
    },

    /**
     * 重置点餐系统，主要用在快餐模式
     */
    resetOrderSystem() {
        for (var i = 0; i < this.data.goodsList.length; i++) {
            this.data.goodsList[i].num = 0;
        }
        this.setData({
            gList: this.data.firstGlist,
            goodsList: this.data.goodsList
        })
        this.setClassNum();
        this.setSubmitGoods();
    },

    /**
     * 设置商品状态选择
     */
    setGoodsStatus(ev) {
        var data = ev.currentTarget.dataset.d; //商品详情
        let selectNumArr = []
        for (let i = 1; i <= data.N_NUM; i++) {
            selectNumArr.push(i.toString())
        }
        var arr = ["退菜", "优惠"];
        if (data.GZT == 2) {
            arr[0] = "取消退菜"
        };
        wx.showActionSheet({
            itemList: arr,
            success: res => {
                if (res.tapIndex == 0) {
                    var idx = "";
                    var gInfo = this.data.gbase[ev.currentTarget.dataset.i];
                    for (var i = 0; i < this.data.subGoods.length; i++) {
                        if (this.data.subGoods[i].N_GOODS_ID == data.N_GOODS_ID) {
                            idx = i;
                            break
                        }
                    }
                    if (arr[0] == "退菜") {
                        this.setData({
                            selectNumArr: selectNumArr,
                            selectNumIndex: selectNumArr.length - 1,
                        })
                        // wx.showModal({
                        //     title: '提示',
                        //     content: '小程序暂时不支持部分退菜，确认则退当前菜品的所有菜。如要部分退菜请到收银台，是否继续？',
                        //     success: res => {
                        //         if (res.confirm) {
                        //             for (var i = 0; i < this.data.subGoods.length; i++) {
                        //                 if (this.data.subGoods[i].N_GOODS_ID == data.N_GOODS_ID) {
                        //                     if (this.data.TableId == this.data.subGoods[i].N_TableID) {
                        //                         if (this.data.subGoods[i].GZT == 2) {
                        //                             continue;
                        //                         } else {
                        //                             this.data.subGoods[i].GZT = 2;
                        //                             break;
                        //                         }
                        //                     }
                        //                 }
                        //             }
                        //             this.setData({
                        //                 showCancelMsg: true,
                        //                 deteleIndex: -1,
                        //                 deleteMessage: "",
                        //             })
                        //             this.data.currentDelIndex = ev.currentTarget.dataset.i;
                        //             this.data.currentDeleteGoods = ev.currentTarget.dataset.d
                        //         }
                        //     }
                        // })
                        for (var i = 0; i < this.data.subGoods.length; i++) {
                            if (this.data.subGoods[i].N_GOODS_ID == data.N_GOODS_ID) {
                                if (this.data.TableId == this.data.subGoods[i].N_TableID) {
                                    if (this.data.subGoods[i].GZT == 2) {
                                        continue;
                                    } else {
                                        this.data.subGoods[i].GZT = 2;
                                        break;
                                    }
                                }
                            }
                        }
                        this.setData({
                            showCancelMsg: true,
                            deteleIndex: -1,
                            deleteMessage: "",
                        })
                        this.data.currentDelIndex = ev.currentTarget.dataset.i;
                        this.data.currentDeleteGoods = ev.currentTarget.dataset.d
                        console.log(this.data)
                    } else {
                        this.data.totalNum = this.data.totalNum + parseFloat(gInfo.N_NUM);
                        this.data.totalPrice = parseFloat(this.data.totalPrice) + parseFloat(gInfo.N_REALPRICE) + parseFloat(gInfo.N_GoodsFeedJe);
                        this.data.gbase[ev.currentTarget.dataset.i].GZT = 0;
                        for (var i = 0; i < this.data.subGoods.length; i++) {
                            if (this.data.subGoods[i].N_GOODS_ID == data.N_GOODS_ID) {
                                if (this.data.TableId == this.data.subGoods[i].N_TableID) {
                                    this.data.subGoods[i].GZT = 0;
                                    this.data.subGoods[i].C_GoodsRemark = "";
                                }
                            }
                        }
                        // 把取消退菜的商品重新添加到提交列表当作加菜单处理
                        // this.data.subGoods.push(this.data.subGoods[idx]);
                        // 调用提交方法，声明本次为取消退菜操作
                        this.setData({
                            gbase: this.data.gbase,
                            totalPrice: this.data.totalPrice.toFixed(2),
                            totalNum: this.data.totalNum,
                        })
                        this.corder("add-cancel-goods", idx)
                    }
                }
            }
        })
    },
    // 选择退菜数量
    bindPickerChange(e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            selectNumIndex: e.detail.value.toString()
        })
    },

    closeDelete() {
        this.setData({
            showCancelMsg: false,
        })
    },

    setDeleteMessage(ev) {
        this.setData({
            deteleIndex: -1,
            deleteMessage: ev.detail.value
        })
    },

    setDeleteText(ev) {
        this.setData({
            deleteMessage: "",
            deteleIndex: ev.currentTarget.dataset.i,
        })
        this.data.deleteMessage = ev.currentTarget.dataset.t
    },

    sureDelete() {
        if (!this.data.deleteMessage) {
            wx.showModal({
                title: '提示',
                content: '请选择或者填写退菜原因',
                showCancel: false,
            })
            return;
        }
        var gInfo = this.data.gbase[this.data.currentDelIndex];
        this.data.delIndex = [];
        for (var i = 0; i < this.data.subGoods.length; i++) {
            if (this.data.subGoods[i].N_GOODS_ID == this.data.currentDeleteGoods.N_GOODS_ID) {
                if (this.data.TableId == this.data.subGoods[i].N_TableID) {
                    if (this.data.subGoods[i].GZT == 2) {
                        this.data.subGoods[i].C_GoodsRemark = this.data.deleteMessage
                    }
                }
            }
        }
        if (this.data.gbase[this.data.currentDelIndex].GZT != 2) {
            this.data.delIndex.push(parseInt(this.data.currentDelIndex) + 1);
        } else {
            for (var i = 0; i < this.data.delIndex.length; i++) {
                if (this.data.delIndex == this.data.currentDelIndex) {
                    this.data.delIndex.splice(i, 1)
                }
            }
        }
        this.data.totalNum = this.data.totalNum - parseFloat(gInfo.N_NUM);
        this.data.totalPrice = parseFloat(this.data.totalPrice) - parseFloat(gInfo.N_REALPRICE) - parseFloat(gInfo.N_GoodsFeedJe);
        this.data.gbase[this.data.currentDelIndex].GZT = this.data.gbase[this.data.currentDelIndex].GZT == 2 ? 0 : 2;
        this.setData({
            totalPrice: this.data.totalPrice.toFixed(2),
            totalNum: this.data.totalNum,
            showCancelMsg: false,
        })
        console.log(this.data.currentDelIndex)
        this.corder("del-food", this.data.currentDelIndex);
        this.data.delIndex = [];
    },

    /**
     * 下单成功
     * 改变订单状态
     */
    changeTableStatus(OrderNo) {
        var str = `OrderNo&TID&ShopNo=${OrderNo}*${this.data.TableId}*${this.data.shopNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 2217,
            OrderNo,
            TID: this.data.TableId,
            ShopNo: this.data.shopNo,
            MD5: md5.toUpperCase()
        };

        request(data, (data) => {
            if (data.code === "100") {
                wx.showToast({
                    title: "下单成功",
                    duration: 1000,
                    success: res => {
                        setTimeout(() => {
                            wx.navigateBack({
                                delta: 1,
                            })
                        }, 1000)
                    }
                })
            }
        })
    },

    sureGoods() {
        var goods = {};
        if (this.data.currentGoodsNum == 0) {
            this.data.currentGoodsNum = 1;
        }

        var f = false;

        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.goodsList[i].N_GB_ID == this.data.currentGoods.N_GB_ID) {
                if (this.data.goodsList[i].ID == this.data.currentGoods.ID) {
                    this.data.goodsList[i].num = this.data.currentGoodsNum;
                    this.data.goodsList[i].SPECIFICATION = this.data.currentGoods.SPECIFICATION;
                    goods = this.data.goodsList[i];
                    f = true;
                    break;
                }
            }
        };

        this.data.C_GoodsRemark2 = "";

        if (!f) {
            goods = this.data.currentGoods;
            goods.currentPractice = this.data.currentPractice;
            goods.PracticeOrType = this.data.PracticeOrType;
            goods.Batchings = this.data.Batchings;
            goods.Remarks = this.data.Remarks;
            goods.num = this.data.currentGoodsNum;
        }
        if (this.data.C_GoodsRemark) {
            goods.C_GoodsRemark = this.data.C_GoodsRemark;
        } else {
            goods.C_GoodsRemark = "";
        }
        for (var i = 0; i < this.data.gList.length; i++) {
            if (this.data.gList[i].cid == this.data.CateId) {
                if (this.data.gList[i].gList.length > 0) {
                    var flag = false;
                    for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                        if (this.data.gList[i].gList[j].ID == goods.ID) {
                            this.data.gList[i].gList[j] = goods;
                            flag = true;
                            break;
                        }
                    }

                    if (!flag) {
                        this.data.gList[i].gList.push(goods);
                    }
                } else {
                    this.data.gList[i].gList.push(goods);
                }
            }
        }
        this.setClassNum();
        this.hideDialog();
        this.setSubmitGoods();
        this.data.totalPrice = parseFloat(this.data.totalPrice).toFixed(2)
        this.setData({
            totalPrice: this.data.totalPrice,
            goodsList: this.data.goodsList,
        })
    },

    /**
     * 切换规格
     */
    changeAttr(ev) {
        var index = ev.currentTarget.dataset.i;
        var sIndex = ev.currentTarget.dataset.si;

        switch (index) {
            case 0:
                this.data.Spv1 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
            case 1:
                this.data.Spv2 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
            case 2:
                this.data.Spv3 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
        };

        this.getGoodsDetailAttr()
    },

    /**
     * 获取规格商品
     */
    getGoodsDetailAttr() {
        wx.showLoading({
            title: '加载中',
        })
        goodsControllor.getGoodsDetailAttr({
            GbId: this.data.GbId,
            Spv1: this.data.Spv1,
            Spv2: this.data.Spv2,
            Spv3: this.data.Spv3,
            mebno: this.data.mebno,
            ShopNo: this.data.shopNo
        }, data => {
            // 筛选规格，判断选中规格
            this.filterLvgg(data.goods[0].SPECIFICATION, data.lvgg);
            data.goods[0].C_MAINPICTURE = config.FilePath + data.goods[0].C_MAINPICTURE;
            this.setData({
                currentGoods: data.goods[0]
            })
            wx.hideLoading()
        })
    },

    setIsdf() {
        this.setData({
            ysdf: !this.data.ysdf,
        })
    },

    /**
     * 获取本地的桌台信息
     */
    getLocalTableInfo() {
        wx.getStorage({
            key: 'Tablesinfo',
            success: res => {
                this.data.Tablesinfo = res.data;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].ID == this.data.TableId) {
                        this.data.currentTable = res.data[i];
                        if (res.data[i].gList && res.data[i].gList.length > 0) {
                            this.data.gList = res.data[i].gList;
                            this.setFirstGoodsCheckedInfo(res.data[i].gList);
                        }
                        if (res.data[i].C_ORD_NO && this.data.cTotalNum == 0) {
                            this.getOrderInfo(res.data[i].C_ORD_NO);
                            if (res.data[i].sublist) {
                                this.data.cSublist = res.data[i].sublist;
                            }
                        } else {
                            this.setCartGoods();
                            this.setSubmitGoods("f");
                            this.getZtGoods();
                        }

                        // 如果当前是多桌开桌
                        if (this.data.billingTable) {
                            if (this.data.billingTable.length > 1) {
                                // 如果其中的桌子没有订单编号，
                                // 表示为为点菜，显示一式多样
                                if (!res.data[i].C_ORD_NO) {
                                    this.setData({
                                        showMoreType: true,
                                    })
                                }
                            }
                        }
                    }
                }
                this.setClassNum();
            },
            fail: rs => {}
        })
    },

    getZtGoods() {
        var str = `TID&ShopNo=${this.data.TableId}*${this.data.shopNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 2212,
            TID: this.data.TableId,
            ShopNo: this.data.shopNo,
            MD5: md5.toUpperCase()
        };

        request(data, res => {
            var arr = [];
            for (var i = 0; i < res.GoodsList.length; i++) {
                arr.push({
                    N_GOODS_ID: res.GoodsList[i].ID,
                    N_GB_ID: res.GoodsList[i].N_GB_ID,
                    N_NUM: 1,
                    N_REALPRICE: res.GoodsList[i].SALE_PRICE,
                    C_GOODS_NAME: res.GoodsList[i].GOODS_NAME,
                    G_CODE: res.GoodsList[i].GOODS_CODE,
                    C_GoodsTodo: res.GoodsList[i].GoodsTodo, // 做法名称
                    C_GoodsFeed: res.GoodsList[i].GoodsFeed, // 口味名称
                    ZFAmount: res.GoodsList[i].ZFAmount, // 做法金额
                    N_GoodsFeedJe: res.GoodsList[i].GoodsFeedJe, // 口味金额
                    C_GoodsRemark: res.GoodsList[i].GoodsRemark || "", // 备注
                    N_ServiceFee: 0,
                    N_PACKID: 0,
                    N_ISPACK: 0,
                    N_LPTC: 0,
                    GZT: res.GoodsList[i].GZT, // 商品状态，判断是正常商品还是删菜
                })
            }
            this.data.ztGoods = arr;
        })
    },

    getOrderInfo(OrderNo) {
        var str = `ShopNo&OrderNo=${this.data.shopNo}*${OrderNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4004,
            ShopNo: this.data.shopNo,
            OrderNo,
            MD5: md5.toUpperCase()
        };

        request(data, (data) => {
            if (data.code === "100") {
                this.data.delIndex = [];
                var arr = [];
                var cartList = [];
                // for (var i = 0; i < data.gbase.length; i++) {
                //   if (data.gbase[i].G_CODE == "thiszt") {
                //     this.data.orderTableInfo = data.gbase[i];
                //     break;
                //   }
                // }
                for (var i = 0; i < data.gbase.length; i++) {
                    if (data.gbase[i].C_GOODS_NAME.length > 18) {
                        data.gbase[i].C_GOODS_NAME = data.gbase[i].C_GOODS_NAME.substr(0, 17) + "..."
                    }
                    if (data.gbase[i].C_GOODS_NAME != "餐位费" && data.gbase[i].G_CODE != "thiszt") {
                        arr.push(data.gbase[i]);
                        if (data.gbase[i].N_TableID == this.data.TableId) {
                            cartList.push(data.gbase[i])
                        }
                    } else {
                        this.data.tableOrderInfo.push(data.gbase[i])
                    }
                }
                for (var i = 0; i < cartList.length; i++) {
                    cartList[i].C_MAINPICTURE = config.FilePath + arr[i].C_MAINPICTURE
                    if (cartList[i].GZT != 2) {
                        this.data.cTotalPrice += cartList[i].N_REALPRICE * cartList[i].N_NUM + cartList[i].N_GoodsFeedJe + cartList[i].N_ServiceFee + cartList[i].ZFAmount;
                        this.data.cTotalNum += cartList[i].N_NUM;
                    }
                }
                this.data.orderGoods = arr;
                this.data.totalPrice = this.data.totalPrice + this.data.cTotalPrice;
                this.setData({
                    tableOrderInfo: this.data.tableOrderInfo,
                    totalNum: this.data.totalNum + this.data.cTotalNum,
                    totalPrice: parseFloat(this.data.totalPrice).toFixed(2),
                    gbase: cartList
                })
                this.setSubmitGoods("f");
            }
            this.setCartGoods();
        })
    },

    /**
     * 在这里计算总价
     */
    setFirstGoodsCheckedInfo(data) {
        if (data) {
            for (var j = 0; j < data.length; j++) {
                if (data[j].cid == this.data.CateId) {
                    for (var i = 0; i < data[j].gList.length; i++) {
                        for (var u = 0; u < this.data.goodsList.length; u++) {
                            if (this.data.goodsList[u].ID == data[j].gList[i].ID) {
                                this.data.goodsList[u] = data[j].gList[i]
                            }
                        }
                    }
                }
            }
            this.setData({
                goodsList: this.data.goodsList
            })
        }

    },

    /**
     * 设置分类数量中挑选的商品初始值
     * 
     */
    setClassNum() {
        if (this.data.gList) {
            for (var i = 0; i < this.data.gList.length; i++) {
                var num = 0;
                for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                    num += this.data.gList[i].gList[j].num
                }
                this.data.classify[i].num = num;
            }

            this.setData({
                classify: this.data.classify
            })
        }
    },

    cartMinus(ev) {
        var item = ev.currentTarget.dataset.d;
        this.data.currentGoods = item;
        if (item.num > 1) {
            item.num--;
            this.data.currentGoodsNum = item.num;
            this.setEditGoodsNum(item.ID, item.num);
            this.data.cart[ev.currentTarget.dataset.i] = item;
            this.setData({
                cart: this.data.cart
            })
        } else {
            this.clearChooseGoodsInfo(item.ID, item.N_GB_ID);
        }
    },

    cartPlus(ev) {
        var item = ev.currentTarget.dataset.d;
        item.num++;
        this.data.currentGoodsNum = item.num;
        this.data.currentGoods = item;
        this.setEditGoodsNum(item.ID, item.num)
        this.data.cart[ev.currentTarget.dataset.i] = item;
        this.setData({
            cart: this.data.cart
        })
    },

    delCartGoods(ev) {
        var item = ev.currentTarget.dataset.d;
        var idx = ev.currentTarget.dataset.i;
        this.data.currentGoodsNum = item.num;
        this.data.currentGoods = item;
        this.data.cart.splice(idx, 1);
        this.clearChooseGoodsInfo(item.ID, item.N_GB_ID);
        this.setData({
            cart: this.data.cart
        })
    },

    clearChooseGoodsInfo(id, nid) {
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.goodsList[i].ID == id) {
                this.data.goodsList[i].num = 0;
                if (this.data.goodsList[i].currentPractice) {
                    this.data.goodsList[i].currentPractice.checked = false;
                }

                if (this.data.goodsList[i].Batchings) {
                    for (var j = 0; j < this.data.goodsList[i].Batchings.length; j++) {
                        this.data.goodsList[i].Batchings[j].checked = false;
                        this.data.goodsList[i].Batchings[j].num = 0;
                    }
                }
            }
        }
        for (var i = 0; i < this.data.subGoods.length; i++) {
            if (this.data.subGoods[i].ID == id) {
                this.data.subGoods.splice(i, 1)
            }
        }
        for (var i = 0; i < this.data.gList.length; i++) {
            for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                if (this.data.gList[i].gList[j].ID == id) {
                    this.data.gList[i].gList.splice(j, 1)
                }
            }
        }

        this.setData({
            goodsList: this.data.goodsList,
            subGoods: this.data.subGoods,
        })
        this.setClassNum();
        this.clearSublist(nid);
        this.setSubmitGoods("del");
    },

    clearSublist(id) {
        for (var i = 0; i < this.data.sublist.length; i++) {

            if (this.data.sublist[i].GoodsID == id) {
                this.data.sublist.splice(i, 1);
                this.clearSublist(id);
            }
        }
    },

    setEditGoodsNum(id, num) {
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.goodsList[i].ID == id) {
                this.data.goodsList[i].num = num;
            }
        }

        for (var i = 0; i < this.data.subGoods.length; i++) {
            if (this.data.subGoods[i].ID == id) {
                this.data.subGoods[i].num = num;
            }
        }

        for (var i = 0; i < this.data.gList.length; i++) {
            for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                if (this.data.gList[i].gList[j].ID == id) {
                    this.data.gList[i].gList[j].num = num;
                }
            }
        }

        this.setData({
            goodsList: this.data.goodsList,
            subGoods: this.data.subGoods,
        })
        this.setClassNum();
        this.sureGoods();
    },

    /**
     * 设置需要提交的商品信息
     */
    setSubmitGoods(t) {
        var arr = [];
        arr = arr.concat(this.data.orderGoods)
        var sublist = this.data.cSublist;
        for (var i = 0; i < this.data.gList.length; i++) {
            for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                var Batching_Name = [];
                var Remarks = [];
                var Practice_Name = "";
                var Practice_Price = 0;
                var N_GoodsFeedJe = 0;

                if (this.data.gList[i].gList[j].Batchings) {
                    for (var k = 0; k < this.data.gList[i].gList[j].Batchings.length; k++) {
                        if (this.data.gList[i].gList[j].Batchings[k].checked) {
                            Batching_Name.push(this.data.gList[i].gList[j].Batchings[k].Batching_Name);
                            N_GoodsFeedJe += this.data.gList[i].gList[j].Batchings[k].Batching_Price * this.data.gList[i].gList[j].Batchings[k].num;
                            if (t != "del") {
                                this.pushOptInfo({
                                    ID: this.data.gList[i].gList[j].Batchings[k].ID,
                                    Type: 1,
                                    GoodsID: this.data.gList[i].gList[j].N_GB_ID,
                                    Count: this.data.gList[i].gList[j].Batchings[k].num,
                                    Price: this.data.gList[i].gList[j].Batchings[k].Batching_Price,
                                    // MaxXh,
                                }, i);
                            }
                        }
                    }
                }
                if (this.data.gList[i].gList[j].Remarks) {
                    for (var l = 0; l < this.data.gList[i].gList[j].Remarks.length; l++) {
                        if (this.data.gList[i].gList[j].Remarks[l].checked) {
                            Remarks.push(this.data.gList[i].gList[j].Remarks[l].Remark_Name);
                            if (t != "del") {
                                this.pushOptInfo({
                                    ID: this.data.gList[i].gList[j].Remarks[l].ID,
                                    Type: 2,
                                    GoodsID: this.data.gList[i].gList[j].N_GB_ID,
                                    Count: 0,
                                    Price: 0,
                                    // MaxXh,
                                }, i);
                            }
                        }
                    }
                }
                if (this.data.gList[i].gList[j].currentPractice) {
                    Practice_Name = "做法：" + this.data.gList[i].gList[j].currentPractice.Practice_Name;
                    Practice_Price = this.data.gList[i].gList[j].currentPractice.Practice_Price;
                    if (t != "del") {
                        // 从这里拿到商品本身的属性，再做添加
                        this.pushOptInfo({
                            ID: this.data.gList[i].gList[j].currentPractice.ID,
                            Type: 0,
                            GoodsID: this.data.gList[i].gList[j].N_GB_ID,
                            Count: 0,
                            Price: this.data.gList[i].gList[j].currentPractice.Practice_Price,
                            // MaxXh,
                        }, i)
                    }
                }

                if (Batching_Name.length > 0) {
                    Batching_Name = Batching_Name.join(",")
                    Batching_Name = "口味：" + Batching_Name
                } else {
                    Batching_Name = "";
                }

                var data = {
                    N_GOODS_ID: this.data.gList[i].gList[j].ID,
                    N_GB_ID: this.data.gList[i].gList[j].N_GB_ID,
                    N_NUM: this.data.gList[i].gList[j].num,
                    N_REALPRICE: this.data.gList[i].gList[j].SALE_PRICE,
                    C_GOODS_NAME: this.data.gList[i].gList[j].GOODS_NAME,
                    G_CODE: this.data.gList[i].gList[j].GOODS_CODE,
                    C_GoodsTodo: Practice_Name, // 做法名称
                    C_GoodsFeed: Batching_Name, // 口味名称
                    GoodsGg: this.data.gList[i].gList[j].SPECIFICATION,
                    ZFAmount: Practice_Price, // 做法金额
                    N_GoodsFeedJe: N_GoodsFeedJe, // 口味金额
                    C_GoodsRemark: this.data.gList[i].gList[j].C_GoodsRemark, // 备注
                    // C_GoodsRemark: Remarks.join(",") + this.data.C_GoodsRemark, // 备注
                    N_ServiceFee: 0,
                    N_PACKID: 0,
                    N_ISPACK: 0,
                    N_LPTC: 0,
                    GZT: 0, // 商品状态，判断是正常商品还是删菜
                }
                arr.push(data);
            }
            this.data.subGoods = arr;
            this.data.gList[i].subGoods = arr;
        };

        if (t != "f") {
            this.setLocalData();
        }
    },

    /**
     * 设置每次挑选商品的时候添加的商品信息到本地
     */
    setLocalData() {
        for (var i = 0; i < this.data.Tablesinfo.length; i++) {
            if (this.data.Tablesinfo[i].ID == this.data.TableId) {
                this.data.Tablesinfo[i].gList = this.data.gList;
                wx.setStorage({
                    key: 'Tablesinfo',
                    data: this.data.Tablesinfo,
                })
            }
        }
        this.setCartGoods();
    },

    showInventory() {
        this.setData({
            dialogType: 2,
        })
    },

    pushOptInfo(data, idx) {
        // 如果当前已存在做法等信息
        if (this.data.sublist.length > 0) {
            var flag = false;
            // 检测是否有重复的做法信息
            for (var i = 0; i < this.data.sublist.length; i++) {
                if (this.data.sublist[i].ID == data.ID) {
                    // 如果有就覆盖
                    this.data.sublist[i] = data;
                    flag = true;
                    break;
                }
            }

            // 没有就添加
            if (!flag) {
                this.data.sublist.push(data);
            }
        } else {
            this.data.sublist.push(data);
        }
        this.data.gList[idx].sublist = this.data.sublist;
    },

    setCartGoods() {
        var arr = [];
        var totalNum = 0;
        var totalPrice = 0;
        if (!this.data.gList) {
            return;
        }
        for (var i = 0; i < this.data.gList.length; i++) {
            for (var j = 0; j < this.data.gList[i].gList.length; j++) {
                arr.push(this.data.gList[i].gList[j]);
            }
        };

        for (var i = 0; i < arr.length; i++) {
            totalNum += arr[i].num;
            totalPrice += arr[i].num * arr[i].SALE_PRICE;
        }

        totalNum += this.data.cTotalNum;
        totalPrice += this.data.cTotalPrice;

        for (var i = 0; i < arr.length; i++) {
            var batchStr = "";
            if (arr[i].Batchings && arr[i].Batchings.length > 0) {
                batchStr = "口味："
                for (var j = 0; j < arr[i].Batchings.length; j++) {
                    if (arr[i].Batchings[j].checked) {
                        batchStr += arr[i].Batchings[j].Batching_Name;
                        totalPrice += arr[i].Batchings[j].Batching_Price * arr[i].Batchings[j].num;
                    }
                }
            }
            arr[i].batchStr = batchStr;
        }

        for (var i = 0; i < arr.length; i++) {
            if (arr[i].currentPractice) {
                arr[i].practiceStr = "做法：" + arr[i].currentPractice.Practice_Name
                totalPrice += arr[i].currentPractice.Practice_Price;
            }
        }

        totalPrice = parseFloat(totalPrice).toFixed(2)

        this.setData({
            cart: arr,
            totalPrice,
            totalNum,
        })
    },

    /**
     * 切换做法选择
     */
    choosePracitice(ev) {
        var i1 = ev.currentTarget.dataset.i;
        var i2 = ev.currentTarget.dataset.j;
        if (this.data.PracticeOrType[i1].goods_practice[i2].checked) {
            this.data.totalPrice -= this.data.PracticeOrType[i1].goods_practice[i2].Practice_Price;
            this.data.PracticeOrType[i1].goods_practice[i2].checked = false;
        } else {
            for (var i = 0; i < this.data.PracticeOrType.length; i++) {
                for (var j = 0; j < this.data.PracticeOrType[i].goods_practice.length; j++) {
                    this.data.PracticeOrType[i].goods_practice[j].checked = false;
                }
            }
            this.data.PracticeOrType[i1].goods_practice[i2].checked = true;
            this.data.currentPractice = this.data.PracticeOrType[i1].goods_practice[i2];

            for (var i = 0; i < this.data.goodsList.length; i++) {
                if (this.data.goodsList[i].ID == this.data.currentGoods.ID) {
                    this.data.goodsList[i].currentPractice = this.data.PracticeOrType[i1].goods_practice[i2];
                    this.data.goodsList[i].PracticeOrType = this.data.PracticeOrType;
                }
            }
            this.data.totalPrice += this.data.PracticeOrType[i1].goods_practice[i2].Practice_Price
        }
        this.setData({
            totalPrice: parseFloat(this.data.totalPrice).toFixed(2),
            goodsList: this.data.goodsList,
            PracticeOrType: this.data.PracticeOrType,
        })
    },

    showSpec(ev) {

        if (ev.currentTarget.dataset.d.N_IsSellOut == 1) {
            wx.showToast({
                title: '已售罄',
                icon: "none"
            })
            return;
        }

        if (!this.data.allowSale) {
            if (ev.currentTarget.dataset.d.STOCK <= 0) {
                wx.showToast({
                    title: '库存不足',
                    icon: "none"
                })
                return;
            }
        }

        this.data.currentGoodsIndex = ev.currentTarget.dataset.t;
        this.setData({
            currentGoodsNum: ev.currentTarget.dataset.d.num || 0,
            C_GoodsRemark: "",
        })
        wx.showLoading({
            title: '加载中',
        })
        goodsControllor.getGoodsDetail701({
            gid: ev.currentTarget.dataset.i,
            ShopNo: this.data.shopNo,
            mebno: this.data.mebno,
        }, data => {
            console.log('data-----------')
            console.log(data)
            this.data.GbId = data.goods[0].N_GB_ID
            data.goods[0].C_MAINPICTURE = config.FilePath + data.goods[0].C_MAINPICTURE;
            this.filterLvgg(data.goods[0].SPECIFICATION, data.lvgg);
            data.goods[0].currentPracitice = null;
            data.goods[0].Batchings = null;
            this.data.goodsList[ev.currentTarget.dataset.t].GOODS_CODE = data.goods[0].GOODS_CODE
            this.setData({
                GoodsImgs: data.GoodsImgs,
                currentGoods: data.goods[0],
                currentAttr: data.lvgg,
                detailCover: true,
                lptcdt: data.lptcdt,
                dialogType: 1,
                // spec: 1,
            })
            if (ev.currentTarget.dataset.d.hasData) {
                this.setData({
                    Batchings: ev.currentTarget.dataset.d.Batchings,
                    PracticeOrType: ev.currentTarget.dataset.d.PracticeOrType,
                    Remarks: ev.currentTarget.dataset.d.Remarks
                })
                wx.hideLoading();
            } else {
                this.getGoodsOtherInfo(data.goods[0].N_GB_ID, ev.currentTarget.dataset.t)
            }
        })
    },

    setRemark(ev) {
        this.data.C_GoodsRemark = ev.detail.value;
        this.data.C_GoodsRemark2 = ev.detail.value;
        this.setData({
            C_GoodsRemark: this.data.C_GoodsRemark
        })
    },

    /**
     * 设置通过标签选择的代码
     */
    setRemark2(ev) {
        this.data.Remarks[ev.currentTarget.dataset.i].checked = !this.data.Remarks[ev.currentTarget.dataset.i].checked
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.goodsList[i].ID == this.data.currentGoods.ID) {
                this.data.goodsList[i].Remarks = this.data.Remarks;
            }
        }
        this.data.C_GoodsRemark = ev.currentTarget.dataset.n;
        this.setData({
            goodsList: this.data.goodsList,
            Remarks: this.data.Remarks
        })
    },

    /**
     * 选择加料
     */
    chooseThisBatching(ev) {
        this.data.Batchings[ev.currentTarget.dataset.i].checked = !this.data.Batchings[ev.currentTarget.dataset.i].checked;
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.currentGoods.ID == this.data.goodsList[i].ID) {
                this.data.goodsList[i].Batchings = this.data.Batchings;
            } else {
                this.data.goodsList[i].Batchings = null;
            }
        };
        this.setData({
            Batchings: this.data.Batchings,
            goodsList: this.data.goodsList
        })
        this.setChooseBatchings();
    },

    /**
     * 设置当前选中的加料
     */
    setChooseBatchings() {
        var batchPirce = 0;
        for (var i = 0; i < this.data.Batchings.length; i++) {
            if (this.data.Batchings[i].checked) {
                batchPirce += this.data.Batchings[i].Batching_Price * this.data.Batchings[i].num
            }
        }
        batchPirce = parseFloat(batchPirce).toFixed(2)
        this.data.totalPrice += batchPirce;
        this.data.batchPirce = batchPirce;
    },

    /**
     * 加料数量减
     */
    batchMinus(ev) {
        this.data.Batchings[ev.currentTarget.dataset.i].num--;
        this.setData({
            Batchings: this.data.Batchings
        })
        this.setChooseBatchings();
    },

    /**
     * 加料数量增加
     */
    batchPlus(ev) {
        this.data.Batchings[ev.currentTarget.dataset.i].num++;
        this.setData({
            Batchings: this.data.Batchings
        })
        this.setChooseBatchings();
    },

    /**
     * 当前菜品的增加
     */
    plus() {
        this.data.currentGoodsNum++;
        this.setData({
            currentGoodsNum: this.data.currentGoodsNum
        })
    },

    /**
     * 商品数量相减
     */
    minus() {
        if (this.data.currentGoodsNum > 0) {
            this.data.currentGoodsNum--;
            this.setData({
                currentGoodsNum: this.data.currentGoodsNum
            })
        } else {
            this.setData({
                currentGoodsNum: 0
            })
        }
    },

    /**
     * 获取商品的做法等信息
     */
    getGoodsOtherInfo(GoodsID, idx) {
        goodsControllor.getGoodsOtherInfo({
            GoodsID,
            ShopNo: this.data.shopNo,
        }, data => {
            this.data.goodsList[idx].hasData = false;
            if (data.Batchings) {
                // 设置加料
                for (var i = 0; i < data.Batchings.length; i++) {
                    data.Batchings[i].checked = false;
                    data.Batchings[i].num = 1;
                }
                this.setData({
                    Batchings: data.Batchings
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].Batchings = data.Batchings;
            } else {
                this.setData({
                    Batchings: []
                })
            }
            // 设置做法
            if (data.PracticeOrType) {
                for (var i = 0; i < data.PracticeOrType.length; i++) {
                    for (var j = 0; j < data.PracticeOrType[i].goods_practice.length; j++) {
                        data.PracticeOrType[i].goods_practice[j].checked = false;
                    }
                }
                this.setData({
                    PracticeOrType: data.PracticeOrType
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].PracticeOrType = data.PracticeOrType;
            } else {
                this.setData({
                    PracticeOrType: []
                })
            }
            // 设置备注
            if (data.Remarks) {
                for (var i = 0; i < data.Remarks.length; i++) {
                    data.Remarks[i].checked = false;
                }
                this.setData({
                    Remarks: data.Remarks
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].Remarks = data.Remarks;
                this.setData({
                    spec: 4,
                })
            } else {
                this.setData({
                    Remarks: [],
                    spec: 1,
                })
            }
            wx.hideLoading()
            this.setData({
                goodsList: this.data.goodsList
            })
        })
    },

    getGoodsClassify() {
        g.getGoodsClassify({
            bNo: 3100,
            ShopNo: this.data.shopNo,
        }, (data) => {
            // 创建商品分类列表
            var arr = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].CATEGORY_NAME.length > 4) {
                    data[i].CATEGORY_NAME = data[i].CATEGORY_NAME.substr(0, 4) + "..."
                }
                data[i].num = 0;
                arr.push({
                    cid: data[i].ID,
                    gList: [],
                    num: 0,
                    tprice: 0,
                })
            }
            // this.data.gList = arr;
            this.data.firstGlist = arr;
            this.setData({
                CateId: data[0].ID,
                classify: data,
                gList: arr,
            })

            wx.hideLoading()
            this.getGoodsList();
        })
    },

    /**
     * 切换分类
     */
    changeCate(ev) {
        this.setData({
            goodsList: [],
            pageindex: 1,
            CateId: ev.currentTarget.dataset.i
        })
        this.data.totalPage = 0;
        this.getGoodsList("s");
    },

    loadMoreGoods() {
        if (this.data.pageindex < this.data.totalPage) {
            this.data.pageindex++;
            this.getGoodsList("s");
        } else {
            this.setData({
                noData: true,
            })
        }
    },

    getGoodsList(t) {
        wx.showLoading({
            title: '加载中',
            mask: true,
        })
        if (this.data.getStatus) {
            this.data.getStatus = false;
        } else {
            return;
        }
        goodsControllor.getGoodsByCateidWaiter({
            pageindex: this.data.pageindex,
            pagesize: this.data.pagesize,
            cid: this.data.CateId,
            mebno: this.data.mebno,
            ShopNo: this.data.shopNo,
            name: "",
        }, (data) => {
            console.log(data)
            if (this.data.totalPage == 0) {
                this.data.totalPage = data.totalPage;
            }
            for (var i = 0; i < data.dtResult.length; i++) {
                if (!data.dtResult[i].N_IsSellOut) {
                    data.dtResult[i].N_IsSellOut = 0;
                }
                data.dtResult[i].listImg = data.dtResult[i].C_MAINPICTURE
                data.dtResult[i].C_MAINPICTURE = config.FilePath + data.dtResult[i].C_MAINPICTURE
                if (data.dtResult[i].GOODS_NAME.length > 14) {
                    data.dtResult[i].GOODS_NAME = data.dtResult[i].GOODS_NAME.substr(0, 14) + "..."
                }
                data.dtResult[i].hasData = false;
                this.data.goodsList.push(data.dtResult[i]);
            }
            for (var i = 0; i < this.data.subGoods.length; i++) {
                for (var j = 0; j < this.data.goodsList.length; j++) {
                    if (this.data.subGoods[i].N_GOODS_ID == this.data.goodsList[j].ID) {
                        this.data.goodsList[j].num = this.data.subGoods[i].N_NUM
                    }
                }
            };
            this.setData({
                goodsList: this.data.goodsList,
            })
            this.data.getStatus = true;
            // 等商品数据请求结束之后，
            // 再查找本地的桌台信息
            if (!t && !this.data.noTable) {
                this.getLocalTableInfo();
            }
            wx.hideLoading();
        })
    },
    // 放大商品图片
    showGoodsPic() {
        if (this.data.GoodsImgs.length > 0) {
            this.data.previewImage = true;
            var arr = [];
            for (var i = 0; i < this.data.GoodsImgs.length; i++) {
                arr.push(config.FilePath + this.data.GoodsImgs[i].PICNAME)
            };
            wx.previewImage({
                urls: arr,
            })
        }
    },
    // 放大列表图片
    showlistPic(e) {
        var listImg = e.currentTarget.dataset.src; //获取data-src
        var listSrc = e.currentTarget.dataset.listsrc
        var imgList = []; //获取data-list
        imgList.push(listSrc)
        if (listImg) {
            this.data.previewImage = true;
            wx.previewImage({
                urls: imgList // 需要预览的图片http链接列表
            })
        } else {
            wx.showToast({
                title: '没有图片！',
                icon: 'none'
            })
        }
    },

    /**
     * 
     */
    changeSpecTab(ev) {
        this.setData({
            spec: ev.currentTarget.dataset.i
        })
    },

    /**
     * 筛选规格
     */
    filterLvgg(t, lvgg) {
        // 切出当前商品的规格
        var attr = t.split(" ");
        // 去掉多余的数组
        attr.pop();
        var gAttr = [];
        // 筛选规格，只留下规格值
        for (var i = 0; i < attr.length; i++) {
            gAttr.push(attr[i].split(":")[1]);
        }
        // 判断哪个规格是选中状态
        for (var i = 0; i < gAttr.length; i++) {
            for (var j = 0; j < lvgg[i].SpvList.length; j++) {
                if (lvgg[i].SpvList[j].C_SPE_VALUE == gAttr[i]) {
                    lvgg[i].SpvList[j].checked = true;
                    switch (i) {
                        case 0:
                            this.data.Spv1 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                        case 1:
                            this.data.Spv2 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                        case 2:
                            this.data.Spv3 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                    }
                }
            }
        }

        // 设置规格值
        this.setData({
            currentAttr: lvgg,
        })
    },

    def() {},

    hideDialog() {
        this.setData({
            dialogType: 0,
        })
    },
})