// pages/waiter/orderDetail/orderDetail.js
var util = require("../../../utils/MD5.js")
var request = require("../../../utils/request.js")
var config = require("../../../config.js");
var tableControllor = require("../../../utils/tableControllor.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ID: "",
    OrderNo: "",
    ShopNo: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.TableId) {
      this.setData({
        ID: options.TableId,
        NAME: options.tname
      })
    }
    if (options.OrderNo) {
      this.data.OrderNo = options.OrderNo
    }

    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getOrderDetail();
        this.queryTableInfo();
      },
    })
  },

  ret() {
    wx.navigateBack({
      delta: 1,
    })
  },

  queryTableInfo() {
    tableControllor.queryTable({
      TID: this.data.ID,
      ShopNo: this.data.ShopNo,
    }, data => {
      data.Tablesinfo.STARTTIME = data.Tablesinfo.STARTTIME.replace("T", " ")
      this.setData({
        table: data,
      })
    })
  },

  getOrderDetail(fr) {
    var str = `ShopNo&OrderNo=${this.data.ShopNo}*${this.data.OrderNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 4004,
      ShopNo: this.data.ShopNo,
      OrderNo: this.data.OrderNo,
      MD5: md5.toUpperCase()
    };

    request(data, (data) => {
      if (fr) {
        wx.stopPullDownRefresh()
      }
      if (data.code === "100") {
        for (var i = 0; i < data.gbase.length; i++) {
          this.data.cTotalPrice += data.gbase[i].N_REALPRICE * data.gbase[i].N_NUM;
          this.data.cTotalNum += data.gbase[i].N_NUM;
          data.gbase[i].C_MAINPICTURE = config.FilePath + data.gbase[i].C_MAINPICTURE
        }
          console.log(data.gbase)
        this.setData({
          PayAmount: data.PayAmount,
          goodsList: data.gbase,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getOrderDetail(1);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})