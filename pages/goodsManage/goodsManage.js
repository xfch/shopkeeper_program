// pages/goodsManage/goodsManage.js
var g = require('../../utils/goodsCommon.js')
var config = require("../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classify: [],
    goodsList: [],
    opInfo: {},
    pageindex: 1,
    pagesize: 10,
    kword: "",
    CateId: "",
    activeIndex: 0,
    totalPage: 0,
    err: {
      status: false,
      msg: "",
    },
    shopNo: "",
    getStatus: true,
    hasPgEnd: false,
  },

  moreOpt(ev) {
    var data = ev.currentTarget.dataset.d;
    var arr = ["", "", "删除"];
    if (data.N_ISWEBSELL == 0) {
      arr[0] = "上架电商";
    } else {
      arr[0] = "下架电商"
    };

    if (data.N_SALE_STATE == 0) {
      arr[1] = "上架"
    } else {
      arr[1] = "下架"
    }

    wx.showActionSheet({
      itemList: arr,
      success: res => {
        switch (res.tapIndex) {
          case 0:
            if (data.N_ISWEBSELL == 0) {
              // 上架电商
              this.goodsSetting("sjds", data.ID);
            } else {
              this.goodsSetting("xjds", data.ID);
            };
            break;
          case 1:
            if (data.N_SALE_STATE == 0) {
              this.goodsSetting("sjxs", data.ID);
            } else {
              this.goodsSetting("xjts", data.ID);
            }
            break;
          case 2:
            this.delGoods(data.ID, data.GOODS_NAME);
        }
      }
    })
  },

  setSearchContent(ev) {
    this.data.kword = ev.detail.value;
  },

  searchGoods() {
    this.data.pageindex = 1;
    this.setData({
      goodsList: [],
    })
    this.getGoodsList();
  },

  goodsSetting(Moth, id) {
    wx.showLoading({
      title: '',
      mask: true,
    })
    g.goodsSetting({
      bNo: 3204,
      gid: id,
      ShopNo: this.data.shopNo,
      opid: this.data.opInfo.n_ss_id,
      opname: this.data.opInfo.c_name,
      Moth,
    }, (data) => {
      wx.hideLoading()
      if (data.Result == 1) {
        this.setData({
          goodsList: [],
        })
        this.getGoodsList();
      }
    })
  },

  onGoods() {
    wx.showActionSheet({
      itemList: ["上架实体", "上架网店"],
    })
  },

  changeClassify(ev) {
    this.setData({
      activeIndex: ev.currentTarget.dataset.i,
      goodsList: [],
      hasPgEnd: false
    })
    this.data.pageindex = 1;
    this.data.CateId = ev.currentTarget.dataset.d.ID;
    this.getGoodsList();
  },

  offGoods() {
    wx.showActionSheet({
      itemList: ["下架实体", "下架网店"],
    })
  },

  delGoods(id, name) {
    wx.showModal({
      title: '提示',
      content: '确认删除商品"' + name + '"吗？',
      success: ms => {
        if (ms.confirm) {
          this.goodsSetting("delAllGoods", id)
        }
      }
    })
  },

  editGoods(ev) {
    wx.navigateTo({
      url: '/pages/editGoods/editGoods?gid=' + ev.currentTarget.dataset.id,
    })
  },

  addGoods() {
    wx.navigateTo({
      url: '/pages/editGoods/editGoods?gid='
    })
  },

  manageClassify() {
    wx.navigateTo({
      url: '/pages/goodsClassify/goodsClassify',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
      },
    })
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.setData({
          opInfo: res.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.pageindex = 1;
    this.setData({
      goodsList: [],
      activeIndex: 0
    })
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getGoodsClassify();
      },
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      goodsList: [],
      hasPgEnd: false
    })
    this.data.pageindex = 1;
    this.getGoodsList();
  },

  /**
   * 获取商品分类
   */
  getGoodsClassify() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    g.getGoodsClassify({
      bNo: 3100,
      ShopNo: this.data.shopNo,
    }, (data) => {
        console.log(data)
        if (data.length > 0){
            for (var i = 0; i < data.length; i++) {
                if (data[i].CATEGORY_NAME.length > 5) {
                    data[i].CATEGORY_NAME = data[i].CATEGORY_NAME.substr(0, 5) + "..."
                }
            }
            this.setData({
                CateId: data[0].ID || '',
                classify: data,
            })
        }
      wx.hideLoading()
      this.getGoodsList();
    })
  },

  loadMoreGoods() {
      if (this.data.hasPgEnd){
          return
      }
      this.data.pageindex++;
      this.getGoodsList();
    //   console.log(this.data.pageindex + 1)
    // if (this.data.hasPgEnd) {
    //   this.data.pageindex++;
    //   this.getGoodsList();
    // } else {
    //   this.setData({
    //     err: {
    //       status: true,
    //       msg: "没有更多了~"
    //     }
    //   })
    // }
  },

  getGoodsList() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    if (this.data.getStatus) {
      this.data.getStatus = false;
    } else {
      return;
    }
    
    g.getGoodsList({
      bNo: 3200,
      ShopNo: this.data.shopNo,
      pageindex: this.data.pageindex,
      pagesize: this.data.pagesize,
      kword: this.data.kword,
      CateId: this.data.CateId,
    }, (data) => {
      wx.hideLoading()
      if (this.data.totalPage == 0) {
        this.data.totalPage = data.totalPage;
      }
        // console.log(this.data.pageindex)
        // console.log(this.data.totalPage)
        // console.log(this.data.getStatus)
      if (data.dtResult) {
        for (var i = 0; i < data.dtResult.length; i++) {
          if (data.dtResult[i].C_MAINPICTURE) {
            data.dtResult[i].C_MAINPICTURE = config.FilePath + data.dtResult[i].C_MAINPICTURE;
          }
          this.data.goodsList.push(data.dtResult[i])
        }
        if (data.dtResult.length < this.data.pagesize){
            this.data.hasPgEnd = true
        }
        this.setData({
            goodsList: this.data.goodsList,
            hasPgEnd: this.data.hasPgEnd
        })
          console.log(this.data.hasPgEnd)
        // if (data.dtResult.length == 0 && data.totalPage == 0) {
        //     this.setData({

        //     })
        //   this.setData({
        //     err: {
        //       status: true,
        //       msg: "没找到该商品~"
        //     }
        //   })
        // }
      } else {
        this.setData({
          err: {
            status: true,
            msg: "没有商品~"
          }
        })
      }
      this.data.getStatus = true;
    });
  }
})