// pages/uploadInfo/uploadInfo.js
var config = require("../../config.js");
var util = require("../../utils/MD5.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    editId: "",
    src: "",
    filePath: config.FilePath,
    p1: false,
    p2: false,
    p3: false,
    p4: false,
    p5: false,
    p6: false,
    p7: false,
    p8: false,
    p9: false,
    p10: false,
    p11: false,
    src1: "",
    src2: "",
    src3: "",
    src4: "",
    src5: "",
    src6: "",
    src7: "",
    src8: "",
    src9: "",
    src10: "",
    src11: "",
    c_yyzzurl: "", // 营业执照
    c_flurl1: "", // 法人身份证正面
    c_flurl2: "", // 法人身份证反面
    c_flurl3: "", // 结算银行卡正面
    c_flurl5: "", // 开户许可证
    c_dpurl1: "", // 门头照片
    c_dpurl2: "", // 收银台照片
    c_dpurl3: "", // 环境照片
    c_posurl1: "",
    c_posurl2: "",
    c_posurl3: "",
    n_open_pos: 0,
    subData: {},
    imgUrl: {},
    sType: 0,
    aType: 1,
    show: false,
    c_shop_no: "",
  },

  choosePhoto(ev) {
    var editId = ev.currentTarget.dataset.i;
    this.data.editId = editId;
    wx.chooseImage({
      sizeType: "compressed",
      count: 1,
      success: res => {
        this.data.src = res.tempFilePaths[0];
        this.uploadImages();
      },
    })
  },

  showModel(ev) {
    if (ev.detail.value.length > 0) {
      this.setData({
        show: true,
      })
      this.data.n_open_pos = 1;
    } else {
      this.setData({
        show: false,
      })
      this.data.n_open_pos = 0;
    }
  },

  prevStep() {
    wx.navigateBack({
      delta: 1
    })
  },

  showData() {
    if (!this.data.c_yyzzurl) {
      wx.showModal({
        title: '提示',
        content: '请上传营业执照',
        showCancel: false,
      })
      return;
    }

    if (!this.data.c_flurl1) {
      wx.showModal({
        title: '提示',
        content: '请上传法人身份证正面',
        showCancel: false
      })
      return;
    }

    if (!this.data.c_flurl2) {
      wx.showModal({
        title: '提示',
        content: '请上传法人身份证反面',
        showCancel: false
      })
      return;
    }

    if (this.data.sType == 1) {
      if (!this.data.c_flurl5) {
        wx.showModal({
          title: '提示',
          content: '请上传开户许可证',
          showCancel: false,
        })
        return;
      }
      
    }

    if (this.data.aType == 1) {
      if (!this.data.c_flurl3) {
        wx.showModal({
          title: '提示',
          content: '请上传结算银行卡正面',
          showCancel: false,
        })
        return;
      }
    }

    if (this.data.n_open_pos == 1) {
      if (!this.data.c_posurl1) {
        wx.showModal({
          title: '提示',
          content: '请上传结算银行卡正面',
          showCancel: false,
        })
        return;
      }
    }

    if (!this.data.c_dpurl1) {
      wx.showModal({
        title: '提示',
        content: '请上传门头/招牌照',
        showCancel: false,
      })
      return;
    }

    var data = this.data.subData;
    data.c_yyzzurl = this.data.c_yyzzurl;
    data.c_flurl1 = this.data.c_flurl1;
    data.c_flurl2 = this.data.c_flurl2;
    data.c_flurl3 = this.data.c_flurl3;
    data.c_flurl4 = "";
    data.c_flurl5 = this.data.c_flurl5;
    data.c_dpurl1 = this.data.c_dpurl1;
    data.c_dpurl2 = this.data.c_dpurl2;
    data.c_dpurl3 = this.data.c_dpurl3;
    data.n_open_pos = this.data.n_open_pos;
    data.c_shop_no = this.data.c_shop_no;
    // data.c_shop_no = "100302";

    if (data.n_open_pos == 1) {
      data.c_posurl1 = this.data.c_posurl1;
      data.c_posurl2 = this.data.c_posurl2;
      data.c_posurl3 = this.data.c_posurl3;
    } else {
      data.c_posurl1 = "";
      data.c_posurl2 = "";
      data.c_posurl3 = "";
    }

    wx.request({
      url: config.requestUrl + "?bNo=OpwnWkPay",
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      method: "POST",
      data: {
        data: JSON.stringify(data),
      },
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        if (data.Result) {
          wx.reLaunch({
            url: '/pages/regSuccess/regSuccess',
          })
        } else {
          wx.showModal({
            title: '提示',
            content: data.msg,
            showCancel: false,
          })
        }
      }
    })
  },

  uploadImages() {
    wx.showLoading({
      title: '正在上传',
    })
    wx.uploadFile({
      url: config.requestUrl + "?bNo=1907",
      filePath: this.data.src,
      name: 'file',
      formData: {
        filepath: encodeURI(this.data.src)
      },
      success: res => {
        if (res.data) {
          var data = res.data.replace(/\(|\)/g, "");
          var data2 = JSON.parse(data);
          switch (this.data.editId) {
            case "1":
              this.data.subData.c_yyzzurl = data2.filepath;
              this.setData({
                p1: true,
                src1: data2.filepath,
                c_yyzzurl: data2.filepath
              });
              break;
            case "2":
              this.data.subData.c_flurl1 = data2.filepath;
              this.setData({
                p2: true,
                src2: data2.filepath,
                c_flurl1: data2.filepath
              });
              break;
            case "3":
              this.data.subData.c_flurl2 = data2.filepath;
              this.setData({
                p3: true,
                src3: data2.filepath,
                c_flurl2: data2.filepath,
              });
              break;
            case "4":
              this.data.subData.c_flurl5 = data2.filepath;
              this.setData({
                p4: true,
                src4: data2.filepath,
                c_flurl5: data2.filepath,
              });
              break;
            case "5":
              this.data.subData.c_flurl3 = data2.filepath;
              this.setData({
                p5: true,
                src5: data2.filepath,
                c_flurl3: data2.filepath
              });
              break;
            case "6":
              this.data.subData.c_posurl1 = data2.filepath;
              this.setData({
                p6: true,
                src6: data2.filepath,
                c_posurl1: data2.filepath,
              });
              break;
            case "7":
              this.data.subData.c_posurl2 = data2.filepath;
              this.setData({
                p7: true,
                src7: data2.filepath,
                c_posurl2: data2.filepath,
              });
              break;
            case "8":
              this.data.subData.c_posurl3 = data2.filepath;
              this.setData({
                p8: true,
                src8: data2.filepath,
                c_posurl3: data2.filepath,
              });
              break;
            case "9":
              this.data.subData.c_dpurl1 = data2.filepath;
              this.setData({
                p9: true,
                src9: data2.filepath,
                c_dpurl1: data2.filepath,
              });
              break;
            case "10":
              this.data.subData.c_dpurl2 = data2.filepath;
              this.setData({
                p10: true,
                src10: data2.filepath,
                c_dpurl2: data2.filepath,
              });
              break;
            case "11":
              this.data.subData.c_dpurl3 = data2.filepath;
              this.setData({
                p11: true,
                src11: data2.filepath,
                c_dpurl3: data2.filepath,
              });
              break;
          }
          wx.setStorage({
            key: 'perfect_info',
            data: this.data.subData,
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '上传失败',
            showCancel: false
          })
        }
      },
      fail: msg => {
        wx.showModal({
          title: '提示',
          content: '上传失败',
          showCancel: false
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'perfect_info',
      success: res => {
        var t = "";
        var a = "";
        if (res.data.c_shop_lx == 1 && res.data.card_user_type == 1) {
          t = 1;
          a = 1;
        } else if (res.data.c_shop_lx == 1 && res.data.card_user_type == 2) {
          t = 1;
          a = 0;
        } else if (res.data.c_shop_lx == 2 && res.data.card_user_type == 1) {
          t = 0;
          a = 1;
        } else if (res.data.c_shop_lx == 2 && res.data.card_user_type == 2) {
          t = 1;
          a = 0;
        }
        // 设置图片
        var src1 = "";
        var src2 = "";
        var src3 = "";
        var src4 = "";
        var src5 = "";
        var src6 = "";
        var src7 = "";
        var src8 = "";
        var src9 = "";
        var src10 = "";
        var src11 = "";
        var p1 = false;
        var p2 = false;
        var p3 = false;
        var p4 = false;
        var p5 = false;
        var p6 = false;
        var p7 = false;
        var p8 = false;
        var p9 = false;
        var p10 = false;
        var p11 = false;
        if (res.data.c_yyzzurl) {
          src1 = res.data.c_yyzzurl;
          this.data.c_yyzzurl = res.data.c_yyzzurl;
          p1 = true;
        }

        if (res.data.c_flurl1) {
          src2 = res.data.c_flurl1;
          this.data.c_flurl1 = res.data.c_flurl1;
          p2 = true;
        }

        if (res.data.c_flurl2) {
          src3 = res.data.c_flurl2;
          this.data.c_flurl2 = res.data.c_flurl2;
          p3 = true;
        }

        if (res.data.c_flurl5) {
          src4 = res.data.c_flurl5;
          this.data.c_flurl5 = res.data.c_flurl5;
          p4 = true;
        }

        if (res.data.c_flurl3) {
          src5 = res.data.c_flurl3;
          this.data.c_flurl3 = res.data.c_flurl3;
          p5 = true;
        }

        if (res.data.c_posurl1) {
          src6 = res.data.c_posurl1;
          this.data.c_posurl1 = res.data.c_posurl1;
          p6 = true;
        }

        if (res.data.c_posurl2) {
          src7 = res.data.c_posurl2;
          this.data.c_posurl2 = res.data.c_posurl2;
          p7 = true;
        }

        if (res.data.c_posurl3) {
          src8 = res.data.c_posurl3;
          this.data.c_posurl3 = res.data.c_posurl3;
          p8 = true;
        }

        if (res.data.c_dpurl1) {
          src9 = res.data.c_dpurl1;
          this.data.c_dpurl1 = res.data.c_dpurl1;
          p9 = true;
        }
        if (res.data.c_dpurl2) {
          src10 = res.data.c_dpurl2;
          this.data.c_dpurl2 = res.data.c_dpurl2;
          p10 = true;
        }
        if (res.data.c_dpurl3) {
          src11 = res.data.c_dpurl3;
          this.data.c_dpurl3 = res.data.c_dpurl3;
          p11 = true;
        }
        this.setData({
          subData: res.data,
          sType: t,
          aType: a,
          src1,
          src2,
          src3,
          src4,
          src5,
          src6,
          src7,
          src8,
          src9,
          src10,
          src11,
          p1,
          p2,
          p3,
          p4,
          p5,
          p6,
          p7,
          p8,
          p9,
          p10,
          p11,
        })
      },
    })

    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.c_shop_no = res.data
      },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})