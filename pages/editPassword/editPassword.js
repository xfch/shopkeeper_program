// pages/editPassword/editPassword.js
var config = require("../../config.js");
var util = require("../../utils/MD5.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    te: "",
    opwd: "",
    pwd: "",
    pwd2: "",
    checkData: {
      p2: false,
      p3: false,
      p4: false,
    },
    pass: "",
    err: false,
    errMessage: "密码错误",
    shopInfo: ""
  },

  onLoad() {
    var _this = this;
    wx.getStorage({
      key: 'OP_INFO',
      success: function (res) {
        console.log(res.data)
        _this.setData({
          shopInfo: res.data
        })
      },
    })
  },

  /**
   * 设置登录账号
   */
  setOldPasswrd(ev) {
    if (ev.detail.value.length > 0) {
      this.data.checkData.p2 = true;
    } else {
      this.data.checkData.p2 = false;
    }
    this.setData({
      opwd: ev.detail.value
    })
    this.checkData()
  },

  /**
   * 设置登录密码
   */
  setPassword(ev) {
    if (ev.detail.value.length > 0) {
      this.data.checkData.p3 = true;
    } else {
      this.data.checkData.p3 = false;
    }
    this.setData({
      pwd: ev.detail.value
    })
    this.checkData()
  },

  setPassword2(ev) {
    if (ev.detail.value.length > 0) {
      this.data.checkData.p4 = true;
    } else {
      this.data.checkData.p4 = false;
    }
    this.setData({
      pwd2: ev.detail.value
    })
    this.checkData()
  },

  checkData() {
    var flag = true;
    for (var i in this.data.checkData) {
      if (!this.data.checkData[i]) {
        flag = false;
        break;
      }
    }

    if (flag) {
      this.setData({
        pass: "active",
      })
    } else {
      this.setData({
        pass: "",
      })
    }
  },

  hideDialog() {
    this.setData({ err: false })
  },

  editFunc() {
    console.log(this.data.opwd)
    if (this.data.opwd.length < 6 || this.data.opwd != this.data.shopInfo.c_logpwd) {
      this.setData({
        err: true,
        errMessage: "旧密码错误"
      })
      return;
    };
    if (this.data.pwd.length < 6) {
      this.setData({
        err: true,
        errMessage: "密码长度为6位以上"
      })
      return;
    }
    if (this.data.pwd != this.data.pwd2) {
      this.setData({
        err: true,
        errMessage: "两次密码不相同，请重新输入"
      })
      return;
    }

    wx.showLoading({
      title: '',
      mask: true,
    })
    var str = "ShopNo&StaffId&NewPwd=" + this.data.shopInfo.c_shop_no + "*" + this.data.shopInfo.n_ss_id + "*" + this.data.pwd,
      md5 = util.md5(str),
      _this = this,
      data = {
        bNo: 1702,
        ShopNo: this.data.shopInfo.c_shop_no,
        StaffId: this.data.shopInfo.n_ss_id,
        NewPwd: this.data.pwd,
        MD5: md5.toUpperCase()
      }

    wx.request({
      url: config.requestUrl,
      data,
      success(res) {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));

        if (data.code === "100") {

          wx.showToast({
            title: '修改成功',
            icon: "success",
            duration: 1000,
            success() {
              setTimeout(() => {
                wx.removeStorage({
                  key: 'OP_INFO',
                  success: function (res) { },
                })
                wx.redirectTo({
                  url: '/pages/login/login',
                })
              }, 1000)
            }
          })
        }
      }
    })
  }
})