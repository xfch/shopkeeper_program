// pages/messageDetail/messageDetail.js
var util = require("../../utils/MD5.js");
var config = require("../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: {},
    filePath: config.FilePath
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'message_content',
      success: res => {
        this.setData({
          message: res.data
        });

        if (res.data.N_MsgState == 1) {
          this.setReadStatus();
        }
      },
    });
  },

  setReadStatus() {
    var str = "msgid=" + this.data.message.N_Id;
    var md5 = util.md5(str);
    var data = {
      bNo: 2001,
      msgid: this.data.message.N_Id
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})