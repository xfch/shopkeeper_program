// pages/memberDetail/integralChange/integralChange.js
const app = getApp()
const common = require('../../../common/js/common.js')
Page({
    data: {
        integertypeArr: ['加', '减']
    },
    onLoad: function (options) {
        console.log(common.prevPage().data)
        const member = common.prevPage().data.member
        this.setData({
            member: member
        })
        wx.getStorage({
            key: 'OP_INFO',
            success: res => {
                this.data.opInfo = res.data;
                this.setData({
                    opInfo: this.data.opInfo
                })
            },
        })
    },
    onInput(e){
        const names = e.currentTarget.dataset.names; 
        const value = e.detail.value; 
        if (names == 'Integer'){
            this.setData({
                Integer: value
            })
        }else{
            this.setData({
                remark: value
            })
        }
    },
    integertypeChange(e){
        console.log(e)
        this.setData({ integertypeIndex: (e.detail.value).toString()})
    },
    // 确定
    submit(){
        if (!this.data.Integer) {
            wx.showModal({
                title: '提示',
                content: '请输入积分数量',
                showCancel: false,
            })
            return;
        }
        if (!this.data.remark) {
            wx.showModal({
                title: '提示',
                content: '请输入更改原因',
                showCancel: false,
            })
            return;
        }
        if (!this.data.integertypeIndex){
            wx.showModal({
                title: '提示',
                content: '请选择积分类型',
                showCancel: false,
            })
            return;
        }
        const str =`mebno&Integer&integertype&remark&opid&opname&ShopNo`
        let params = {
            bNo: '1709I',
            mebno: this.data.member.C_MB_NO,
            Integer: this.data.Integer,
            integertype: parseInt(this.data.integertypeIndex) + 1, // +1,0-
            remark: this.data.remark,
            opid: this.data.opInfo.n_ss_id,
            opname: this.data.opInfo.c_name
        }
        console.log(params)
        app.apiGet(str, params, (res) => {
            wx.showToast({
                title: res.msg,
            })
            setTimeout(() => {
                wx.navigateBack()
            },500)
        })
    }
})