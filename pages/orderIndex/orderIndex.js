var config = require("../../config.js")
var util = require("../../utils/MD5.js")
var goodsControllor = require("../../utils/goodsControllor.js");
var tableControllor = require("../../utils/tableControllor.js");
var g = getApp().globalData;
var request = require("../../utils/request.js");
var timer = null;
var time = 60;

let app = getApp();
const common = require('../../common/js/common.js')
Page({
    data: {
        ShopNo: "",
        classify: [],
        activeItem: 0,
        pageindex: 1, 
        mebno: 0,
        allowSale: true,
        textLength: 0,
        dialogType: 0,
        currentGoodsNum: 0,
        zpid: "",
        Remarks: [],
        previewImage: false,
        lptcdt: null,
        goodsList: [],
        resultList: [],
        totalNum: 0,
        totalPrice: 0,
        openOrder: true,
        showCart: false,
        ztInfo: {
            C_CODE: "34234",
            C_ZPNAME: "佳人有约",
            N_CID: 361,
            RegionName: "娱乐区",
            TableName: "佳人有约",
        },
        totalPage: 0,
        stateClass: "state1",
        callStatus: true,
        currentRemark: null,
        callText: "呼叫",
        spec: 4,
    },
    onLoad: function (options) {
        console.log(options.scene)
        let scene = wx.getStorageSync('scene')
        if (scene){
            if (scene != options.scene){
                wx.removeStorageSync('result-list')
            }
        }
        if (this.data.openOrder) {
            wx.removeStorageSync("scan-order-status")
            if (options.scene) {
                var info = options.scene.split("%2C");
                g.SHOP_NO = info[1];
                this.data.ShopNo = info[1];
                g.zpid = info[0]
                this.data.zpid = info[0]
                // 设置全局
                app.shopno = info[1];
                app.tableId = info[0]
                wx.setStorageSync('scene', options.scene)
                // 获取店铺信息
                common.getShopInfo(info[1], (res) => {
                    wx.setNavigationBarTitle({
                        title: res.model.c_name,
                    })
                    this.setData({
                        shopInfo: res.model
                    })
                    common.getIsPay(this)
                })
            }
            wx.showLoading({
                title: '加载中',
            })
        }
        common.getOpenid()
    },
    onShow: function () {
        console.log('onShow------------')
        if (!this.data.previewImage) {
            this.getTableInfo();
            this.getSureOrderConfig();
            var orderStatus = wx.getStorageSync("scan-order-status") + "";
            if (orderStatus == "") {orderStatus = 1;}
            this.getGoodsClassify();
            var localList = wx.getStorageSync("result-list");
            // ==============================================
            console.log('console.log(localList)==============')
            console.log(localList)
            if (localList) {
                this.data.resultList = localList.gList;
                this.getTotalInfo();
            } else {
                this.data.resultList = [];
                this.getTotalInfo();
            }
            this.setData({
                // goodsList: this.data.goodsList,
                goodsList: [],
                // totalNum: 0,
                // totalPrice: 0,
                pageindex: 1,
                orderStatus,
                activeItem: 0,
            })
        } else {
            this.data.previewImage = false;
        }
    },

    getSureOrderConfig() {
        var str = `Setlx&ShopNo=40*${this.data.ShopNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 429,
            Setlx: 40,
            ShopNo: this.data.ShopNo,
            MD5: md5.toUpperCase()
        }
        console.log(config.requestUrl)
        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                if (res.data.setValue) {
                    if (res.data.setValue != "1") {
                        this.setData({
                            allowSale: false,
                        })
                    } else {
                        this.setData({
                            allowSale: true,
                        })
                    }
                }
            }
        })
    },

    viewList() {
        wx.navigateTo({
            url: '/pages/my-food-order/my-food-order',
        })
    },

    showGoodsPic() {
        if (this.data.GoodsImgs.length > 0) {
            this.data.previewImage = true;
            var arr = [];
            for (var i = 0; i < this.data.GoodsImgs.length; i++) {
                arr.push(config.FilePath + this.data.GoodsImgs[i].PICNAME)
            };

            wx.previewImage({
                urls: arr,
            })
        }
    },
    showlistPic(e) {
        var listImg = e.currentTarget.dataset.src;//获取data-src
        var listSrc = e.currentTarget.dataset.listsrc
        var imgList = [];//获取data-list
        imgList.push(listSrc)
        if (listImg) {
            this.data.previewImage = true;
            wx.previewImage({
                urls: imgList // 需要预览的图片http链接列表
            })
        }
        else {
            wx.showToast({
                title: '没有图片！',
                icon: 'none'
            })
        }
    },

    showCallOptions() {
        if (!this.data.callStatus) {
            return;
        }
        if (this.data.orderStatus == 1 && this.data.gbase) {
            wx.showActionSheet({
                itemList: ["呼叫服务员", "催菜", "呼叫结账"],
                success: res => {
                    this.callWaiter(res.tapIndex + 1);
                }
            })
        } else {
            this.callWaiter(1);
        }
    },

    callWaiter(CallType) {
        var MD5 = util.md5(`OrderNo&TID&CallType&ShopNo=${this.data.ztInfo.C_ORD_NO}*${this.data.ztInfo.ID}*${CallType}*${this.data.ShopNo}`).toUpperCase();
        var data = {
            bNo: 2223,
            OrderNo: this.data.ztInfo.C_ORD_NO,
            TID: this.data.ztInfo.ID,
            CallType: CallType,
            ShopNo: this.data.ShopNo,
            MD5,
        }

        if (this.data.callStatus) {
            this.setData({
                callStatus: false,
            })
            wx.request({
                url: config.requestUrl,
                data,
                success: res => {
                    if (res.data.code === "100") {
                        wx.showToast({
                            title: '已呼叫服务',
                        })
                        timer = setInterval(() => {
                            if (time > 1) {
                                time--;
                                this.setData({
                                    callText: time + "s",
                                })
                            } else {
                                this.setData({
                                    callText: "呼叫",
                                    callStatus: true,
                                })
                                time = 60;
                                clearInterval(timer)
                            }
                        }, 1000)
                    }
                }
            })
        }
    },

    showCart() {
        if (this.data.resultList.length > 0) {
            this.setData({
                showCart: !this.data.showCart,
            })
        }
    },

    clear() {
        this.data.resultList = [];
        for (var i = 0; i < this.data.goodsList.length; i++) {
            this.data.goodsList[i].N_NUM = 0;
        }

        this.setData({
            goodsList: this.data.goodsList
        })
        wx.removeStorage({
            key: 'result-list',
        })
        this.hideCart();
        this.getTotalInfo();
    },

    jian(ev) {
        if (this.data.resultList[ev.currentTarget.dataset.i].N_NUM > 1) {
            this.data.resultList[ev.currentTarget.dataset.i].N_NUM--;
            this.updateGoodsList(this.data.resultList[ev.currentTarget.dataset.i].ID, this.data.resultList[ev.currentTarget.dataset.i].N_NUM);
        } else {
            this.updateGoodsList(this.data.resultList[ev.currentTarget.dataset.i].ID, 0);
            this.data.resultList.splice(ev.currentTarget.dataset.i, 1)
        }
        if (this.data.resultList.length == 0) {
            this.hideCart();
        }
        this.getTotalInfo();
    },

    jia(ev) {
        this.data.resultList[ev.currentTarget.dataset.i].N_NUM++;
        this.updateGoodsList(this.data.resultList[ev.currentTarget.dataset.i].ID, this.data.resultList[ev.currentTarget.dataset.i].N_NUM);
        this.getTotalInfo();
    },

    updateGoodsList(id, num) {
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.goodsList[i].ID == id) {
                this.data.goodsList[i].N_NUM = num;
            }
        }

        this.setData({
            goodsList: this.data.goodsList
        })
    },

    hideCart() {
        this.setData({
            showCart: false,
        })
    },

    setRemark2(ev) {
        var idx = ev.currentTarget.dataset.i,
            d = ev.currentTarget.dataset.d,
            n = ev.currentTarget.dataset.n;
        this.data.currentRemark = ev.currentTarget.dataset.d;
        // for (var i = 0; i < this.data.Remarks.length; i++) {
        //     this.data.Remarks[i].checked = false;
        // }

        this.data.Remarks[idx].checked = !this.data.Remarks[idx].checked;
        this.setData({
            Remarks: this.data.Remarks,
        })
    },

    getTableInfo() {
        var shopNo = this.data.ShopNo;
        var str = `N_CID&ShopNo=${this.data.zpid}*${shopNo}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 2100,
            N_CID: this.data.zpid,
            ShopNo: shopNo,
            MD5: md5.toUpperCase()
        };

        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                if (res.data.code === "100") {
                    g.tableId = res.data.store_ewm[0].TID
                    this.setData({
                        storeEwm: res.data.store_ewm[0] 
                    })
                    this.getTableDetail();
                }
            }
        })
    },
    // 获取桌台信息
    getTableDetail() {
        tableControllor.queryTableUser({
            TID: g.tableId,
            ShopNo: this.data.ShopNo,
        }, data => {
            if (data.StoreCWF) {
                data.TableInfo.StoreCWF = data.StoreCWF
            } else {
                data.TableInfo.StoreCWF = 0;
            }
            wx.setStorage({
                key: 'zt-info',
                data: data.TableInfo,
            })
            var stateClass = "state1";
            if (data.TableInfo.ZTSTATE == "空闲") {
                stateClass = "state1"
            } else if (data.TableInfo.ZTSTATE == "使用中") {
                stateClass = "state2"
            } else if (data.TableInfo.ZTSTATE == "待清理") {
                stateClass = "state3"
            } else if (data.TableInfo.ZTSTATE == "预结帐") {
                stateClass = "state4"
            }
            this.setData({
                ztInfo: data.TableInfo,
                stateClass
            })

            this.checkOrderStatus("1");
        })
    },

    getGoodsClassify() {
        var md5 = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
        var data = {
            bNo: 3100,
            ShopNo: this.data.ShopNo,
            MD5: md5,
        }

        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                for (var i = 0; i < res.data.Result.length; i++) {
                    if (res.data.Result[i].CATEGORY_NAME.length > 4) {
                        res.data.Result[i].CATEGORY_NAME = res.data.Result[i].CATEGORY_NAME.substr(0, 4) + "..."
                    }
                    res.data.Result[i].num = 0;
                }
                this.setData({
                    cid: res.data.Result[0].ID,
                    classify: res.data.Result,
                })
                this.getGoodsList();
            }
        })
    },

    changeClassify(ev) {
        this.setData({
            goodsList: [],
            pageindex: 1,
            totalPage: 0,
            activeItem: ev.currentTarget.dataset.d,
            cid: ev.currentTarget.dataset.i,
        }, () => {
            this.getGoodsList();
        })
    },

    loadGoods() {
        if (this.data.pageindex < this.data.totalPage) {
            this.data.pageindex++;
            this.getGoodsList();
        }
    },

    getGoodsList() {
        wx.showLoading({
            title: '加载中',
            mask: true,
        })
        var str = `ShopNo&pageindex&pagesize&CateId=${this.data.ShopNo}*${this.data.pageindex}*10*${this.data.cid}`;
        var md5 = util.md5(str);
        var data = {
            bNo: "4005",
            pageindex: this.data.pageindex,
            pagesize: 10,
            mebno: this.data.mebno,
            CateId: this.data.cid,
            ShopNo: this.data.ShopNo,
            MD5: md5.toUpperCase()
        }

        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                wx.hideLoading()
                if (this.data.totalPage == 0) {
                    this.data.totalPage = res.data.totalPage;
                }
                var arr = [];
                var noData = false;
                for (var i = 0; i < res.data.dtResult.length; i++) {
                    if (!res.data.dtResult[i].N_IsSellOut) {
                        res.data.dtResult[i].N_IsSellOut = 0;
                    }
                    res.data.dtResult[i].listImg = res.data.dtResult[i].C_MAINPICTURE
                    if (res.data.dtResult[i].GOODS_NAME.length > 24) {
                        res.data.dtResult[i].GOODS_NAME = res.data.dtResult[i].GOODS_NAME.substr(0, 24) + "..."
                    }
                    res.data.dtResult[i].C_MAINPICTURE = config.FilePath + res.data.dtResult[i].C_MAINPICTURE;
                    res.data.dtResult[i].N_NUM = 0;
                    this.data.goodsList.push(res.data.dtResult[i]);
                }

                if (res.data.dtResult.length < 10) {
                    noData = true;
                }

                for (var i = 0; i < this.data.resultList.length; i++) {
                    for (var j = 0; j < this.data.goodsList.length; j++) {
                        if (this.data.resultList[i].ID == this.data.goodsList[j].ID) {
                            this.data.goodsList[j].N_NUM = this.data.resultList[i].N_NUM
                        }
                    }
                }
                this.setData({
                    goodsList: this.data.goodsList,
                    noData
                })

                wx.hideLoading()
            }
        })
    },

    showSpec(ev) {
        console.log(ev.currentTarget.dataset)
        if (ev.currentTarget.dataset.d.N_IsSellOut == 1) {
            wx.showToast({
                title: '商品已售罄',
                icon: "none"
            })
            return;
        }
        // 判断是否允许负库存销售
        // 如果可以，则不允许负库存销售，则不判断
        if (!this.data.allowSale) {
            if (ev.currentTarget.dataset.d.STOCK <= 0) {
                wx.showToast({
                    title: '库存不足',
                    icon: "none"
                })
                return;
            }
        }

        wx.showLoading({
            title: '加载中',
            mask: true,
        })
        this.data.Spv1 = 0;
        this.data.Spv2 = 0;
        this.data.Spv3 = 0;
        this.data.currentGoodsIndex = ev.currentTarget.dataset.t;
        this.setData({
            currentGoodsNum: ev.currentTarget.dataset.d.N_NUM || 1,
            C_GoodsRemark: "",
            currentPractice: null,
            currentRemark: null,
        })
        goodsControllor.getGoodsDetail701({
            gid: ev.currentTarget.dataset.i,
            ShopNo: this.data.ShopNo,
            mebno: this.data.mebno,
        }, data => {
            console.log('----data规格')
            console.log(data)
            wx.hideLoading()
            this.data.GbId = data.goods[0].N_GB_ID
            data.goods[0].C_MAINPICTURE = config.FilePath + data.goods[0].C_MAINPICTURE;
            this.filterLvgg(data.goods[0].SPECIFICATION, data.lvgg);
            data.goods[0].currentPractice = null;
            data.goods[0].Batchings = null;
            this.data.goodsList[ev.currentTarget.dataset.t].GOODS_CODE = data.goods[0].GOODS_CODE
            data.goods[0].lptcdt = data.lptcdt //套餐商品
            this.setData({
                GoodsImgs: data.GoodsImgs,
                currentGoods: data.goods[0],
                currentAttr: data.lvgg,
                lptcdt: data.lptcdt,
                detailCover: true,
                dialogType: 1,
                // spec: 1,
            })
            //备注、做法、加料
            this.getGoodsOtherInfo(data.goods[0].N_GB_ID, ev.currentTarget.dataset.t)
        })
    },

    /**
     * 获取商品的做法等信息
     */
    getGoodsOtherInfo(GoodsID, idx) {
        goodsControllor.getGoodsOtherInfo({
            GoodsID,
            ShopNo: this.data.ShopNo,
        }, data => {
            this.data.goodsList[idx].hasData = false;
            if (data.Batchings) {
                // 设置加料
                for (var i = 0; i < data.Batchings.length; i++) {
                    data.Batchings[i].checked = false;
                    data.Batchings[i].num = 1;
                }
                this.setData({
                    Batchings: data.Batchings
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].Batchings = data.Batchings;
            } else {
                this.setData({
                    Batchings: []
                })
            }
            // 设置做法
            if (data.PracticeOrType) {
                for (var i = 0; i < data.PracticeOrType.length; i++) {
                    for (var j = 0; j < data.PracticeOrType[i].goods_practice.length; j++) {
                        data.PracticeOrType[i].goods_practice[j].checked = false;
                    }
                }
                this.setData({
                    PracticeOrType: data.PracticeOrType
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].PracticeOrType = data.PracticeOrType;
            } else {
                this.setData({
                    PracticeOrType: []
                })
            }
            // 设置备注
            if (data.Remarks) {
                for (var i = 0; i < data.Remarks.length; i++) {
                    data.Remarks[i].checked = false;
                }
                this.setData({
                    Remarks: data.Remarks
                })
                this.data.goodsList[idx].hasData = true;
                this.data.goodsList[idx].Remarks = data.Remarks;
                this.setData({
                    spec: 4,
                })
            } else {
                this.setData({
                    Remarks: [],
                    spec: 1,
                })
            }
            wx.hideLoading()
            this.setData({
                goodsList: this.data.goodsList
            })
        })
    },

    /**
     * 选择加料
     */
    chooseThisBatching(ev) {
        this.data.Batchings[ev.currentTarget.dataset.i].checked = !this.data.Batchings[ev.currentTarget.dataset.i].checked;
        for (var i = 0; i < this.data.goodsList.length; i++) {
            if (this.data.currentGoods.ID == this.data.goodsList[i].ID) {
                this.data.goodsList[i].Batchings = this.data.Batchings;
            } else {
                this.data.goodsList[i].Batchings = null;
            }
        };
        this.setData({
            Batchings: this.data.Batchings,
            goodsList: this.data.goodsList
        })
        this.setChooseBatchings();
    },

    setRemark(ev) {
        if (ev.detail.value.length < 100) {
            this.data.C_GoodsRemark = ev.detail.value;
            this.data.C_GoodsRemark2 = ev.detail.value;
            this.setData({
                textLength: 100 - ev.detail.value.length,
                C_GoodsRemark: this.data.C_GoodsRemark
            })
        }
    },
    // 确定加入购物车
    sureGoods() {
        console.log(this.data.currentGoodsNum)
        console.log(this.data.currentGoods)
        console.log(this.data.goodsList)
        if (this.data.currentGoodsNum > 0) {
            this.data.currentGoods.N_NUM = this.data.currentGoodsNum
            var flag = false;
            this.data.goodsList[this.data.currentGoodsIndex].N_NUM = this.data.currentGoodsNum;
            var Remark_Name = [];
            if (this.data.Remarks) {
                for (var i = 0; i < this.data.Remarks.length; i++) {
                    if (this.data.Remarks[i].checked) {
                        Remark_Name.push(this.data.Remarks[i].Remark_Name);
                    }
                }
            };

            if (Remark_Name.length > 0) {
                // this.data.C_GoodsRemark = Remark_Name.join(",");
                if (this.data.C_GoodsRemark) {
                    this.data.currentGoods.C_GoodsRemark = Remark_Name.join(",") + "," + this.data.C_GoodsRemark;
                    this.data.goodsList[this.data.currentGoodsIndex].C_GoodsRemark = Remark_Name.join(",") + "," + this.data.C_GoodsRemark;
                } else {
                    this.data.currentGoods.C_GoodsRemark = Remark_Name.join(",");
                    this.data.goodsList[this.data.currentGoodsIndex].C_GoodsRemark = Remark_Name.join(",")
                }
            } else {
                if (this.data.C_GoodsRemark) {
                    this.data.currentGoods.C_GoodsRemark = this.data.C_GoodsRemark;
                    this.data.goodsList[this.data.currentGoodsIndex].C_GoodsRemark = this.data.C_GoodsRemark;
                } else {
                    this.data.currentGoods.C_GoodsRemark = "";
                    this.data.goodsList[this.data.currentGoodsIndex].C_GoodsRemark = "";
                }
            }

            if (this.data.currentPractice) {
                this.data.currentGoods.currentPractice = this.data.currentPractice;
            }

            if (this.data.Batchings) {
                this.data.currentGoods.Batchings = this.data.Batchings;
            }

            if (this.data.resultList.length > 0) {
                for (var i = 0; i < this.data.resultList.length; i++) {
                    if (this.data.resultList[i].ID == this.data.currentGoods.ID) {
                        this.data.resultList[i] = this.data.goodsList[this.data.currentGoodsIndex];
                        flag = true;
                        break;
                    }
                }

                if (!flag) {
                    this.data.resultList.push(this.data.currentGoods)
                    // this.data.resultList.push(this.data.goodsList[this.data.currentGoodsIndex])
                }
            } else {
                this.data.resultList.push(this.data.currentGoods)
                // this.data.resultList.push(this.data.goodsList[this.data.currentGoodsIndex])
            }

            this.setData({
                goodsList: this.data.goodsList,
            })

            this.getTotalInfo();
            this.hideDialog();
        } else {
            wx.showModal({
                title: '提示',
                content: '请设置数量',
                showCancel: false,
            })
        }
        this.hideDialog();
    },

    getTotalInfo() {
        var totalNum = 0;
        var totalPrice = 0;
        // =============================================
        console.log('this.data.resultList-----------------------')
        console.log(this.data.resultList)
        // =============================================
        for (var i = 0; i < this.data.resultList.length; i++) {
            if (this.data.resultList[i].currentPractice) {
                totalPrice += this.data.resultList[i].currentPractice.Practice_Price;
            }

            if (this.data.resultList[i].Batchings) {
                for (var j = 0; j < this.data.resultList[i].Batchings.length; j++) {
                    if (this.data.resultList[i].Batchings[j].checked) {
                        totalPrice += this.data.resultList[i].Batchings[j].Batching_Price * this.data.resultList[i].Batchings[j].num;
                    }
                }
            }

            totalPrice += this.data.resultList[i].SALE_PRICE * this.data.resultList[i].N_NUM;
            totalNum += this.data.resultList[i].N_NUM;
        }

        totalPrice = parseFloat(totalPrice).toFixed(2)
        // =============================================
        console.log(totalNum)
        // =============================================
        this.setData({
            totalNum,
            totalPrice,
            resultList: this.data.resultList,
        })
    },

    /**
     * 设置当前选中的加料
     */
    setChooseBatchings() {
        var batchPirce = 0;
        for (var i = 0; i < this.data.Batchings.length; i++) {
            if (this.data.Batchings[i].checked) {
                batchPirce += this.data.Batchings[i].Batching_Price * this.data.Batchings[i].num
            }
        }
    },

    /**
     * 切换做法选择
     */
    choosePracitice(ev) {
        var i1 = ev.currentTarget.dataset.i;
        var i2 = ev.currentTarget.dataset.j;
        if (this.data.PracticeOrType[i1].goods_practice[i2].checked) {
            this.data.totalPrice -= this.data.PracticeOrType[i1].goods_practice[i2].Practice_Price;
            this.data.PracticeOrType[i1].goods_practice[i2].checked = false;
            this.data.currentPractice = null
        } else {
            for (var i = 0; i < this.data.PracticeOrType.length; i++) {
                for (var j = 0; j < this.data.PracticeOrType[i].goods_practice.length; j++) {
                    this.data.PracticeOrType[i].goods_practice[j].checked = false;
                }
            }
            this.data.PracticeOrType[i1].goods_practice[i2].checked = true;
            this.data.currentPractice = this.data.PracticeOrType[i1].goods_practice[i2];

            for (var i = 0; i < this.data.goodsList.length; i++) {
                if (this.data.goodsList[i].ID == this.data.currentGoods.ID) {
                    this.data.goodsList[i].currentPractice = this.data.PracticeOrType[i1].goods_practice[i2];
                    this.data.goodsList[i].PracticeOrType = this.data.PracticeOrType;
                }
            }
            this.data.totalPrice += this.data.PracticeOrType[i1].goods_practice[i2].Practice_Price
        }

        this.setData({
            totalPrice: this.data.totalPrice,
            goodsList: this.data.goodsList,
            PracticeOrType: this.data.PracticeOrType,
        })
    },

    /**
     * 筛选规格
     */
    filterLvgg(t, lvgg) {
        // 切出当前商品的规格
        var attr = t.split(" ");
        this.data.lvgg = lvgg;
        // 去掉多余的数组
        attr.pop();
        var gAttr = [];
        // 筛选规格，只留下规格值
        for (var i = 0; i < attr.length; i++) {
            gAttr.push(attr[i].split(":")[1]);
        }
        // 判断哪个规格是选中状态
        for (var i = 0; i < gAttr.length; i++) {
            for (var j = 0; j < lvgg[i].SpvList.length; j++) {
                if (lvgg[i].SpvList[j].C_SPE_VALUE == gAttr[i]) {
                    lvgg[i].SpvList[j].checked = true;
                    switch (i) {
                        case 0:
                            this.data.Spv1 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                        case 1:
                            this.data.Spv2 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                        case 2:
                            this.data.Spv3 = lvgg[i].SpvList[j].N_SPV_ID;
                            break;
                    }
                }
            }
        }

        // 设置规格值
        this.setData({
            currentAttr: lvgg,
        })
    },

    /**
     * 当前菜品的增加
     */
    plus() {
        this.data.currentGoodsNum++;
        this.setData({
            currentGoodsNum: this.data.currentGoodsNum
        })
    },

    /**
     * 商品数量相减
     */
    minus() {
        this.data.totalNum--;
        if (this.data.currentGoodsNum > 0) {
            this.data.currentGoodsNum--;
            if (this.data.currentGoodsNum == 0) {
                for (var i = 0; i < this.data.resultList.length; i++) {
                    if (this.data.resultList[i].ID == this.data.goodsList[this.data.currentGoodsIndex].ID) {
                        this.data.resultList.splice(i, 1)
                    }
                }
                this.getTotalInfo();
            }
            if (this.data.goodsList[this.data.currentGoodsIndex].N_NUM - 1 > 0) {
                this.data.goodsList[this.data.currentGoodsIndex].N_NUM--;
            } else {
                this.data.goodsList[this.data.currentGoodsIndex].N_NUM = 0;
            }

            this.setData({
                currentGoodsNum: this.data.currentGoodsNum,
                totalNum: this.data.totalNum,
                goodsList: this.data.goodsList,
            })
        } else {
            this.data.goodsList[this.data.currentGoodsIndex].N_NUM = 0;
            this.setData({
                currentGoodsNum: 0,
                totalNum: this.data.totalNum,
                goodsList: this.data.goodsList,
            })
        }
    },

    /**
     * 
     */
    changeSpecTab(ev) {
        this.setData({
            spec: ev.currentTarget.dataset.i
        })
    },
    // 去下单
    sureOrder() {
        const ztInfo = this.data.ztInfo

        if (ztInfo.ZTSTATE == "待清理") {
            wx.showModal({
                title: '提示',
                content: '待清理的桌台不能下单，请等待商家清桌后，下拉刷新页面重试',
                showCancel: false,
            })
            return;
        }

        if (this.data.resultList.length > 0) {
            wx.setStorage({
                key: 'result-list',
                data: {
                    gList: this.data.resultList,
                    totalPrice: this.data.totalPrice,
                    totalNum: this.data.totalNum
                },
                success: res => {
                    this.setData({ showCart: false})
                    wx.navigateTo({
                        url: '/pages/submitGoods/submitGoods',
                    })
                }
            })
        } else {
            wx.showModal({
                title: '提示',
                content: '请先选菜',
                showCancel: false,
            })
        }
    },

    /**
     * 切换规格
     */
    changeAttr(ev) {
        var index = ev.currentTarget.dataset.i;
        var sIndex = ev.currentTarget.dataset.si;

        switch (index) {
            case 0:
                this.data.Spv1 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
            case 1:
                this.data.Spv2 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
            case 2:
                this.data.Spv3 = this.data.currentAttr[index].SpvList[sIndex].N_SPV_ID;
                break;
        };

        this.getGoodsDetailAttr()
    },

    /**
     * 获取规格商品
     */
    getGoodsDetailAttr() {
        wx.showLoading({
            title: '加载中',
        })
        goodsControllor.getGoodsDetailAttr({
            GbId: this.data.GbId,
            Spv1: this.data.Spv1,
            Spv2: this.data.Spv2,
            Spv3: this.data.Spv3,
            mebno: this.data.mebno,
            ShopNo: this.data.ShopNo
        }, data => {
            // 筛选规格，判断选中规格
            this.filterLvgg(data.goods[0].SPECIFICATION, data.lvgg);
            data.goods[0].C_MAINPICTURE = config.FilePath + data.goods[0].C_MAINPICTURE;
            this.setData({
                currentGoods: data.goods[0]
            })
            wx.hideLoading()
        })
    },



    def() {

    },

    hideDialog() {
        this.setData({
            dialogType: 0,
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.getTableDetail();
        this.getSureOrderConfig();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 500)
    },
    // 404 获取我的订单详情
    checkOrderStatus(froms) {
        common.getOrderStatus(this, this.data.ztInfo.C_ORD_NO)
        var str = `ShopNo&OrderNo=${this.data.ShopNo}*${this.data.ztInfo.C_ORD_NO}`;
        var md5 = util.md5(str);
        var data = {
            bNo: 4004,
            ShopNo: this.data.ShopNo,
            OrderNo: this.data.ztInfo.C_ORD_NO,
            MD5: md5.toUpperCase()
        };

        request(data, (res) => {
            console.log('4004----------')
            console.log(this.data.ztInfo.C_ORD_NO)
            console.log(res)
            var flag = false;
            if (res.Examine != 2) { //审核状态 0未审1已审2拒绝 
                // if (!res.gbase) {
                //     this.data.ztInfo.C_ORD_NO = null;
                // }
            }
            if (res.gbase) {
                for (var i = 1; i < res.gbase.length; i++) {
                    if (res.gbase[i].GZT == 3) { //待确认的菜品
                        flag = true;
                        break;
                    }
                }
            }
            wx.setStorage({
                key: 'scan-order-status',
                data: !flag ? res.Examine : 0,
            })
            this.setData({
                orderStatus: !flag ? res.Examine : 0,
                ztInfo: this.data.ztInfo,
                gbase: res.gbase
            })
            wx.setStorage({
                key: 'zt-info',
                data: this.data.ztInfo,
            })

        })
    },
})