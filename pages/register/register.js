// pages/register/register.js
var config = require("../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msgActive: false,
    msgTips: "获取验证码",
    tel: "",
    tel2: "",
    vcode: "", // 验证码
    yzm: "",
    shop: "",
    pwd: "",
    qdh: "",
    pass: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  setTel(ev) {
    var v = ev.detail.value;
    if (/^1\d{10}/.test(v)) {
      this.setData({
        msgActive: true,
      })
    } else {
      this.setData({
        msgActive: false,
      })
    }
    this.setData({
      tel: v,
    })
    this.checkPass();
  },

  sendMsg() {
    if (this.data.msgActive) {
      this.data.tel2 = this.data.tel;
      this.setData({
        msgActive: false,
      });

      var t = 60;
      var timer = setInterval(() => {
        if (t - 1 > 0) {
          t--;
          this.setData({
            msgTips: `${t}s后重新获取`
          });
        } else {
          t = 60;
          this.setData({
            msgActive: true,
            msgTips: `获取验证码`
          });
          clearInterval(timer);
        }
      }, 1000)

      var data = {
        bNo: "SendMes",
        Phone: this.data.tel,
      };

      wx.request({
        url: config.requestUrl,
        data,
        success: res => {
          var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
          if (data.vcode) {
            this.data.vcode = data.vcode;
            wx.showToast({
              title: '已发送验证码',
              icon: 'success',
              duration: 2000
            })
          } else {
            clearInterval(timer);
            t = 60;
            this.setData({
              msgTips: `获取验证码`,
              msgActive: true,
            });
            wx.showModal({
              title: '提示',
              content: data.msg,
              showCancel: false,
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '请填写正确的手机号',
        showCancel: false
      })
    }
  },

  setyzm(ev) {
    this.data.yzm = ev.detail.value;
    this.checkPass();
  },

  setshop(ev) {
    this.data.shop = ev.detail.value;
    this.checkPass();
  },

  setpwd(ev) {
    this.data.pwd = ev.detail.value;
    this.checkPass();
  },

  setqdh(ev) {
    this.data.qdh = ev.detail.value;
    this.setData({ qdh: this.data.qdh})
      console.log(this.data.qdh)
    this.checkPass();
  },

  checkPass() {
      if (this.data.yzm && this.data.shop && this.data.pwd && this.data.qdh != '') {
      this.setData({
        pass: true,
      })
    } else {
      this.setData({
        pass: false,
      })
    }
  },

  reg() {

    // if (!/^1\d{10}/.test(this.data.tel)) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '请填写正确的手机号',
    //     showCancel: false
    //   })
    //   return;
    // }

    // if (this.data.vcode != this.data.yzm || !this.data.vcode) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '验证码错误',
    //     showCancel: false
    //   })
    //   return;
    // }

    // if (this.data.shop.length < 1) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '请填写店铺名称',
    //     showCancel: false
    //   })
    //   return;
    // }

    // if (this.data.pwd.length < 6) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '登录密码最少为6位',
    //     showCancel: false
    //   })
    //   return;
    // }
    if (this.data.qdh == ''){
        wx.showModal({
            title: '提示',
            content: '请填写邀请码',
            showCancel: false
        })
        return;
    }
    if (this.data.pass) {
    //   var qdh = this.data.qdh ? this.data.qdh : "13883385308";
      var data = {
        bNo: "RegZg",
        Phone: this.data.tel,
        shopName: this.data.shop,
        pwd: this.data.pwd,
        yqm: this.data.qdh,
      };

      wx.request({
        url: config.requestUrl,
        data,
        success: res => {
          var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
          if (data.result == "1") {
            wx.removeStorage({
              key: 'OP_INFO',
              success: () => {
                if (data.msg != "注册成功") {
                  wx.showModal({
                    title: '提示',
                    content: data.msg,
                    showCancel: false,
                    success: res => {
                      if (res.confirm) {
                        wx.reLaunch({
                          url: '/pages/login/login',
                        })
                      }
                    }
                  })
                } else {
                  wx.showToast({
                    title: data.msg,
                    duration: 1000,
                    success: res => {
                      setTimeout(() => {
                        wx.reLaunch({
                          url: '/pages/login/login',
                        })
                      }, 1000)
                    }
                  })
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              content: data.msg,
              showCancel: false
            })
          }
        }
      })
    }
  },

  /**
   * msg:"您已经是微客掌柜的商户了，请直接登录"
   * result:"1"
   */

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})