var g = require('../../utils/goodsCommon.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    classify: [],
    edit: false,
    editArr: [],
    opInfo: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.setData({
          opInfo: res.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  edit() {
    this.setData({
      edit: true
    })
  },

  cancel() {
    this.setData({
      edit: false
    })
  },

  del() {
    var id = [];
    for (var i = 0; i < this.data.classify.length; i++) {
      if (this.data.classify[i].checked) {
        id.push(this.data.classify[i].ID + "☆" + this.data.classify[i].CATEGORY_NAME);
      };
    }
    wx.showLoading({
      title: '',
    })
    g.delGoodsClassify({
      bNo: 3101,
      ShopNo: this.data.shopNo,
      delCateId: id.join(";"),
      opid: this.data.opInfo.n_ss_id,
      opname: this.data.opInfo.c_name,
    }, (data) => {
      if (data.code === "100" && data.Result == "1") {
        wx.showToast({
          title: '删除成功',
          duration: 1000,
          mask: true,
          success: res => {
            setTimeout(() => {
              wx.navigateBack({
                delta: 1
              })
            }, 1000)
          }
        })
      } else {
        wx.hideLoading()
      }
    })

    // this.setData({
    //   edit: false
    // })
    // wx.showModal({
    //   title: '提示',
    //   content: '删除成功',
    //   showCancel: false,
    // })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getGoodsClassify();
      },
    })
  },

  chooseClassify(ev) {
    if (this.data.edit) {
      this.data.editArr = [];
      this.data.classify[ev.currentTarget.dataset.i].checked = !this.data.classify[ev.currentTarget.dataset.i].checked;
      for (var i = 0; i < this.data.classify.length; i++) {
        if (this.data.classify[i].checked) {
          this.data.editArr.push(i);
        }
      }
      this.setData({
        classify: this.data.classify
      })
    } else {
      wx.navigateTo({
        url: '/pages/editGoodsClassify/editGoodsClassify?cid=' + ev.currentTarget.dataset.id,
      })
    }
  },
  
  addClassify() {
    wx.navigateTo({
      url: '/pages/editGoodsClassify/editGoodsClassify?cid='
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 获取商品分类
   */
  getGoodsClassify() {
    wx.showLoading({
      title: '',
      mask: true
    })
    g.getGoodsClassify({
      bNo: 3100,
      ShopNo: this.data.shopNo,
    }, (data) => {
        if(data.length > 0){
            for (var i = 0; i < data.length; i++) {
                var str = "";
                for (var j = 0; j < parseInt(data[i].Level); j++) {
                    str += "-"
                }
                data[i].levelText = str;
                data[i].checked = false;
            }
            this.setData({
                flid: data[0].ID,
                classify: data,
            })
        }
      wx.hideLoading()
    })
  },
})