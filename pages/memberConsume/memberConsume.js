var config = require("../../config.js")
var util = require("../../utils/MD5.js")

// pages/memberConsume.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    member: {},
    noData: true,
    filePath: config.FilePath,
    C_SHOP_NO: "",
    sCount: [],
    gCount: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    wx.getStorage({
      key: 'member_data',
      success: res => {
        var member = res.data;

        member.D_AddDate = member.D_AddDate ? member.D_AddDate.replace("T", " ").substr(0, member.D_AddDate.lastIndexOf(":")) : ""
        member.D_OpDate = member.D_OpDate ? member.D_OpDate.replace("T", " ").substr(0, member.D_OpDate.lastIndexOf(":")) : ""
        member.D_PrevDate = member.D_PrevDate ? member.D_PrevDate.replace("T", " ").substr(0, member.D_PrevDate.lastIndexOf(":")) : ""
        member.sex = member.sex ? member.sex : 1
        this.setData({
          member,
        })

        wx.getStorage({
          key: 'C_SHOP_NO',
          success: res => {
            this.data.C_SHOP_NO = res.data;
            if (member.C_MB_NO != 0) {
              this.getSaleCount();
              this.getGoodsCount();
            }
          },
        })
      },
    })
  },

  editMember(ev) {
    var no = ev.currentTarget.dataset.no;
    var data = ev.currentTarget.dataset.d;
    wx.removeStorage({
      key: 'choose_member_tag',
      success: function (res) { },
    })
    wx.navigateTo({
      url: '/pages/editMember/editMember?memberNo=' + no + "&data=" + JSON.stringify(data),
    })
  },


  getSaleCount() {
    // 116855
    var str = "ShopNo&mebno=" + this.data.C_SHOP_NO + "*" + this.data.member.C_MB_NO;
    // var str = "ShopNo&mebno=" + this.data.C_SHOP_NO + "*" + 116855;
    var md5 = util.md5(str);
    var data = {
      bNo: 1752,
      ShopNo: this.data.C_SHOP_NO,
      MebNo: this.data.member.C_MB_NO,
      // MebNo: 116855,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        var data2 = JSON.parse(data.Result);
        if (data2 == null) {
          data2 = []
        }
        this.setData({
          sCount: data2,
        })
      }
    })
  },

  showBigPic(ev) {
    var src = ev.currentTarget.dataset.src;
    console.log(src)
    wx.previewImage({
      urls: [src],
    })
  },

  getGoodsCount() {
    var str = "ShopNo&mebno=" + this.data.C_SHOP_NO + "*" + this.data.member.C_MB_NO;
    // var str = "ShopNo&mebno=" + this.data.C_SHOP_NO + "*" + 116855;
    var md5 = util.md5(str);
    var data = {
      bNo: 1753,
      ShopNo: this.data.C_SHOP_NO,
      MebNo: this.data.member.C_MB_NO,
      // MebNo: 116855,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        var data2 = JSON.parse(data.Result);
        for (var i = 0; i < data2.length; i++) {
          if (data2[i].GOODS_NAME.length > 10) {
            data2[i].GOODS_NAME = data2[i].GOODS_NAME.substr(0, 10) + "..."
          }

        }
        this.setData({
          gCount: data2,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})