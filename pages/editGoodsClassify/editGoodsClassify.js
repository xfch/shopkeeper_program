// pages/editClassify/editClassify.js
var g = require('../../utils/goodsCommon.js')
var config = require("../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cTxt: "顶级分类",
    cid: "0",
    cate: {
      PARENT_ID: 0,
    },
    classify: [],
    subPic: "",
    opInfo: {},
    oldPenid: 0,
    showChoose: false,
    isEdit: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.cid) {
      this.data.cid = options.cid;
      this.data.isEdit = true;
    }

    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;
      },
    })
      wx.getStorage({
          key: 'C_SHOP_NO',
          success: res => {
              console.log(res)
              this.data.shopNo = res.data
              this.getGoodsClassify();
          },
      })
    
    // this.getGoodsClassify();
  },
  
  setClassName(ev) {
    this.data.cate.CATEGORY_NAME = ev.detail.value
  },

  setGoodsCode(ev) {
    this.data.cate.SinNumber = ev.detail.value
  },

  setRemark(ev) {
    this.data.cate.C_CA_REMARK = ev.detail.value
  },

  chooseClassify() {
    this.setData({
      showChoose: true
    })
  },

  bindChange(ev) {
    if (this.data.classify[ev.detail.value[0]].ID != this.data.cid) {
      this.data.cate.PARENT_ID = this.data.classify[ev.detail.value[0]].ID;
      this.setData({
        cTxt: this.data.classify[ev.detail.value[0]].CATEGORY_NAME
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '不能选择分类自身作为上级分类',
        showCancel: false,
      })
    }
  },

  hideDateChoose() {
    this.setData({
      showChoose: false
    })
  },

  cancel() {
    wx.navigateBack({
      delta: 1
    })
  },

  getCateDetail() {
    g.requestCall({
      bNo: "3103",
      ShopNo: this.data.shopNo,
      CateId: this.data.cid,
    }, (data) => {
      if (data.code === "100") {
        if (data.Result.CATEGORY_IMG) {
          data.Result.CATEGORY_IMG2 = data.Result.CATEGORY_IMG
          data.Result.CATEGORY_IMG = config.FilePath + data.Result.CATEGORY_IMG;
        }
        if (data.Result.PARENT_ID != 0) {
          for (var i = 0; i < this.data.classify.length; i++) {
            if (this.data.classify[i].ID == data.Result.PARENT_ID) {
              this.data.cTxt = this.data.classify[i].CATEGORY_NAME
            }
          }
        } else {
          this.data.cTxt = "顶级分类"
        }
        this.setData({
          oldPenid: data.Result.PARENT_ID,
          cate: data.Result,
          cTxt: this.data.cTxt,
        })
      }
    });
  },

  getGoodsClassify() {
    g.getGoodsClassify({
      bNo: 3100,
      ShopNo: this.data.shopNo,
    }, (data) => {
        console.log(data)
      data.unshift({
        CATEGORY_NAME: "顶级分类"
      })
      this.setData({
        classify: data,
      })

      if (this.data.cid != 0) {
        this.getCateDetail();
      }
    })
  },

  chooseClassifyIcon() {
    wx.chooseImage({
      count: 1,
      sizeType: ["compressed"],
      success: res => {
        this.uploadClassifyIcon(res.tempFilePaths[0]);
      },
    })
  },

  uploadClassifyIcon(src) {
    wx.showLoading({
      title: '正在上传',
    })
    wx.uploadFile({
      url: config.requestUrl + "?bNo=1907",
      filePath: src,
      name: 'file',
      formData: {
        filepath: encodeURI(src)
      },
      success: res => {
        if (res.data) {
          var data = res.data.replace(/\(|\)/g, "");

          var data2 = JSON.parse(data);
          this.data.cate.CATEGORY_IMG = config.FilePath + data2.filepath
          this.data.cate.CATEGORY_IMG2 = data2.filepath;
          this.setData({
            cate: this.data.cate
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '上传失败',
            showCancel: false
          })
        }
      },
      fail: msg => {
        wx.showModal({
          title: '提示',
          content: '上传失败',
          showCancel: false
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  saveClassify() {
    if (!this.data.cate.CATEGORY_NAME) {
      wx.showModal({
        title: '提示',
        content: '请填写分类名称',
        showCancel: false,
      })
      return
    }
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        var data = {
          ID: this.data.cid,
          N_SORT: this.data.cid,
          PARENT_ID: this.data.cate.PARENT_ID,
          CATEGORY_IMG: this.data.cate.CATEGORY_IMG2,
          CATEGORY_NAME: this.data.cate.CATEGORY_NAME,
          SinNumber: this.data.cate.SinNumber,
          C_CA_REMARK: this.data.cate.C_CA_REMARK,
          ShopNo: res.data,
        }
        g.requestCall({
          bNo: 3102,
          data: JSON.stringify(data),
          ShopNo: this.data.shopNo,
          opid: this.data.opInfo.n_ss_id,
          opname: this.data.opInfo.c_name,
          oldPenid: this.data.oldPenid,
        }, (data) => {
          if (data.code === "100" && data.Result) {
            var title = "修改成功"
            if (!this.data.isEdit) {
              title = "新增成功"
            }
            wx.showToast({
              title,
              duration: 1000,
              mask: true,
              success: res => {
                setTimeout(() => {
                  wx.navigateBack({
                    delta: 1
                  })
                }, 1000)
              }
            })
          }
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
          console.log(res)
        this.data.shopNo = res.data
      },
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
})