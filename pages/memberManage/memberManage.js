// pages/memberManage/memberManage.js
var config = require("../../config.js");
var util = require("../../utils/MD5.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    filter: false,
    edit: false,
    filterActive: 1,
    pageIndex: 1, // 分页起始页
    pageSize: 10, // 分页条数
    filterText: "筛选",
    gradeId: 0, // 分类筛选条件
    keytype: -1,
    PageCount: 0, // 总页数
    keywords: "",
    shopNo: "",
    noData: {
      status: false,
    }, // 没有数据
    mebdt: [], // 会员列表
    tag: [{
      type: -1,
      name: "全部"
    }, {
      type: 0,
      name: "实体店会员"
    }, {
      type: 1,
      name: "微信会员"
    }, {
      type: 11,
      name: "人脸会员"
    }, {
      type: 9,
      name: "支付宝会员"
    }, {
      type: 10,
      name: "POS会员"
    }, {
      type: 2,
      name: "今日过生日的"
    }, {
      type: 3,
      name: "即将过生日的"
    }, {
      type: 4,
      name: "已清卡会员"
    }, {
      type: 5,
      name: "已锁卡会员"
    }, {
      type: 6,
      name: "已挂失会员"
    }, {
      type: 8,
      name: "30日到期会员"
    }, {
      type: 12,
      name: "已到期会员"
    }],
    mebgradedt: [],
  },

  /**
   * 
   */
  edit() {
    this.setData({
      edit: true
    })
  },

  /**
   * 新增会员
   * 与修改会员公用一个页面
   */
  editMember() {
    var data = {
      N_Id:0
    }
    wx.removeStorage({
      key: 'member_data',
      success: function(res) {
        wx.navigateTo({
          url: '/pages/editMember/editMember?memberNo=0&data=' + JSON.stringify(data),
        })
      },
    })
  },

  /**
   * 查看会员分类
   */
  toMemberClassify() {
    wx.navigateTo({
      url: '/pages/memberClassify/memberClassify',
    })
  },

  /**
   * 筛选会员
   */
  setFilter(ev) {
    // 数据对象
    var n = ev.currentTarget.dataset.n;
    // 两种筛选的判断依据
    var t = ev.currentTarget.dataset.t;
    this.data.PageCount = 0;
    if (n.length > 4) {
      n = n.substr(0, 4) + "..."
    }
    this.setData({
      filterText: n.name,
      filter: false,
      keytype: t == "t" ? n.type : -1,
      gradeId: t == "c" ? n.N_MG_ID : 0,
      mebdt: [],
    });
    this.getMenberList();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 展开筛选选择框
   */
  filterMember() {
    this.setData({
      filter: !this.data.filter
    })
  },

  /**
   * 切换筛选访视
   */
  changeFilterType(ev) {
    this.setData({
      filterActive: ev.currentTarget.dataset.i
    })
  },

  /**
   * 设置按会员
   */
  setKeywords(ev) {
    this.data.keywords = ev.detail.value;
  },

  /**
   * 执行搜索
   */
  searchByKeywords() {
    this.data.pageIndex = 1;
    this.setData({
      mebdt: []
    })
    this.getMenberList(1);
  },

  /**
   * 隐藏筛选框
   */
  hideFilter() {
    this.setData({
      filter: false,
    })
  },

  /**
   * 编辑会员s
   */
  chooseMember(ev) {
    if (this.data.edit) {
      this.data.member[ev.currentTarget.dataset.i].checked = !this.data.member[ev.currentTarget.dataset.i].checked;
      this.setData({
        member: this.data.member
      })
    } else {
      wx.navigateTo({
        url: '/pages/memberDetail/memberDetail?mebno=' + ev.currentTarget.dataset.n + "&id=" + ev.currentTarget.dataset.id,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      mebdt: [],
      pageIndex: 1
    })
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getMenberList();
        this.getMemberClassify()
      },
      fail: () => {
      }
    })
  },

  getMemberClassify() {
    var str = `ShopNo=${this.data.shopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 1205,
      ShopNo: this.data.shopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var rd = JSON.parse(res.data.replace(/\(|\)/g, ""));
        if (rd.code === "100") {
          for (var i = 0; i < rd.mebgradedt.length; i++) {
            rd.mebgradedt[i].name = rd.mebgradedt[i].C_MG_NAME
          }
          rd.mebgradedt.unshift({
            C_MG_NAME: "全部",
            name: "全部",
            C_SHOP_NO: "100010",
            N_MG_ID: 0
          })
          this.setData({
            mebgradedt: rd.mebgradedt
          })
        }
      }
    })
  },

  /**
   * 获取会员列表
   */
  getMenberList(optType) {
    var str = `ShopNo&keywords&gradeId&pageIndex&pageSize=${this.data.shopNo}*${this.data.keywords}*${this.data.gradeId}*${this.data.pageIndex}*${this.data.pageSize}`;
    var md5 = util.md5(str);
    wx.showLoading({
      title: '',
    })
    var data = {
      bNo: 1204,
      ShopNo: this.data.shopNo,
      keywords: this.data.keywords,
      gradeId: this.data.gradeId,
      pageIndex: this.data.pageIndex,
      pageSize: this.data.pageSize,
      keytype: this.data.keytype,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var rd = JSON.parse(res.data.replace(/\(|\)/g, ""));
        if (rd.code === "100") {
          for (var i = 0; i < rd.mebdt.length; i++) {
            rd.mebdt[i].checked = false;
            if (!rd.mebdt[i].C_MB_PIC) {
              rd.mebdt[i].C_MB_PIC = "/images/d-face.jpg"
            } else {
              rd.mebdt[i].C_MB_PIC = config.FilePath + rd.mebdt[i].C_MB_PIC
            }
            this.data.mebdt.push(rd.mebdt[i]);
          }

          if (this.data.PageCount == 0) {
            this.data.PageCount = rd.PageCount
          }

          var noData = {
            status: false,
          };
          if (rd.PageCount == 1 && rd.mebdt.length < this.data.pageSize) {
            if (rd.mebdt.length > 0) {
              noData = {
                status: true,
                msg: "没有了~"
              }
            } else {
              noData = {
                status: true,
                msg: "暂无会员信息~"
              }
            }
          };

          if (this.data.pageIndex == 1 && rd.PageCount == 0) {
            noData = {
              status: true,
              msg: "暂无会员信息~"
            }
          }

          if (optType == 1 && rd.PageCount == 0) {
            noData = {
              status: true,
              msg: "没搜到任何信息~"
            }
          }

          this.setData({
            mebdt: this.data.mebdt,
            noData,
          })
        }
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  del() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.pageIndex + 1 <= this.data.PageCount) {
      this.data.pageIndex++;
      this.getMenberList();
    } else {
      var noData = {
        status: true,
        msg: "没有了~"
      }
      this.setData({
        noData,
      })
    }
  },
})
