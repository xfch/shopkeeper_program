// pages/chooseGoods/chooseGoods.js
var util = require("../../utils/MD5.js")
var config = require("../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mebno: "",
    goodscate: [],
    slideWidth: 0,
    classifyIndex: 0,
    ShopNo: "",
    goodsList: [],
    cid: 0,
    pageindex: 1,
    noData: false,
    tips: "",
    PageCount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getGoodsClassify();
      },
    })
  },

  changeClassify(ev) {
    this.setData({
      pageindex: 1,
      goodsList: [],
      cid: ev.currentTarget.dataset.d.ID,
      classifyIndex: ev.currentTarget.dataset.i
    })

    this.getGoodsList();
  },

  getGoodsClassify() {
    var str = "ShopNo=" + this.data.ShopNo;
    var md5 = util.md5(str);
    var data = {
      bNo: 704,
      ShopNo: this.data.ShopNo,
      MD5: md5.toUpperCase(),
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          var w = 0;
          for (var i = 0; i < res.data.goodscate.length; i++) {
            w += this.getStrLength(res.data.goodscate[i].name) + 40;
          }
          this.data.cid = res.data.goodscate[0].ID;
          this.getGoodsList();
          this.setData({
            slideWidth: w,
            goodscate: res.data.goodscate
          })
        }
      }
    })
  },

  viewGoods(ev) {
    wx.setStorage({
      key: 'goods_info',
      data: ev.currentTarget.dataset.d,
      success: res => {
        wx.navigateBack({
          delta: 1
        })
      }
    })
    // wx.redirectTo({
    //   url: '/pages/preDetails/preDetails?gid=' + ev.currentTarget.dataset.i,
    // })
  },

  getGoodsList() {
    var str = `pageindex&pagesize&cid&name&ShopNo=${this.data.pageindex}*10*${this.data.cid}**${this.data.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: "702b",
      pageindex: this.data.pageindex,
      pagesize: 10,
      name: "",
      goodsattr: 1,
      cid: this.data.cid,
      ShopNo: this.data.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.goodsList.length; i++) {
            if (res.data.goodsList[i].C_MAINPICTURE) {
              res.data.goodsList[i].C_MAINPICTURE = config.FilePath + res.data.goodsList[i].C_MAINPICTURE;
            } else {
              res.data.goodsList[i].C_MAINPICTURE = "/images/no-pic.png"
            }
            this.data.goodsList.push(res.data.goodsList[i])
          }
          if (this.data.PageCount == 0) {
            this.data.PageCount = res.data.PageCount;
          }
          if (res.data.goodsList.length < 10) {
            this.setData({
              noData: true,
              tips: "没有了~"
            })
          }
          this.setData({
            goodsList: this.data.goodsList
          })
        } else {
          if (this.data.pageindex == 1) {
            this.setData({
              noData: true,
              tips: "该分类无内容"
            })
          }
        }
      }
    })
  },

  getStrLength(str) {
    var c = 0;
    var len = 0;
    for (var j = 0; j < str.length; j++) {
      c = str.charCodeAt(j);
      if (c >= 0 && c <= 128) {
        len += 16;
      }
      else {
        len += 26
      }
    }

    return len;
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
   
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})