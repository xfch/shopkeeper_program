var config = require("../../config.js")
var util = require("../../utils/MD5.js")

// pages/memberTag/memberTag.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tagList: [],
    add: false,
    tagText: "",
    optId: "",
    err: false,
    errMessage: "请填写标签内容",
    tagStr: "",
    tagLength: 0,
    tagArr: [],
    edit: false,
    chooseTagSize: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.tag)
    this.setData({
      tagArr: options.tag.split(",")
    })
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.setData({
          optId: res.data.n_ss_id
        })
      },
    })

    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.SHOP_NO = res.data
        this.getShopTags();
      },
    })
  },

  getShopTags() {
    var str = "ShopNo=" + this.data.SHOP_NO;
    var md5 = util.md5(str);
    var data = {
      bNo: 1706,
      ShopNo: this.data.SHOP_NO,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          var tagLength = 0;
          var cArr = [];
          for (var i = 0; i < res.data.membertagList.length; i++) {
            for (var j = 0; j < this.data.tagArr.length; j++) {
              if (res.data.membertagList[i].C_TagName == this.data.tagArr[j]) {
                cArr.push(i);
                tagLength++
              } else {
                res.data.membertagList[i].checked = false;
              }
            }
          }
          for (var i = 0; i < cArr.length; i++) {
            res.data.membertagList[cArr[i]].checked = true;
          }
          this.setData({
            tagLength,
            tagList: res.data.membertagList
          })
        }
      }
    })
  },

  addTag() {
    this.setData({
      add: true
    })
  },

  cancelAdd() {
    this.setData({
      add: false
    })
  },

  setTagText(ev) {
    this.setData({
      tagText: ev.detail.value
    })
  },

  hideDialog() {
    this.setData({
      err: false,
    })
  },

  addTagFunc() {
    // n_ss_id
    if (!this.data.tagText) {
      this.setData({
        err: true,
      })
      return;
    }
    var flag = true;
    for (var i = 0; i < this.data.tagList.length; i++) {
      if (this.data.tagList[i].C_TagName === this.data.tagText) {
        flag = false;
      }

    }

    if (!flag) {
      wx.showModal({
        title: '提示',
        content: '该标签已存在，勿重复添加',
        showCancel: false,
      })
      return;
    }

    var str = "tagname&opname&opid&ShopNo=" + this.data.tagText + "**" + this.data.optId + "*" + this.data.SHOP_NO;
    var md5 = util.md5(str);
    var data = {
      bNo: 1707,
      tagname: this.data.tagText,
      opname: "",
      opId: this.data.optId,
      ShopNo: this.data.SHOP_NO,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          this.setData({
            add: false,
          })
          this.getShopTags();
        }
      }
    })
  },

  chooseTag(ev) {
    this.data.tagStr = [];
    var data = ev.currentTarget.dataset.i;
    var l = 0;
    for (var i = 0; i < this.data.tagList.length; i++) {
      if (data.C_TagName === this.data.tagList[i].C_TagName) {
        this.data.tagList[i].checked = !this.data.tagList[i].checked;
      }
      if (this.data.tagList[i].checked) {
        if (l < 5) {
          this.data.tagStr.push(this.data.tagList[i].C_TagName);
        }
        l++;
      }
    }
    if (l < 6) {
      this.setData({
        tagList: this.data.tagList,
        tagLength: l
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '最多只能选择五个标签',
        showCancel: false,
      })
    }
  },

  hidDialog() {
    this.setData({
      err: false,
    })
  },

  sureTag() {
    if (this.data.tagStr.length > 0) {
      wx.setStorage({
        key: 'choose_member_tag',
        data: this.data.tagStr.join(","),
      });
    }

    wx.navigateBack({
      delta: -1
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})