// pages/roleControllor/roleControllor.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  toManage() {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },

  toService() {
    wx.getStorage({
      key: 'shop_data',
      success: res => {
        if (res.data.n_yjhid == 390) {
          wx.navigateTo({
            url: '/pages/staffManage/myAchievement/myAchievement',
          })
        } else if (res.data.n_yjhid == 316) {
          wx.reLaunch({
            url: '/pages/waiter/index/index',
          })
        } else {
          wx.reLaunch({
            url: '/pages/retail/retail-order/retail-order',
          })
        }
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})