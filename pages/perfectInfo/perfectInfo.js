// pages/perfectInfo/perfectInfo.js
let config = require("../../config.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    c_shop_lx: 1,
    card_user_type: 1,
    t_type: 7,
    c_store_qc: "", // 企业全称（企业）
    yinye_code: "", // 工商执照注册号（企业）
    c_fr_name: "", // 法定代表人/企业负责人姓名（企业）
    card_code: "", // 身份证号（企业）
    card_start_date: "", // 证件有效期（企业）
    card_end_date: "", // 证件有效期（企业）
    yinye_rang: "", // 经营范围（企业）
    accountType: 2, // 结算账户类型
    accountProvince: "", // 开户城市
    accountCity: "", // 开户城市
    accountName: "", // 个人开户名称
    bankBranch: "", // 个人开户银行
    accountNO: "", // 个人银行账号
    cardId: "", // 结算人身份证号
    bankPhone: "", //结算人银行预留手机号码
    cardUnionNo: "", // 结算卡银行联行号
    c_lxr: "", // 联系人姓名
    c_lxphone: "", // 联系人手机号码
    c_lxemail: "", // 联系人电子邮箱
    c_appid: "", // AppId(小程序)
    c_appscript: "", // AppSecret(小程序)
    c_appid1: "", // AppId(公众号)
    c_appscript1: "", // AppSecret(公众号)
    card_type: 0,
    valueS: [[2], [2], [2]],
    valueE: [[2], [2], [2]],
    valueP: [[0], [0]],
    yinye_start_date: "",
    yinye_end_date: "",
    Ford1: true,
    negHeight: 40,
    Ford2: true,
    startData: {
      y: [],
      m: [],
      d: [],
    },
    endData: {
      y: [],
      m: [],
      d: [],
    },
    ys: "1990",
    ms: "1",
    ds: "1",
    ye: "2019",
    me: "1",
    de: "1",
    chooseStart: false,
    chooseEnd: false,
    chooseAdd1: false,
    chooseAdd2: false,
    showCover: false,
    dateT: 1,
    dateI: "",
    startDateText: "点击设置日期（生效）",
    endDateText: "点击设置日期（失效）",
    startDateText1: "点击设置日期（生效）",
    endDateText1: "点击设置日期（失效）",
    endDateText2: "",
    endDateText3: "",
    addArr1: [],
    addArr2: [],
    allArr: [],
    addText1: "选择开户省份",
    addText2: "选择开户区县",
    localInfo: "", // 本地数据
  },

  onLoad() {
    var date = new Date();
    var startData = {
      y: [],
      m: [],
      d: [],
    }

    var endData = {
      y: [],
      m: [],
      d: [],
    }
    for (var i = date.getFullYear(); i > 1970; i--) {
      startData.y.push(i);
      endData.y.push(i + 20);
    };

    for (var i = 1; i <= 12; i++) {
      startData.m.push(i);
      endData.m.push(i);
    };

    for (var i = 1; i <= 31; i++) {
      startData.d.push(i);
      endData.d.push(i);
    }

    this.setData({
      startData,
      endData,
    })
  },

  prevStep() {
    wx.navigateTo({
      url: "/pages/agreement/agreement"
    })
  },

  setForever(ev) {
    if (ev.currentTarget.dataset.i == 1) {
      if (ev.detail.value.length > 0) {
        this.setData({
          Ford1: false,
          card_end_date: "9999-01-01"
        })
      } else {
        this.setData({
          Ford1: true,
          card_end_date: this.data.endDateText2,
        })
      }
    } else {
      if (ev.detail.value.length > 0) {
        this.setData({
          Ford2: false,
          yinye_end_date: "3000-01-01"
        })
      } else {
        this.setData({
          Ford2: true,
          yinye_end_date: this.data.endDateText3,
        })
      }
    }
  },

  /**
   * 设置选中后的开始日期
   */
  bindChangeStart(ev) {
    var re = ev.detail.value;
    var day = this.comDay(this.data.startData.y[re[0]], this.data.startData.m[re[1]]);
    this.data.dateI = true;

    this.data.startData.d = [];
    for (var i = 1; i <= day; i++) {
      this.data.startData.d.push(i);
    }
    var m = 0;
    var d = 0;
    if (this.data.startData.m[re[1]] < 10) {
      m = "0" + this.data.startData.m[re[1]]
    } else {
      m = this.data.startData.m[re[1]]
    }
    if (this.data.startData.d[re[2]] < 10) {
      d = "0" + this.data.startData.d[re[2]]
    } else {
      d = this.data.startData.d[re[2]]
    }
    if (this.data.dateT == 1) {
      this.setData({
        startData: this.data.startData,
        startDateText: this.data.startData.y[re[0]] + "-" + m + "-" + d,
        card_start_date: this.data.startData.y[re[0]] + "-" + m + "-" + d
      })
    } else {
      this.setData({
        startData: this.data.startData,
        startDateText1: this.data.startData.y[re[0]] + "-" + m + "-" + d,
        yinye_start_date: this.data.startData.y[re[0]] + "-" + m + "-" + d
      })
    }
  },

  /**
   * 设置选中后的结束日期
   */
  bindChangeEnd(ev) {
    var re = ev.detail.value;
    var day = this.comDay(this.data.startData.y[re[0]], this.data.startData.m[re[1]]);
    this.data.dateI = true;

    this.data.startData.d = [];
    for (var i = 1; i <= day; i++) {
      this.data.startData.d.push(i);
    }

    var m = 0;
    var d = 0;
    if (this.data.endData.m[re[1]] < 10) {
      m = "0" + this.data.endData.m[re[1]]
    } else {
      m = this.data.endData.m[re[1]]
    }
    if (this.data.endData.d[re[2]] < 10) {
      d = "0" + this.data.endData.d[re[2]]
    } else {
      d = this.data.endData.d[re[2]];
    }

    if (this.data.dateT == 1) {
      this.setData({
        endData: this.data.endData,
        endDateText: this.data.endData.y[re[0]] + "-" + m + "-" + d,
        endDateText2: this.data.endData.y[re[0]] + "-" + m + "-" + d,
        card_end_date: this.data.endData.y[re[0]] + "-" + m + "-" + d,
      })
    } else {
      this.setData({
        endData: this.data.endData,
        endDateText1: this.data.endData.y[re[0]] + "-" + m + "-" + d,
        endDateText3: this.data.endData.y[re[0]] + "-" + m + "-" + d,
        yinye_end_date: this.data.endData.y[re[0]] + "-" + m + "-" + d,
      })
    }
  },

  bindChangeAdd1(ev) {
    var arr = [];
    for (var i = 0; i < this.data.allArr.length; i++) {
      if (this.data.addArr1[ev.detail.value[0]].areaId == this.data.allArr[i].fatherId) {
        arr.push(this.data.allArr[i])
      }
    };
    this.setData({
      addArr2: arr,
      negHeight: 40,
    });
    this.setData({
      addText1: this.data.addArr1[ev.detail.value[0]].area,
      accountProvince: this.data.addArr1[ev.detail.value[0]].areaId,
      addText2: this.data.addArr2[ev.detail.value[1]].area,
      accountCity: this.data.addArr2[ev.detail.value[1]].areaId,
      valueP: ev.detail.value
    })
  },

  getAreaData() {
    wx.request({
      url: config.requestUrl,
      data: {
        bNo: "GetArealist",
        lx: 2,
      },
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        var arr = [];
        for (var i = 0; i < data.length; i++) {
          if (this.data.addArr1[0].areaId == data[i].fatherId) {
            arr.push(data[i]);
          }
        }
        this.setData({
          allArr: data,
          addArr2: arr
        });
      }
    })
  },

  bindChangeAdd2(ev) {
    this.setData({
      addText2: this.data.addArr2[ev.detail.value].area,
      accountCity: this.data.addArr2[ev.detail.value].areaId,
    })
  },

  /**
   * 展开开始日期的选择
   */
  chooseStartDate(ev) {
    this.data.dateT = ev.currentTarget.dataset.t;
    this.data.dateI = ev.currentTarget.dataset.i;
    this.data.dataS = ev.currentTarget.dataset.s;
    console.log(this.data.dataS);
    console.log(this.data.dateI);
    console.log(this.data.dateT);

    var m = 0;
    var d = 0;
    if (this.data.startData.m[0] < 10) {
      m = "0" + this.data.startData.m[0];
    } else {
      m = this.data.startData.m[0];
    }
    if (this.data.startData.d[0] < 10) {
      d = "0" + this.data.startData.d[0];
    } else {
      d = this.data.startData.d[0];
    }
    if (this.data.dateT == 1 && this.data.dataS == 2) {
      this.setData({
        card_start_date: `${this.data.startData.y[0]}-${m}-${d}`,
        startDateText: `${this.data.startData.y[0]}-${m}-${d}`,
      })
    } else {
      this.setData({
        startDateText1: `${this.data.startData.y[0]}-${m}-${d}`,
        yinye_start_date: `${this.data.startData.y[0]}-${m}-${d}`,
      })
    }
    
    this.setData({
      valueS: [[0],[0],[0]],
      chooseStart: true,
      chooseEnd: false,
      chooseAdd1: false,
      chooseAdd2: false,
      showCover: true,
    })
  },

  /**
   * 展开结束日期的选择
   */
  chooseEndDate(ev) {
    this.data.dateT = ev.currentTarget.dataset.t;
    this.data.dateI = ev.currentTarget.dataset.i;
    this.data.dataE = ev.currentTarget.dataset.e;
    var m = 0;
    var d = 0;
    if (this.data.endData.m[0] < 10) {
      m = "0" + this.data.endData.m[0];
    } else {
      m = this.data.endData.m[0];
    }
    if (this.data.endData.d[0] < 10) {
      d = "0" + this.data.endData.d[0];
    } else {
      d = this.data.endData.d[0];
    }
    if (this.data.dateT == 2 && this.data.dataE == 1) {
      this.setData({
        endDateText1: this.data.endData.y[0] + "-" + m + "-" + d,
        endDateText3: this.data.endData.y[0] + "-" + m + "-" + d,
        card_end_date: this.data.endData.y[0] + "-" + m + "-" + d,
      })
    } else {
      this.setData({
        endDateText: this.data.endData.y[0] + "-" + m + "-" + d,
        endDateText2: this.data.endData.y[0] + "-" + m + "-" + d,
        card_end_date: this.data.endData.y[0] + "-" + m + "-" + d,
      })
    }
    this.setData({
      valueE: [[0], [0], [0]],
      chooseEnd: true,
      chooseStart: false,
      chooseAdd1: false,
      chooseAdd2: false,
      showCover: true
    })
  },

  chooseAdd1() {
    this.setData({
      valueP: [[0], [0]],
      chooseEnd: false,
      chooseStart: false,
      chooseAdd1: true,
      chooseAdd2: false,
      showCover: true
    })
  },

  chooseAdd2() {
    this.setData({
      chooseEnd: false,
      chooseStart: false,
      chooseAdd1: false,
      chooseAdd2: true,
      showCover: true
    })
  },

  /**
   * 隐藏选择
   */
  hideChoose() {
    this.setDateInHidden();
    this.setData({
      chooseEnd: false,
      chooseStart: false,
      chooseAdd1: false,
      chooseAdd2: false,
      showCover: false
    })
  },

  setDateInHidden() {
    if (typeof this.data.dateI != "boolean") {
      var date = "";
      if (this.data.dateI % 2 != 0) {
        var m = this.data.startData.m[0] < 10 ? "0" + this.data.startData.m[0] : this.data.startData.m[0];
        var d = this.data.startData.d[0] < 10 ? "0" + this.data.startData.d[0] : this.data.startData.d[0];
        date = this.data.startData.y[0] + "-" + m + "-" + d;

        switch (this.data.dateI) {
          case "1":
            this.setData({
              startDateText1: date,
            })
            break;
          case "3":
            this.setData({
              startDateText: date,
            })
            break;
        }
      } else {
        var m = this.data.startData.m[0] < 10 ? "0" + this.data.startData.m[0] : this.data.startData.m[0];
        var d = this.data.startData.d[0] < 10 ? "0" + this.data.startData.d[0] : this.data.startData.d[0];
        date = this.data.endData.y[0] + "-" + d + "-" + d;

        switch (this.data.dateI) {
          case "2":
            this.setData({
              endDateText1: date,
            })
            break;
          case "4":
            this.setData({
              endDateText: date,
            })
            break;
        }
      }
    }
  },

  comDay(year, month) {
    var d = 0;
    if (/^(1|3|5|7|8|10|12)$/.test(month)) {
      d = 31;
    } else if (/^(4|6|9|11)$/.test(month)) {
      d = 30;
    } else if (/^2$/.test(month)) {
      if (year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
        d = 29;
      } else {
        d = 28;
      }
    } else {
      throw new Error('month is illegal');
    }
    return d;
  },

  onShow() {
    wx.request({
      url: config.requestUrl,
      data: {
        bNo: "GetArealist",
        lx: "1",
      },
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        this.setData({
          addArr1: data,
        });
        this.getAreaData();
      }
    })

    wx.getStorage({
      key: 'perfect_info',
      success: res => {
        this.data.localInfo = res.data;
      },
    })

  },

  changeTPay(ev) {
    if (ev.currentTarget.dataset.i == "2") {
      this.setData({
        t_type: 7,
      })
    } else {
      this.setData({
        t_type: 1,
      })
    }
  },

  /**
   * 切换结算账户
   */
  changePay(ev) {
    if (ev.currentTarget.dataset.i == "2") {
      this.setData({
        accountType: 2
      })
    } else {
      this.setData({
        accountType: 1
      })
    }
    this.setData({
      accountProvince: "", // 开户城市
      accountCity: "", // 开户城市
      accountName: "", // 个人开户名称
      bankBranch: "", // 个人开户银行
      accountNO: "", // 个人银行账号
      cardId: "", // 结算人身份证号
      bankPhone: "", //结算人银行预留手机号码
      cardUnionNo: "", // 结算卡银行联行号
    })
  },

  /**
   * 切换企业类型
   */
  changeShop(ev) {
    console.log(ev.currentTarget.dataset.i)
    if (ev.currentTarget.dataset.i == 2) {
      this.setData({
        c_shop_lx: 2,
        accountType: 1
      })
    } else {
      this.setData({
        c_shop_lx: 1,
        accountType: 2
      })
    }
    this.setData({
      c_store_qc: "", // 企业全称（企业）
      yinye_code: "", // 工商执照注册号（企业）
      c_fr_name: "", // 法定代表人/企业负责人姓名（企业）
      card_code: "", // 身份证号（企业）
      card_start_date: "", // 证件有效期（企业）
      card_end_date: "", // 证件有效期（企业）
      yinye_rang: "", // 经营范围（企业）
      yinye_start_date: "",
      yinye_end_date: "",
      Ford1: true,
      Ford2: true,
    })
  },

  /**
   * 检测企业名称
   */
  checkLength(ev) {
    var msg = "";
    if (this.data.c_shop_lx == 1) {
      msg = "企业全称不能为空"
    } else {
      msg = "个体工商户名称不能为空"
    };
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false
      })
    }
  },

  /**
   * 检测企业法人信息
   */
  checkQyFaren(ev) {
    var msg = "";
    if (this.data.c_shop_lx == 1) {
      msg = "法定代表人/企业负责人姓名不能为空"
    } else {
      msg = "经营者姓名不能为空"
    };
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false
      })
    }
  },

  /**
   * 检测企业法人身份证
   */
  checkQySfz(ev) {
    var msg = "";
    if (this.data.c_shop_lx == 1) {
      msg = "法定代表人身份证号/企业负责人身份证号长度错误"
    } else {
      msg = "经营者身份证号长度错误"
    };
    if (ev.detail.value.length != 18) {
      wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false
      })
    }
  },

  /**
   * 检测银行开户人
   */
  checkYhKhr(ev) {
    var msg = "";
    if (this.data.c_shop_lx == 1) {
      msg = "个人开户名称不能为空"
    } else {
      msg = "企业开户名称不能为空"
    };
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false
      })
    }
  },

  /**
   * 检测开户行名称是否正常
   */
  checkYhkhh(ev) {
    var msg = "";
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: "开户银行名称不能为空",
        showCancel: false
      })
    }
  },

  /**
   * 验证结算人身份证号是否正确
   */
  checkYhSfz(ev) {
    if (ev.detail.value.length < 16) {
      wx.showModal({
        title: '提示',
        content: '结算人身份证号不正确',
        showCancel: false
      })
    }
  },

  /**
   * 检测结算人的手机号是否正确
   */
  checkYzSjh(ev) {
    if (!/^1\d{10}/.test(ev.detail.value)) {
      wx.showModal({
        title: '提示',
        content: '结算人手机号号不正确',
        showCancel: false
      })
    }
  },

  /**
   * 检测联系人姓名是否为空
   */
  checkLxrName(ev) {
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: '联系人姓名不能为空',
        showCancel: false,
      })
    }
  },

  /**
   * 联系人手机号不正确
   */
  checkLxrTel(ev) {
    if (!/^1\d{10}/.test(ev.detail.value)) {
      wx.showModal({
        title: '提示',
        content: '联系人手机号不正确',
        showCancel: false
      })
    }
  },

  /**
   * 检测联系人电子邮箱是否正确
   */
  checkLxrEmail(ev) {
    if (!/.+\@.+\..+/.test(ev.detail.value)) {
      wx.showModal({
        title: '提示',
        content: '联系人电子邮箱不正确',
        showCancel: false,
      })
    }
  },

  /**
   * 检测开户银行账号是否正常
   */
  checkYhNum(ev) {
    if (ev.detail.value.length < 16) {
      wx.showModal({
        title: '提示',
        content: '开户银行账号不正确',
        showCancel: false,
      })
    }
  },

  /**
   * 检测工商执照注册号
   */
  checkQyCodeLength(ev) {
    if (ev.detail.value.length < 13) {
      wx.showModal({
        title: '提示',
        content: '工商执照注册号长度错误',
        showCancel: false,
      })
    }
  },

  /**
   * 检测经营范围的值是否合法
   */
  checkQyJyfw(ev) {
    if (!ev.detail.value) {
      wx.showModal({
        title: '提示',
        content: '经营范围不能为空',
        showCancel: false,
      })
    }
  },

  nextStep() {
    
    
    if (this.data.c_shop_lx == 1) {
      if (!this.data.c_store_qc) {
        wx.showModal({
          title: '提示',
          content: "企业全称不能为空",
          showCancel: false,
        })
        return;
      }

      if (!this.data.c_fr_name) {
        wx.showModal({
          title: '提示',
          content: "法定代表人/企业负责人姓名不能为空",
          showCancel: false,
        })
        return;
      }

      if (this.data.card_code.length != 18) {
        wx.showModal({
          title: '提示',
          content: "法定代表人身份证号/企业负责人身份证号长度错误",
          showCancel: false,
        })
        return;
      }
    } else {
      if (!this.data.c_store_qc) {
        wx.showModal({
          title: '提示',
          content: "个体工商户名称不能为空",
          showCancel: false,
        })
        return;
      }

      if (!this.data.c_fr_name) {
        wx.showModal({
          title: '提示',
          content: "经营者姓名不能为空",
          showCancel: false,
        })
        return;
      }

      if (this.data.card_code.length < 16) {
        wx.showModal({
          title: '提示',
          content: "经营者身份证号长度错误",
          showCancel: false,
        })
        return;
      }
    }

    if (this.data.yinye_code.length < 13) {
      wx.showModal({
        title: '提示',
        content: '工商执照注册号错误',
        showCancel: false,
      })
      return;
    }

    if (!this.data.yinye_start_date) {
      wx.showModal({
        title: '提示',
        content: '请先选择营业生效日期',
        showCancel: false,
      })
      return;
    }

    if (!this.data.yinye_end_date) {
      wx.showModal({
        title: '提示',
        content: '请先选择营业失效日期',
        showCancel: false,
      })
      return;
    }

    if (!this.data.card_start_date) {
      wx.showModal({
        title: '提示',
        content: '请先选择法人身份证生效日期',
        showCancel: false,
      })
      return;
    }

    if (!this.data.card_end_date) {
      wx.showModal({
        title: '提示',
        content: '请先选择法人身份证失效日期',
        showCancel: false,
      })
      return;
    }

    if (!this.data.yinye_rang) {
      wx.showModal({
        title: '提示',
        content: '请填写经营范围',
        showCancel: false,
      })
      return;
    }

    if (!this.data.accountProvince) {
      wx.showModal({
        title: '提示',
        content: '请选择开户行省份',
        showCancel: false,
      })
      return;
    }

    if (!this.data.accountCity) {
      wx.showModal({
        title: '提示',
        content: '请选择开户行区县',
        showCancel: false,
      })
      return;
    }

    if (this.data.c_shop_lx == 1) {
      if (!this.data.accountName) {
        wx.showModal({
          title: '提示',
          content: '个人开户名称不能为空',
          showCancel: false,
        })
        return
      };
    } else {
      if (!this.data.accountName) {
        wx.showModal({
          title: '提示',
          content: '企业开户名称不能为空',
          showCancel: false,
        })
        return
      };
    }

    if (!this.data.bankBranch) {
      wx.showModal({
        title: '提示',
        content: '开户银行名称不能为空',
        showCancel: false,
      })
      return;
    }

    if (this.data.accountNO.length < 16) {
      wx.showModal({
        title: '提示',
        content: '开户银行账号不正确',
        showCancel: false,
      })
      return;
    }

    // if (this.data.cardId.length < 16) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '结算人身份证不正确',
    //     showCancel: false,
    //   })
    //   return;
    // }

    if (!/^1\d{10}/.test(this.data.bankPhone)) {
      wx.showModal({
        title: '提示',
        content: '结算人手机号不正确',
        showCancel: false,
      })
      return;
    }

    if (!this.data.c_lxr) {
      wx.showModal({
        title: '提示',
        content: '联系人姓名不能为空',
        showCancel: false,
      })
      return;
    }

    if (!/^1\d{10}/.test(this.data.c_lxphone)) {
      wx.showModal({
        title: '提示',
        content: '结算人手机号不正确',
        showCancel: false,
      })
      return;
    }

    if (!/.+\@.+\..+/.test(this.data.c_lxemail)) {
      wx.showModal({
        title: '提示',
        content: '联系人电子邮箱不正确',
        showCancel: false,
      })
      return;
    }

    if (!this.data.cardUnionNo) {
      this.data.cardUnionNo = "888888888888";
    }

    var data = {};
    if (!this.data.localInfo) {
      data = {
        c_shop_lx: this.data.c_shop_lx,
        card_user_type: this.data.card_user_type,
        t_type: this.data.t_type,
        c_store_qc: this.data.c_store_qc, // 企业全称（企业）
        yinye_code: this.data.yinye_code, // 工商执照注册号（企业）
        c_fr_name: this.data.c_fr_name, // 法定代表人/企业负责人姓名（企业）
        card_code: this.data.card_code, // 身份证号（企业）
        card_start_date: this.data.card_start_date, // 证件有效期（企业）
        card_end_date: this.data.card_end_date, // 证件有效期（企业）
        yinye_rang: this.data.yinye_rang, // 经营范围（企业）
        accountType: this.data.accountType,
        accountProvince: this.data.accountProvince, // 开户城市
        accountCity: this.data.accountCity, // 开户城市
        accountName: this.data.accountName, // 个人开户名称
        bankBranch: this.data.bankBranch, // 个人开户银行
        bankType: this.data.bankBranch,
        accountNO: this.data.accountNO, // 个人银行账号
        cardId: this.data.cardId, // 结算人身份证号
        bankPhone: this.data.bankPhone, //结算人银行预留手机号码
        cardUnionNo: this.data.cardUnionNo, // 结算卡银行联行号
        c_lxr: this.data.c_lxr, // 联系人姓名
        c_lxphone: this.data.c_lxphone, // 联系人手机号码
        c_lxemail: this.data.c_lxemail, // 联系人电子邮箱
        c_appid: this.data.c_appid, // AppId(小程序)
        c_appscript: this.data.c_appscript, // AppSecret(小程序)
        c_appid1: this.data.c_appid1, // AppId(公众号)
        c_appscript1: this.data.c_appscript1, // AppSecret(公众号)
        card_type: 0,
        yinye_start_date: this.data.yinye_start_date,
        yinye_end_date: this.data.yinye_end_date,
        c_yyzzurl: "",
        c_flurl1: "",
        c_flurl2: "",
        c_flurl5: "",
        c_flurl3: "",
        c_posurl1: "",
        c_posurl2: "",
        c_posurl3: "",
        c_dpurl1: "",
        c_dpurl2: "",
        c_dpurl3: "",
      }
    } else {
      data = this.data.localInfo
    }

    wx.setStorage({
      key: 'perfect_info',
      data,
      success: () => {
        wx.navigateTo({
          url: '/pages/uploadInfo/uploadInfo',
        })
      }
    })
  },

  /**
   * 设置企业名称
   * c_store_qc
   */
  qyName(ev) {
    this.setData({
      c_store_qc: ev.detail.value
    })
  },

  /**
   * 设置企业注册号
   * yinye_code
   */
  qyCode(ev) {
    var v = ev.detail.value.replace(/[^\d^\w]/g, "")
    this.setData({
      yinye_code: v
    })
  },

  /**
   * 设置企业法人
   * c_fr_name
   */
  qyFaren(ev) {
    this.setData({
      c_fr_name: ev.detail.value
    })
  },

  /**
   * 设置法人身份证
   * card_code
   */
  qySfz(ev) {
    var v = ev.detail.value.replace(/[^\d^\w]/g, "")
    this.setData({
      card_code: v
    })
  },

  /**
   * 设置经营范围
   * yinye_rang
   */
  qyJyfw(ev) {
    this.setData({
      yinye_rang: ev.detail.value
    })
  },

  /**
   * 开户行城市
   * accountProvince
   */
  yhCs(ev) {
    this.setData({
      accountProvince: ev.detail.value
    })
  },

  /**
   * 开户行区县
   * accountCity
   */
  yhQy(ev) {
    this.setData({
      accountCity: ev.detail.value
    })
  },

  /**
   * 开户人名称
   * accountName
   */
  yhKhr(ev) {
    this.setData({
      accountName: ev.detail.value
    })
  },

  /**
   * 开户行名称
   * bankBranch
   */
  yhKhh(ev) {
    this.setData({
      bankBranch: ev.detail.value
    })
  },

  /**
   * 开户行行号
   * accountNO
   */
  yhNum(ev) {
    this.setData({
      accountNO: ev.detail.value
    })
  },

  /**
   * 结算人身份证
   * cardId
   */
  // yhSfz(ev) {
  //   var v = ev.detail.value.replace(/[^\d^\w]/g, "")
  //   this.setData({
  //     cardId: v
  //   })
  // },

  /**
   * 预留手机号
   * bankPhone
   */
  yzSjh(ev) {
    this.setData({
      bankPhone: ev.detail.value
    })
  },

  /**
   * 结算银行卡卡号
   * cardUnionNo
   */
  yhJs(ev) {
    this.setData({
      cardUnionNo: ev.detail.value
    })
  },

  /**
   * 联系人姓名
   * c_lxr
   */
  lxrName(ev) {
    this.setData({
      c_lxr: ev.detail.value
    })
  },

  /**
   * 联系人电话
   * c_lxphone
   */
  lxrTel(ev) {
    this.setData({
      c_lxphone: ev.detail.value
    })
  },

  /**
   * 联系人邮箱
   * c_lxemail
   */
  lxrEmail(ev) {
    this.setData({
      c_lxemail: ev.detail.value
    })
  },

  /**
   * 小程序appid
   * c_appid
   */
  xcxAppid(ev) {
    this.setData({
      c_appid: ev.detail.value
    })
  },

  /**
   * 小程序secret
   * c_appscript
   */
  xcxAppsecret(ev) {
    this.setData({
      c_appscript: ev.detail.value
    })
  },

  /**
   * 公众号appid
   * c_appid1
   */
  gzhAppid(ev) {
    this.setData({
      c_appid1: ev.detail.value
    })
  },

  /**
   * 公众号secret
   * c_appscript1
   */
  gzhAppsecret(ev) {
    this.setData({
      c_appscript1: ev.detail.value
    })
  }
})