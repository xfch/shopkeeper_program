var config = require("../../config.js");
var util = require("../../utils/MD5.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    mebgradedt: [],
    edit: false,
    editArr: [],
    shopNo: "",
    opData: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.setData({
          opData: res.data,
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  edit() {
    this.setData({
      edit: true
    })
  },

  cancel() {
    this.setData({
      edit: false
    })
  },

  del() {
    var strArr = [];
    for (var i = 0; i < this.data.mebgradedt.length; i++) {
      if (this.data.mebgradedt[i].checked) {
        strArr.push(this.data.mebgradedt[i].N_MG_ID)
      }
    }

    if (strArr.length == 0) {
      this.setData({
        edit: false
      });
      return
    }

    wx.showLoading({
      title: '',
      mask: true,
    })
    var data = {
      bNo: 1210,
      mgids: strArr.join(","),
      ShopNo: this.data.shopNo,
      opname: this.data.opData.c_name,
      opid: this.data.opData.n_ss_id,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        wx.hideLoading()
        if (res.data.code === "100") {
          wx.showModal({
            title: '提示',
            content: '删除成功',
            showCancel: false,
          })
          this.getMemberClassify();
        } else {
          wx.showModal({
            title: '提示',
            content: res.data.msg,
            showCancel: false,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.shopNo = res.data;
        this.getMemberClassify();
      },
    })
  },

  getMemberClassify() {
    var str = `ShopNo=${this.data.shopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 1205,
      ShopNo: this.data.shopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var rd = JSON.parse(res.data.replace(/\(|\)/g, ""));
        if (rd.code === "100") {
          for (var i = 0; i < rd.mebgradedt.length; i++) {
            rd.mebgradedt[i].checked = false;
          }
          this.setData({
            mebgradedt: rd.mebgradedt
          })
        }
      }
    })
  },

  chooseClassify(ev) {
    if (this.data.edit) {
      this.data.editArr = [];
      this.data.mebgradedt[ev.currentTarget.dataset.i].checked = !this.data.mebgradedt[ev.currentTarget.dataset.i].checked;
      for (var i = 0; i < this.data.mebgradedt.length; i++) {
        if (this.data.mebgradedt[i].checked) {
          this.data.editArr.push(i);
        }
      }
      this.setData({
        mebgradedt: this.data.mebgradedt
      })
    } else {
      wx.navigateTo({
        url: '/pages/editMemberClassify/editMemberClassify?mgid=' + ev.currentTarget.dataset.id,
      })
    }
  },

  addClassify() {
    wx.navigateTo({
      url: '/pages/editMemberClassify/editMemberClassify',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})