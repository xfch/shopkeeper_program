// pages/retail/work-detail/work-detail.js
var config = require("../../../config.js")
var util = require("../../../utils/MD5.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    jbid: "633",
    ShopNo: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.jbid) {
      this.data.jbid = options.jbid
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getDetail();
      },
    })
  },

  getDetail() {
    var MD5 = util.md5(`ShopNo&jbid=${this.data.ShopNo}*${this.data.jbid}`).toUpperCase();
    var data = {
      bNo: 4105,
      ShopNo: this.data.ShopNo,
      jbid: this.data.jbid,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var re = JSON.parse(res.data.replace(/\(|\)/g, ""));
        wx.setNavigationBarTitle({
          title: re.model.C_SUCNAME + "的交班记录",
        })
        re.model.D_SF_TIME = re.model.D_SF_TIME.replace("T", " ");
        console.log(re.model.D_FISTDATE.indexOf("9999"))
        console.log(re.model.D_FISTDATE)
        if (re.model.D_FISTDATE.indexOf("9999") != -1) {
          re.model.D_FISTDATE = "--"
        } else {
          re.model.D_FISTDATE = re.model.D_FISTDATE.replace("T", " ")
        }
        if (re.model.D_LASTDATE.indexOf("9999") != -1) {
          re.model.D_LASTDATE = "--"
        } else {
          re.model.D_LASTDATE = re.model.D_LASTDATE.replace("T", " ")
        }
        this.setData({
          d: re.model
        })
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
})