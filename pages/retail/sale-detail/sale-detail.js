// pages/retail/sale-detail/sale-detail.js
var date = new Date();
var config = require("../../../config.js")
var util = require("../../../utils/MD5.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ono: "D180927100010102358",
    ShopNo: "",
    totalNumber: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.ono) {
      this.data.ono = options.ono;
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getOrderDetail();
      },
    })
  },

  getOrderDetail() {
    var MD5 = util.md5(`ShopNo&ono=${this.data.ShopNo}*${this.data.ono}`).toUpperCase();
    var data = {
      bNo: 4103,
      ShopNo: this.data.ShopNo,
      ono: this.data.ono,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var re = JSON.parse(res.data.replace(/\(|\)/g, ""));
        var totalNumber = 0;
        for (var i = 0; i < re.dtGoods.length; i++) {
          if (re.dtGoods[i].C_GOODS_NAME.length > 12) {
            re.dtGoods[i].C_GOODS_NAME = re.dtGoods[i].C_GOODS_NAME.substr(0, 12) + "..."
          }
          totalNumber += re.dtGoods[i].N_NUM
        }
        for (var i = 0; i < re.dtPay.length; i++) {
          if (re.dtPay[i].D_CHARGE_DATE) {
            re.dtPay[i].D_CHARGE_DATE = re.dtPay[i].D_CHARGE_DATE.replace("T", " ")
          }
        }
        this.setData({
          order: re.Order,
          goods: re.dtGoods,
          totalNumber,
          pay: re.dtPay[0]
        })
      }
    })
  }
})