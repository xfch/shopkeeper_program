// pages/retail/sale-record/sale-record.js
var date = new Date();
var config = require("../../../config.js")
var util = require("../../../utils/MD5.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    now: "",
    startText: "",
    endText: "",
    pageindex: 1,
    DtData: [],
    PageCount: 0,
    reqStatus: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    if (m < 10) {
      m = "0" + m;
    }
    var now = `${y}-${m}-${d}`
    this.setData({
      now,
      startText: now,
      endText: now,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      pageindex: 1,
      DtData: [],
    })
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getStaffInfo()
      },
    })
  },

  getStaffInfo() {
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;
        this.getRecord();
      },
    })
  },

  setStartText(ev) {
    this.setData({
      startText: ev.detail.value
    })
  },

  setEndText(ev) {
    this.setData({
      endText: ev.detail.value
    })
  },

  filterRecord() {
    this.data.pageindex = 1;
    this.getRecord();
  },

  getRecord() {
    wx.showLoading({
      title: '加载中',
    })
    var MD5 = util.md5(`ShopNo&StarTime&EndTime&pageIndex&pageSize=${this.data.ShopNo}*${this.data.startText}*${this.data.endText}*${this.data.pageindex}*20`).toUpperCase();
    var data = {
      bNo: 4102,
      ShopNo: this.data.ShopNo,
      StarTime: this.data.startText,
      EndTime: this.data.endText,
      pageIndex: this.data.pageindex,
      opid: this.data.opInfo.n_ss_id,
      pageSize: 20,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
        this.data.PageCount = data.PageCount
        if (data.DtData.length > 0) {
          for (var i = 0; i < data.DtData.length; i++) {
            if (data.DtData[i].D_ORDER_TIME) {
              data.DtData[i].D_ORDER_TIME = data.DtData[i].D_ORDER_TIME.replace("T", " ")
            }
            this.data.DtData.push(data.DtData[i])
          }

          this.setData({
            DtData: this.data.DtData,
          })
        } else {
          this.setData({
            noData: true,
          })
        }
        wx.hideLoading()
        this.data.reqStatus = true;
      }
    })
  },

  viewDetail(ev) {
    wx.navigateTo({
      url: '/pages/retail/sale-detail/sale-detail?jbid=' + ev.currentTarget.dataset.i,
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.PageCount > this.data.pageindex) {
      if (this.data.reqStatus) {
        this.data.reqStatus = false;
        this.data.pageindex++;
        this.getRecord();
      }
    }
  },
})