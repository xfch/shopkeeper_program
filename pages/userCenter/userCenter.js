var g = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    shopInfo: {},
    err: false,
    errMessage: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      version: g.version
    })
    wx.getUserInfo({
      success: res=> {
        this.setData({
          userInfo: res.userInfo
        })
      }
    })

    wx.getStorage({
      key: 'shop_data',
      success: res => {
        this.setData({
          shopInfo: res.data
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  myAchievement() {
    wx.navigateTo({
      url: '/pages/staffManage/myAchievement/myAchievement',
    })
  },

  changeRole() {
    wx.navigateTo({
      url: '/pages/roleControllor/roleControllor',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'USER_DATA',
      success: res => {
        this.setData({
          userInfo: res.data,
        })
        console.log(res.data);
      },
      complete: msg => {
        console.log(msg);
      }
    })
  },

  logOut () {
    this.setData({
      err: true,
      errMessage: "确认退出当前账号吗"
    })
    
  },

  hideDialog() {
    this.setData({
      err: false,
    })
  },

  sureOpt () {
    wx.removeStorage({
      key: 'OP_INFO',
      success: function (res) { },
    })
    wx.redirectTo({
      url: '/pages/login/login',
    })
  },

  /**
   * 修改密码
   */
  editPassword () {
    wx.navigateTo({
      url: '/pages/editPassword/editPassword',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})