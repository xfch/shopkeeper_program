var config = require("../../config.js")
var util = require("../../utils/MD5.js")

// pages/nearMenber/nearMenber.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageindex: 1,
    pagesize: 10,
    customList: [],
    filePath: config.FilePath,
    test: true,
    C_SHOP_NO: "",
    PageCount: 0,
    noData: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  viewMemberDetail(ev) {
    var no = ev.currentTarget.dataset.d;
    wx.setStorage({
      key: 'member_data',
      data: no,
    })
    wx.navigateTo({
      url: '/pages/memberConsume/memberConsume',
    })
  },

  getMemberList() {
    var str = "pageindex&pagesize&ShopNo=" + this.data.pageindex + "*" + this.data.pagesize + "*" + this.data.C_SHOP_NO;
    var md5 = util.md5(str);
    wx.showLoading({
      title: '',
    })
    var data = {
      bNo: 1705,
      pageindex: this.data.pageindex,
      pagesize: this.data.pagesize,
      ShopNo: this.data.C_SHOP_NO,
      MD5: md5.toUpperCase(),
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          if (this.data.PageCount === 0) {
            this.data.PageCount = res.data.PageCount;
          }
          for (var i = 0; i < res.data.memberList.length; i++) {
            if (res.data.memberList[i].wdzpicurllast.indexOf("ovopark") < 0) {
              res.data.memberList[i].wdzpicurllast = this.data.filePath + res.data.memberList[i].wdzpicurllast;
            }
            if (res.data.memberList[i]) {
              res.data.memberList[i].pWidth = 0;
              res.data.memberList[i].pHeight = 0;
              res.data.memberList[i].D_OpDate = res.data.memberList[i].D_OpDate ? res.data.memberList[i].D_OpDate.replace("T", " ") : "";
              res.data.memberList[i].D_AddDate = res.data.memberList[i].D_AddDate ? res.data.memberList[i].D_AddDate.replace("T", " ") : "";
              res.data.memberList[i].D_MemberDate = res.data.memberList[i].D_MemberDate ? res.data.memberList[i].D_MemberDate.replace("T", " ") : "";
              res.data.memberList[i].D_PrevDate = res.data.memberList[i].D_PrevDate ? res.data.memberList[i].D_PrevDate.replace("T", " ") : "";
            }
            this.data.customList.push(res.data.memberList[i]);
          }
          this.setData({
            customList: this.data.customList
          })
        } else {
          this.setData({
            customList: [],
          })
        }
      },
      complete: msg => {
        wx.hideLoading();
      }
    })
  },

  resetImageSize(ev) {
    var i = ev.currentTarget.dataset.i;
    console.log(i);
    if (ev.detail.width > ev.detail.height) {
      this.data.customList[i].pHeight = ev.detail.width;
    } else {
      this.data.customList[i].pWidth = ev.detail.height;
    }

    this.setData({
      customList: this.data.customList
    })
  },

  onShow() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.C_SHOP_NO = res.data;
        this.getMemberList();
      },
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.data.pageindex = 1;
    this.setData({
      customList: [],
    })
    this.getMemberList();
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.pageindex < this.data.PageCount) {
      this.data.pageindex++
      this.getMemberList();
    } else {
      this.setData({
        noData: false,
      })
    }
  },
})