// pages/staffManage/projectList/projectList.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    staffid: 0,
    pageindex: 1,
    msg: "",
    noData: false,
    recdt: [],
    PageCount: 0,
    searchfilter: "",
    searchType: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.staffid) {
      this.data.staffid = options.staffid;
    }
  },

  setSearchStr(ev) {
    this.data.searchfilter = ev.detail.value
    if (ev.detail.value == "") {
      this.setData({
        recdt: [],
      })
      this.getProjectList();
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        wx.getStorage({
          key: 'search_text_p',
          success: res => {
            console.log(res.data)
            if (res.data) {
              this.setData({
                recdt: []
              })
              this.data.searchfilter = res.data;
              this.data.searchType = 2;
              wx.setStorage({
                key: 'search_text_p',
                data: '',
              })
            }
            this.getProjectList()
          },
          fail: () => {
            this.getProjectList()
          }
        })
      },
    })
  },

  search() {
    if (this.data.searchfilter) {
      this.setData({
        recdt: [],
      })
      this.getProjectList();
    } else {
      wx.showModal({
        title: '提示',
        content: '请输入搜索内容',
        showCancel: false
      })
    }
  },

  toProjectFilter() {
    wx.navigateTo({
      url: '/pages/staffManage/projectFilterHigh/projectFilterHigh',
    })
  },

  getProjectList() {
    wx.showLoading({
      title: '',
    })
    var MD5 = util.md5(`ShopNo&staffid&pageIndex&pageSize=${this.data.ShopNo}*${this.data.staffid}*${this.data.pageindex}*10`).toUpperCase();
    var data = {
      bNo: 7011,
      ShopNo: this.data.ShopNo,
      staffid: this.data.staffid,
      pageIndex: this.data.pageindex,
      pageSize: 10,
      searchfilter: this.data.searchfilter,
      searchType: this.data.searchType,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        wx.hideLoading()
        this.data.getStatus = true;
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.recdt.length; i++) {
            res.data.recdt[i].C_PendingDate = res.data.recdt[i].C_PendingDate.replace(/\//g, "-");
            res.data.recdt[i].D_OpDate = res.data.recdt[i].C_PendingDate.replace(/T/g, " ");
            if (res.data.recdt[i].N_IntentionType == 1) {
              res.data.recdt[i].intText = "非常有意向"
            } else if (res.data.recdt[i].N_IntentionType == 2) {
              res.data.recdt[i].intText = "一般"
            } else {
              res.data.recdt[i].intText = "有一点意向"
            }

            this.data.recdt.push(res.data.recdt[i])
          }

          if (this.data.PageCount == 0) {
            this.data.PageCount = res.data.PageCount;
          }

          this.setData({
            recdt: this.data.recdt
          })
          if (res.data.recdt.length < 10) {
            if (res.data.recdt.length != 0) {
              this.setData({
                noData: true,
                msg: "没有了~"
              })
            } else {
              this.setData({
                noData: true,
                msg: "暂无潜在项目..."
              })
            }
          }
        }
      }
    })
  },

  addProject() {
    wx.navigateTo({
      url: '/pages/staffManage/addLatent/addLatent',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.PageCount > this.data.pageindex && this.data.getStatus) {
      this.data.getStatus = false;
      this.data.pageindex++;
      this.getProjectList();
    }
  }
})