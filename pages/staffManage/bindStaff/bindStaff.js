// pages/staffManage/bindStaff/bindStaff.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
var date = new Date();
var y = date.getFullYear();
var m = date.getMonth() + 1;
var d = date.getDate();
if (m < 10) {
  m = "0" + m
}
if (d < 10) {
  d = "0" + d
}
var now = y + "-" + m + "-" + d;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    t1: ["男", "女"],
    indexT1: 0,
    mebphone: "",
    recphone: "",
    mebname: "",
    mebbirth: "",
    expiretime: "",
    isybgq: 0,
    start: now,
    sex: 1,
    d1: "请选择会员生日",
    d2: "请选择会员卡过期时间",
    d3: "",
    over: false,
  },

  setOverStatus() {
    this.data.over = !this.data.over;
    
    if (this.data.over) {
      this.data.d3 = this.data.d2;
      this.data.isybgq = 1;
      this.data.d2 = "9999-12-31"
    } else {
      this.data.d2 = this.data.d3;
    }

    this.setData({
      over: this.data.over,
      d2: this.data.d2
    })
  },

  setATel(ev) {
    this.data.recphone = ev.detail.value;
  },

  setMTel(ev) {
    this.data.mebphone = ev.detail.value;
  },

  setMebname(ev) {
    this.data.mebname = ev.detail.value;
  },

  setSex(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })

    this.data.sex = parseInt(ev.currentTarget.dataset.i) + 1
  },

  setD1(ev) {
    this.setData({
      d1: ev.detail.value
    })
    this.data.mebbirth = ev.detail.value;
  },

  setD2(ev) {
    this.data.over = true;
    this.data.d3 = ev.detail.vlue
    this.setData({
      d2: ev.detail.value,
    })
    this.setOverStatus();
    this.data.expiretime = ev.detail.value;
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;

        wx.getStorage({
          key: 'OP_INFO',
          success: res => {
            this.data.opInfo = res.data;
          },
        })
      },
    })
  },

  sure() {

    if (/^1\d{10}/.test(this.data.mebphone)) {
      wx.showModal({
        title: '提示',
        content: '请填写会员手机号',
        showCancel: false
      })

      return;
    }

    if (!this.data.mebname) {
      wx.showModal({
        title: '提示',
        content: '请填写会员姓名',
        showCancel: false
      })

      return;
    }

    if (!this.data.mebbirth) {
      wx.showModal({
        title: '提示',
        content: '请选择会员生日',
        showCancel: false
      })

      return;
    }

    if (!this.data.expiretime) {
      wx.showModal({
        title: '提示',
        content: '请选择会员卡过期时间',
        showCancel: false
      })

      return;
    }
  
    var MD5 = util.md5(`staffid&mebphone&mebname&sex&mebbirth&expiretime&isybgq&ShopNo=${this.data.opInfo.n_ss_id}*${this.data.mebphone}*${this.data.mebname}*${this.data.sex}*${this.data.mebbirth}*${this.data.expiretime}*${this.data.isybgq}*${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNO: 7013,
      recphone: this.data.recphone,
      staffid: this.data.opInfo.n_ss_id,
      mebphone: this.data.mebphone,
      mebname: this.data.mebname,
      sex: this.data.sex,
      mebbirth: this.data.mebbirth,
      expiretime: this.data.expiretime,
      isybgq: this.data.isybgq,
      ShopNo: this.data.ShopNo,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.Result) {
          res.data.Result = JSON.parse(res.data.Result)
        }

        if (res.data.code === "100" && res.data.Result.rs == "1") {
          wx.showToast({
            title: '绑定成功',
            success: res => {
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1
                })
              }, 1000)
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '绑定失败，请重试',
            showCancel: false,
          })
        }
      }
    })
  }
})