// pages/staffManage/memberFilterHigh/memberFilterHigh.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
var date = new Date();
var y = date.getFullYear();
var m = date.getMonth() + 1;
var d = date.getDate();
if (m < 10) {
  m = "0" + m
}

if (d < 10) {
  d = "0" + d
}

var now = y + "-" + m + "-" + d;

Page({

  // 14
  /**
   * 页面的初始数据
   */
  data: {
    t1: ["全部", "未分配", "已分配"],
    t2: ["全部", "男", "女"],
    indexT1: 0,
    indexT2: 0,
    searchText: [{ // 统计类型
      tp: 1,
      value1: '',
      value2: '',
      edit: false,
    }, { // 选择员工
      tp: 2,
      value1: '',
      value2: '',
      edit: false,
    }, { // 会员等级
      tp: 3,
      value1: '',
      value2: '',
      edit: false,
    }, { // 会员性别
      tp: 4,
      value1: '',
      value2: '',
      edit: false,
    }, { // 消费金额
      tp: 5,
      value1: '',
      value2: '',
      edit: false,
    }, { // 账户余额
      tp: 6,
      value1: '',
      value2: '',
      edit: false,
    }, { // 账户积分
      tp: 7,
      value1: '',
      value2: '',
      edit: false,
    }, { // 会员生日
      tp: 8,
      value1: "",
      value2: "",
      edit: false,
    }, { // 添加日期
      tp: 9,
      value1: "",
      value2: "",
      edit: false,
    }, { // 最后一次充值日期
      tp: 10,
      value1: "",
      value2: "",
      edit: false,
    }, { // 最后一次消费日期
      tp: 11,
      value1: "",
      value2: "",
      edit: false,
    }, { // 会员标签
      tp: 12,
      value1: '',
      value2: '',
      edit: false,
    }],
    leval: "全部等级",
    rank: [],
    staffList: [],
    levalList: [],
    staff: "全部员工",
    currentLeval: {},
    currentStaff: {},
    d1: "1990-01-01",
    d2: now,
    d3: "1990-01-01",
    d4: now,
    d5: "1990-01-01",
    d6: now,
    d7: "1990-01-01",
    d8: now,
    tagList: [],
  },

  setTType(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })
    if (ev.currentTarget.dataset.i == 0) {
      this.data.searchText[0].value1 = "";
      this.data.searchText[0].value2 = "";
      this.data.searchText[0].edit = false;
    } else {
      this.data.searchText[0].value1 = ev.currentTarget.dataset.i;
      this.data.searchText[0].value2 = "";
      this.data.searchText[0].edit = true;
    }
  },

  setStaff(ev) {
    if (this.data.indexT1 == 2) {
      this.data.currentStaff = this.data.rank[ev.detail.value];
      this.setData({
        staff: this.data.rank[ev.detail.value].c_name
      })
      this.data.searchText[0].value1 = "";
      this.data.searchText[0].value2 = this.data.rank[ev.detail.value].n_ss_id;
      this.data.searchText[0].edit = true;
    } else {
      this.setData({
        staff: "全部员工"
      })
    }
  },

  setLeval(ev) {
    this.data.currentLeval = this.data.levalList[ev.detail.value];
    this.setData({
      leval: this.data.levalList[ev.detail.value].C_MG_NAME
    })
    this.data.searchText[1].value1 = this.data.levalList[ev.detail.value].N_MG_ID;
    if (ev.detail.value != 0) {
      this.data.searchText[1].edit = true;
    } else {
      this.data.searchText[1].edit = false;
    }
    
  },

  getMemberClassify() {
    var str = `ShopNo=${this.data.ShopNo}`;
    var md5 = util.md5(str);
    var data = {
      bNo: 1205,
      ShopNo: this.data.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var rd = JSON.parse(res.data.replace(/\(|\)/g, ""));
        if (rd.code === "100") {
          var arr = [];
          rd.mebgradedt.unshift({
            N_MG_ID: 0,
            C_SHOP_: this.data.ShopNo,
            C_MG_NAME: "全部等级"
          })
          for (var i = 0; i < rd.mebgradedt.length; i++) {
            arr.push(rd.mebgradedt[i].C_MG_NAME);
            // rd.mebgradedt[i].name = rd.mebgradedt[i].C_MG_NAME
          }
          this.data.classList = arr;
          this.setData({
            levalList: rd.mebgradedt,
            classList: arr,
          })
        }
      }
    })
  },

  getRankData() {
    var str = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7012,
      ShopNo: this.data.ShopNo,
      MD5: str,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        var arr = [];
        res.data.staffdt.unshift({
          C_MG_NAME: "全部等级",
          C_SHOP_NO: this.data.ShopNo,
          N_MG_ID: 0
        })
        for (var i = 0; i < res.data.staffdt.length; i++) {
          // res.data.staffdt[i].c_headimg = config.FilePath + res.data.staffdt[i].c_headimg
          arr.push(res.data.staffdt[i].c_name)
        }
        this.setData({
          rank: res.data.staffdt,
          staffList: arr,
        })
      }
    })
  },

  setSType(ev) {
    this.setData({
      indexT2: ev.currentTarget.dataset.i
    })
    if (ev.currentTarget.dataset.i == 0) {
      this.data.searchText[2].value1 = "";
      this.data.searchText[2].value2 = "";
      this.data.searchText[2].edit = false;
    } else {
      this.data.searchText[2].value1 = ev.currentTarget.dataset.i;
      this.data.searchText[2].value2 = "";
      this.data.searchText[2].edit = true;
    }
  },

  /**
   * 设置消费总额区间，开始
   */
  setXfAmountS(ev) {
    this.data.searchText[3].value1 = ev.detail.value;
    this.data.searchText[3].edit = true;
  },

  /**
   * 设置消费总额区间，结束
   */
  setXfAmountE(ev) {
    this.data.searchText[3].value2 = ev.detail.value;
    this.data.searchText[3].edit = true;
  },

  /**
   * 设置账户余额区间，开始
   */
  setYeAmountS(ev) {
    this.data.searchText[4].value1 = ev.detail.value;
    this.data.searchText[4].edit = true;
  },

  /**
   * 设置账户余额区间，结束
   */
  setYeAmountE(ev) {
    this.data.searchText[4].value2 = ev.detail.value;
    this.data.searchText[4].edit = true;
  },

  /**
   * 设置账户积分区间，开始
   */
  setScoreS(ev) {
    this.data.searchText[5].value1 = ev.detail.value
    this.data.searchText[5].edit = true;
  },

  /**
   * 设置账户积分区间，结束
   */
  setScoreE(ev) {
    this.data.searchText[5].value2 = ev.detail.value
    this.data.searchText[5].edit = true;
  },

  showClassChoose() {
    wx.showActionSheet({
      itemList: this.data.classList,
    })
  },

  /**
   * 生日
   */
  setD1(ev) {
    this.data.searchText[6].value1 = ev.detail.value;
    this.data.searchText[6].edit = true;
  },

  setD2(ev) {
    this.data.searchText[6].value2 = ev.detail.value;
    this.data.searchText[6].edit = true;
  },

  /**
   * 添加
   */
  setD3(ev) {
    this.data.searchText[7].value1 = ev.detail.value;
    this.data.searchText[7].edit = true;
  },

  setD4(ev) {
    this.data.searchText[7].value2 = ev.detail.value;
    this.data.searchText[7].edit = true;
  },

  /**
   * 充值
   */
  setD3(ev) {
    this.data.searchText[8].value1 = ev.detail.value;
    this.data.searchText[8].edit = true;
  },

  setD4(ev) {
    this.data.searchText[8].value2 = ev.detail.value;
    this.data.searchText[8].edit = true;
  },

  /**
   * 消费
   */
  setD3(ev) {
    this.data.searchText[9].value1 = ev.detail.value;
    this.data.searchText[9].edit = true;
  },

  setD4(ev) {
    this.data.searchText[9].value2 = ev.detail.value;
    this.data.searchText[9].edit = true;
  },

  setTag(ev) {
    this.data.tagList[ev.currentTarget.dataset.i].checked = !this.data.tagList[ev.currentTarget.dataset.i].checked;
    this.setData({
      tagList: this.data.tagList
    })

    this.setTagData();
  },

  setTagData() {
    var id = [];
    for (var i = 0; i < this.data.tagList.length; i++) {
      if (this.data.tagList[i].checked) {
        id.push(this.data.tagList[i].N_Id)
      }
    }

    if (id.length > 0) {
      this.data.searchText[1].edit = true;
    } else {
      this.data.searchText[1].edit = false;
    }

    this.data.searchText[11].value1 = id.join(",")
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getMemberClassify();
        this.getRankData();
        this.getShopTags()
      },
    })
  },

  getShopTags() {
    var str = "ShopNo=" + this.data.ShopNo;
    var md5 = util.md5(str);
    var data = {
      bNo: 1706,
      ShopNo: this.data.ShopNo,
      MD5: md5.toUpperCase()
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.membertagList.length; i++) {
            res.data.membertagList[i].checked = false;
          }
          this.setData({
            tagList: res.data.membertagList
          })
        }
      }
    })
  },

  sure() {
    var arr = [];
    for (var i = 0; i < this.data.searchText.length; i++) {
      if (this.data.searchText[i].edit) {
        var c = {};
        arr.push({
          tp: this.data.searchText[i].tp,
          value1: this.data.searchText[i].value1,
          value2: this.data.searchText[i].value2
        });
      }
    }
    console.log(arr)
    wx.setStorage({
      key: 'search_text',
      data: JSON.stringify(arr),
      success: res => {
        wx.navigateBack({
          delta: 1
        })
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },
})