// pages/staffManage/staffList/staffList.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ShopNo: "",
    currentStaff: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  getRankData() {
    var str = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7012,
      ShopNo: this.data.ShopNo,
      MD5: str,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        for (var i = 0; i < res.data.staffdt.length; i++) {
          res.data.staffdt[i].c_headimg = config.FilePath + res.data.staffdt[i].c_headimg
        }
        this.setData({
          rank: res.data.staffdt
        })
      }
    })
  },

  chooseStaff(ev) {
    this.data.disstr.staffid = this.data.rank[ev.currentTarget.dataset.i].n_ss_id;
    this.data.currentStaff = this.data.rank[ev.currentTarget.dataset.i].c_name;
    this.submit();
  },

  submit() {
    this.data.disstr.MD5 = util.md5(`mebnos&staffid&optype&ShopNo=${this.data.disstr.mebnos}*${this.data.disstr.staffid}*${this.data.disstr.optype}*${this.data.disstr.ShopNo}`).toUpperCase();
    this.data.disstr.bNo = 7004
    wx.request({
      url: config.requestUrl,
      data: this.data.disstr,
      success: res => {
        if (res.data.code === "100") {
          wx.showModal({
            title: '提示',
            content: `已成功分配给${this.data.currentStaff}`,
            showCancel: false,
            success: res => {
              wx.navigateBack({
                delta: 3,
              })
            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getRankData();
      },
    })

    wx.getStorage({
      key: 'disstr',
      success: res => {
        this.data.disstr = res.data;
      },
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
})