// pages/staffManage/bounsMain/bounsMain.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ShopNo: "",
    datas: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  getBounsInfo() {
    var str = util.md5(`ShopNo=${this.data.ShopNo}`);
    var data = {
      bNo: 7002,
      ShopNo: this.data.ShopNo,
      MD5: str.toUpperCase()
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100") {
          this.setData({
            datas: res.data.Result
          })
        }
      }
    })
  },

  toCustomerStatistics(ev) {
    wx.navigateTo({
      url: '/pages/staffManage/customerStatistics/customerStatistics?main=1&searchtype=' + ev.currentTarget.dataset.i,
    })
  },

  viewRank() {
    wx.navigateTo({
      url: '/pages/staffManage/achievementRank/achievementRank',
    })
  },

  shMember() {
    wx.navigateTo({
      url: '/pages/staffManage/memberDistribution/memberDistribution',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data
        this.getBounsInfo();
      },
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },
})