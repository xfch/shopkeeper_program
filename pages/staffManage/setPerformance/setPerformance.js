// pages/staffManage/setPerformance/setPerformance.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    t1: ["按月", "按季度", "按年"], // 统计类型,
    t2: ["扣除金额", "其他"], // 未达标处理类型
    indexT1: 0,
    indexT2: 0,
    datas: {},
    minipodatas: {
      N_Id: 1,
      N_Type: 1,
      N_PunishType: 1,
      N_MinPerformance: 2000,
      N_DeductMoney: 500,
      C_Remark: "",
      N_OpId: 1646,
    }
  },

  setT1(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })

    this.data.minipodatas.N_Type = ev.currentTarget.dataset.i;
  },

  setT2(ev) {
    this.setData({
      indexT2: ev.currentTarget.dataset.i
    })
    this.data.minipodatas.N_PunishType = ev.currentTarget.dataset.i;
  },

  setBouns(ev) {
    this.data.minipodatas.N_MinPerformance = ev.detail.value;
  },

  setAmount(ev) {
    this.data.minipodatas.N_DeductMoney = ev.detail.value;
  },

  setRemark(ev) {
    this.data.minipodatas.C_Remark = ev.detail.value;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getSetting();
        wx.getStorage({
          key: 'OP_INFO',
          success: rs => {
            this.data.opInfo = rs.data;
          },
        })
      },
    })
  },

  sure() {
    var MD5 = util.md5(`minipodatas&ShopNo=${JSON.stringify(this.data.minipodatas)}*${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7000,
      minipodatas: JSON.stringify(this.data.minipodatas),
      ShopNo: this.data.ShopNo,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100" && res.data.Result) {
          wx.showToast({
            title: '设置成功',
            success: res => {
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1,
                })
              })
            }
          })
        }
      }
    })
  },

  getSetting() {
    var MD5 = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();

    var data = {
      bNo: 7001,
      ShopNo: this.data.ShopNo,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.ProfitDt.length > 0) {
          this.data.minipodatas.N_Id = res.data.ProfitDt[0].N_Id
          this.setData({
            datas: res.data.ProfitDt[0],
            indexT1: res.data.ProfitDt[0].N_Type,
            indexT2: res.data.ProfitDt[0].N_PunishType,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
})