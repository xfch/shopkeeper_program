// pages/staffManage/reVisit/reVisit.js
var date = new Date();
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    t1: ["电话", "微信", "其他"],
    indexT1: 1,
    score: 5,
    scoreIcons: ["/images/icon-h.png", "/images/icon-h.png", "/images/icon-h.png", "/images/icon-h.png", "/images/icon-h.png"],
    startDate: "",
    endDate: "",
    mebno: "",
    name: "",
    tel: "",
    date: "请选择日期",
    time: "请选择时间",
    opInfo: {},
    goodsInfo: {},
    isManager: false,
    visitrecord: {
      C_MB_NO: "117095",
      N_StaffId: "1646",
      N_VisitWay: "2",
      C_NextVisitTime: "2018-08-20",
      C_VisitContent: "fafadlfjaskjfalfjlasflasf",
      C_NextVisitContent: "fafadlfjaskjfalfjlasflasf",
      C_Remark: "",
      N_OpId: "1646",
      N_MebStart: "4",
      C_GoodsId: "65204",
      C_GoodsName: "肥锅肉套餐（端午节特惠第二份半价，没女朋友的请自带朋友）",
    },
  },

  setVisitType() {

  },

  setContent(ev) {
    this.setData({
      content: ev.detail.value
    })
  },

  cancel() {
    wx.navigateBack({
      delta: 1,
    })
  },

  submit() {
    var N_StaffId = this.data.opInfo.n_ss_id
    if (this.data.isManager) {
      N_StaffId = 0;
    }

    if (!this.data.date) {
      wx.showModal({
        title: '提示',
        content: '请选择下次回访日期',
        showCancel: false,
      })

      return
    }

    if (!this.data.goodsInfo.ID) {
      wx.showModal({
        title: '提示',
        content: '请选择回访项目',
        showCancel: false,
      })

      return
    }

    if (!this.data.content) {
      wx.showModal({
        title: '提示',
        content: '请填写回访内容',
        showCancel: false,
      })

      return
    }

    this.data.visitrecord = {
      C_MB_NO: this.data.mebno,
      N_StaffId,
      N_VisitWay: this.data.indexT1,
      C_NextVisitTime: this.data.date + " " +this.data.time,
      C_VisitContent: this.data.content,
      C_NextVisitContent: this.data.content,
      N_OpId: this.data.n_ss_id,
      N_MebStart: this.data.score,
      C_GoodsId: this.data.goodsInfo.ID,
      C_GoodsName: this.data.goodsInfo.GOODS_NAME2
    }
    var MD5 = util.md5(`visitrecord&ShopNo=${JSON.stringify(this.data.visitrecord)}*${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7007,
      visitrecord: JSON.stringify(this.data.visitrecord),
      ShopNo: this.data.ShopNo,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100" && res.data.Result) {
          wx.showToast({
            title: '回访成功',
            success: res => {
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1
                })
              }, 1000)
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '回访失败，请重试',
            showCancel: false,
          })
        }
      }
    })
  },

  chooseGoods() {
    wx.navigateTo({
      url: '/pages/chooseGoods/chooseGoods',
    })
  },

  setScore(ev) {
    var scoreIcons = ["/images/icon-h-g.png", "/images/icon-h-g.png", "/images/icon-h-g.png", "/images/icon-h-g.png", "/images/icon-h-g.png"];
    this.data.score = ev.currentTarget.dataset.i;
    for (var i = 0; i < ev.currentTarget.dataset.i; i++) {
      scoreIcons[i] = "/images/icon-h.png"
    };

    this.setData({
      scoreIcons,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.main) {
      this.data.isManager = true;
    }

    if (options.mebno) {
      this.setData({
        mebno: options.mebno
      })
    }

    if (options.name) {
      this.setData({
        name: options.name
      })
    }

    if (options.tel) {
      this.setData({
        tel: options.tel
      })
    }
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;
      },
    })
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    var startDate = `${y}-${m}-${d}`;
    if (m + 3 > 12) {
      y++;
      m = m - 9;
    } else {
      m = m + 3;
    }
    var endDate = `${y}-${m}-${d}`;

    this.setData({
      startDate,
      endDate
    })
  },

  setDateFunc(ev) {
    this.setData({
      date: ev.detail.value
    })
  },

  setTimeFunc(ev) {
    this.setData({
      time: ev.detail.value
    })
  },

  setVisitType(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getOldVisit()
      },
    })
    wx.getStorage({
      key: 'goods_info',
      success: res => {
        res.data.GOODS_NAME2 = res.data.GOODS_NAME;
        if (res.data.GOODS_NAME.length > 20) {
          res.data.GOODS_NAME = res.data.GOODS_NAME.substr(0, 20) + "..."
        }
        this.setData({
          goodsInfo: res.data
        })
      },
    })
  },

  getOldVisit() {
    var MD5 = util.md5(`mebno&staffid&ShopNo=${this.data.mebno}*${this.data.opInfo.n_ss_id}*${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7016,
      mebno: this.data.mebno,
      staffid: this.data.opInfo.n_ss_id,
      ShopNo: this.data.ShopNo,
      MD5,
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        this.setData({
          reContent: res.data.Result[0].C_VisitContent
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.removeStorage({
      key: 'goods_info',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})