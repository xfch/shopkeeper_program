// pages/staffManage/customerStatistics/customerStatistics.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ShopNo: "",
    opInfo: {},
    pageindex: 1,
    recdt: [],
    PageCount: 0,
    staffid: 0,
    getStatus: true,
    searchtype: 1,
    searchkey: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (!options.main) {
      if (options.staffid) {
        this.data.staffid = options.staffid;
      }
    } else {
      this.data.staffid = 0;
    }

    if (options.searchtype) {
      this.data.searchtype = options.searchtype
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.setData({
          recdt: [],
        })
        wx.getStorage({
          key: 'OP_INFO',
          success: res => {
            this.data.opInfo = res.data;
            this.getCustomerInfo();
          },
        })
      },
    })
  },

  reVisitCustomer(ev) {
    if (this.data.staffid != 0) {
      wx.navigateTo({
        url: '/pages/staffManage/reVisit/reVisit?mebno=' + ev.currentTarget.dataset.d.C_MB_NO + "&tel=" + ev.currentTarget.dataset.d.C_MB_PHONE + "&name=" + ev.currentTarget.dataset.d.C_MB_NAME,
      })
    } else {
      wx.navigateTo({
        url: '/pages/staffManage/reVisit/reVisit?mebno=' + ev.currentTarget.dataset.d.C_MB_NO + "&tel=" + ev.currentTarget.dataset.d.C_MB_PHONE + "&name=" + ev.currentTarget.dataset.d.C_MB_NAME + "&main=1",
      })
    }
  },

  viewReRecord(ev) {
    wx.navigateTo({
      url: '/pages/staffManage/visitRecord/visitRecord?mebno=' + ev.currentTarget.dataset.n,
    })
  },

  getCustomerInfo() {
    wx.showLoading({
      title: '',
      mask: true,
    })
    var MD5 = util.md5(`ShopNo&pageIndex&pageSize=${this.data.ShopNo}*${this.data.pageindex}*10`).toUpperCase();
    var data = {
      bNo: 7006,
      ShopNo: this.data.ShopNo,
      pageIndex: this.data.pageindex,
      pageSize: 10,
      staffid: this.data.staffid,
      searchtype: this.data.searchtype,
      searchkey: this.data.searchkey,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        wx.hideLoading()
        this.data.getStatus = true;
        if (res.data.code === "100") {
          if (this.data.PageCount == 0) {
            this.data.PageCount = res.data.PageCount
          }
          for (var i = 0; i < res.data.recdt.length; i++) {
            this.data.recdt.push(res.data.recdt[i])
          }
          this.setData({
            recdt: this.data.recdt,
          })
        }
      }
    })
  },

  highFilter() {
    wx.navigateTo({
      url: '/pages/staffManage/memberFilterHigh/memberFilterHigh',
    })
  },

  reVisit() {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.pageindex < this.data.PageCount && this.data.getStatus) {
      this.data.getStatus = false;
      this.data.pageindex++;
      this.getCustomerInfo()
    }
  },
})