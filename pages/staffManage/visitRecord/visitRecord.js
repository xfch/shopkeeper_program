// pages/staffManage/visitRecord/visitRecord.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mebno: "",
    pageindex: 1,
    ShopNo: "",
    recdt: [],
    noData: false,
    PageCount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.data.mebno = options.mebno || "117084"
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getRecord()
      },
    })
  },

  getRecord() {
    var MD5 = util.md5(`ShopNo&pageIndex&pageSize=${this.data.ShopNo}*${this.data.pageindex}*10`).toUpperCase();
    var data = {
      bNo: 7008,
      ShopNo: this.data.ShopNo,
      pageIndex: this.data.pageindex,
      mebno: this.data.mebno,
      pagesize: 10,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        this.data.getStatus = false;
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.recdt.length; i++) {
            if (res.data.recdt[i].N_VisitWay == 1) {
              res.data.recdt[i].text = "手机"
            } else if (res.data.recdt[i].N_VisitWay == 2) {
              res.data.recdt[i].text = "微信"
            } else {
              res.data.recdt[i].text = "其他"
            }
            res.data.recdt[i].D_OpDate = res.data.recdt[i].D_OpDate.replace("T", " ")
            this.data.recdt.push(res.data.recdt[i]);
          }

          if (this.data.PageCount == 0) {
            this.data.PageCount = res.data.PageCount
          }

          this.setData({
            recdt: this.data.recdt
          })
        } else {
          this.setData({
            noData: true,
          })
        }
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.PageCount > this.data.pageindex && this.data.getStatus) {
      this.data.pageindex++;
      this.data.getStatus = false;
      this.getRecord()
    }
  },
})