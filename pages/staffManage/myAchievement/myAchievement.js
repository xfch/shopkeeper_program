// pages/staffManage/myAchievement/myAchievement.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js");
var g = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opInfo: {},
    ShopNo: "",
    datas: {},
    version: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      version: g.version
    })
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;

        wx.getStorage({
          key: 'C_SHOP_NO',
          success: res => {
            this.data.ShopNo = res.data;
            this.getStaffData()
          },
        })
      },
    })
  },

  getStaffData() {
    var MD5 = util.md5(`ShopNo&staffid=${this.data.ShopNo}*${this.data.opInfo.n_ss_id}`).toUpperCase();
    var data = {
      bNo: 7005,
      ShopNo: this.data.ShopNo,
      staffid: this.data.opInfo.n_ss_id,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        this.setData({
          datas: res.data.Result
        })
      }
    })
  },

  shMember() {
    wx.navigateTo({
      url: '/pages/staffManage/bindStaff/bindStaff',
    })
  },

  toCustomerStatistics(ev) {
    wx.navigateTo({
      url: '/pages/staffManage/customerStatistics/customerStatistics?staffid=' + this.data.opInfo.n_ss_id + "&searchtype=" + ev.currentTarget.dataset.t,
    })
  },

  viewProject() {
    wx.navigateTo({
      url: "/pages/staffManage/projectList/projectList?staffid=" + this.data.opInfo.n_ss_id
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  }
})