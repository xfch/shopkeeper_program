// pages/staffManage/projectFilterHigh/projectFilterHigh.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
var date = new Date();
var y = date.getFullYear();
var m = date.getMonth() + 1;
var d = date.getDate();
if (m < 10) {
  m = "0" + m
}

if (d < 10) {
  d = "0" + d
}

var now = y + "-" + m + "-" + d;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    t1: ["非常有意向", "一般", "有一点意向"],
    indexT1: 0,
    d1: "1990-01-01",
    d2: now,
    d3: "1990-01-01",
    d4: now,
    searchText: [{ // 会员相关
      tp: 1,
      value1: '',
      value2: '',
      edit: false,
    }, { // 商品相关
      tp: 2,
      value1: '',
      value2: '',
      edit: false,
    }, { // 录入日期
      tp: 3,
      value1: '',
      value2: '',
      edit: false,
    }, { // 施工日期
      tp: 4,
      value1: '',
      value2: '',
      edit: false,
    }]
  },

  setKeyword(ev) {
    this.data.searchText[0].value1 = ev.detail.value;
    if (ev.detail.value) {
      this.data.searchText[0].edit = true;
    } else {
      this.data.searchText[0].edit = false;
    }
  },

  setD1(ev) {
    this.setData({
      d1: ev.detail.value
    })
    this.data.searchText[1].value1 = ev.detail.value;

    this.data.searchText[1].edit = true;
  },

  setD2(ev) {
    this.setData({
      d2: ev.detail.value
    })

    this.data.searchText[1].value2 = ev.detail.value;
    this.data.searchText[1].edit = true;
  },

  setD3(ev) {
    this.setData({
      d3: ev.detail.value
    })

    this.data.searchText[2].value1 = ev.detail.value;
    this.data.searchText[2].edit = true;
  },

  setD4(ev) {
    this.setData({
      d4: ev.detail.value
    })

    this.data.searchText[2].value2 = ev.detail.value;
    this.data.searchText[2].edit = true;
  },

  setHeart(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })

    this.data.searchText[3].value1 = parseInt(ev.currentTarget.dataset.i) + 1;
    this.data.searchText[3].edit = true;
  },

  sure() {
    var arr = [];
    for (var i = 0; i < this.data.searchText.length; i++) {
      if (this.data.searchText[i].edit) {
        var c = {};
        arr.push({
          tp: this.data.searchText[i].tp,
          value1: this.data.searchText[i].value1,
          value2: this.data.searchText[i].value2
        });
      }
    }

    wx.setStorage({
      key: 'search_text_p',
      data: JSON.stringify(arr),
      success: res => {
        wx.navigateBack({
          delta: 1
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
})