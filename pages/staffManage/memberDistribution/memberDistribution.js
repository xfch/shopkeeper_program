// pages/staffManage/memberDistribution/memberDistribution.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    l: [],
    checkAll: false,
    ShopNo: "",
    pageindex: 1,
    recdt: [],
    PageCount: 0,
    PageRecord: 0,
    mebnos: "",
    opInfo: {},
    getStatus: true,
    totalNum: 0,
    searchfilter: "",
    searchtype: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.opInfo = res.data;
      },
    })
  },

  highFilter() {
    wx.navigateTo({
      url: '/pages/staffManage/memberFilterHigh/memberFilterHigh',
    })
  },

  showOption() {
    var num = 0;
    for (var i = 0; i < this.data.l.length; i++) {
      if (this.data.l[i].checked) {
        num++;
      }
    }

    if (num > 0) {
      wx.showActionSheet({
        itemList: ["全部取消分配", "未分配会员分配给指定员工", "全部分配给指定员工"],
        success: res => {
          this.subDistriData(res.tapIndex + 1);
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '请先选择员工',
        showCancel: false,
      })
    }
  },

  subDistriData(t) {
    var data = {
      mebnos: this.data.mebnos,
      staffid: "",
      optype: t,
      opid: this.data.opInfo.n_ss_id,
      opname: this.data.opInfo.c_name,
      ShopNo: this.data.ShopNo,
    }

    wx.setStorage({
      key: 'disstr',
      data,
      success: res => {
        wx.navigateTo({
          url: '/pages/staffManage/staffList/staffList',
        })
      }
    })
  },

  chooseShowOption(ev) {
    if (this.data.l[ev.currentTarget.dataset.i].checked) {
      this.showOption();
    } else {
      this.checkOne(ev.currentTarget.dataset.i, () => {
        this.showOption();
      });
    }
  },

  chooseMember(ev) {
    this.checkOne(ev.currentTarget.dataset.i);
  },

  checkOne(i, call) {
    this.data.l[i].checked = !this.data.l[i].checked;
    var checkAll = true;
    for (var i = 0; i < this.data.l.length; i++) {
      if (!this.data.l[i].checked) {
        checkAll = false;
        break;
      }
    }
    if (call) {
      call();
    }
    this.setData({
      l: this.data.l,
      checkAll,
    })
    this.setTotalNum();
    this.getMemberNo();
  },

  checkAll() {
    this.setData({
      checkAll: !this.data.checkAll
    })
    this.setCheckAll();
    this.getMemberNo();
    this.setTotalNum();
  },

  setTotalNum() {
    var num = 0;
    for (var i = 0; i < this.data.l.length; i++) {
      if (this.data.l[i].checked) {
        num++
      }
    }

    this.setData({
      totalNum: num
    })
  },

  setCheckAll() {
    if (!this.data.checkAll) {
      for (var i = 0; i < this.data.l.length; i++) {
        this.data.l[i].checked = false;
      }
    } else {
      for (var i = 0; i < this.data.l.length; i++) {
        this.data.l[i].checked = true;
      }
    }

    this.setData({
      l: this.data.l
    })
  },

  getMemberNo() {
    this.data.mebnos = "";
    var no = [];
    for (var i = 0; i < this.data.l.length; i++) {
      if (this.data.l[i].checked) {
        no.push(this.data.l[i].C_MB_NO);
      }
    }

    if (no.length != 0) {
      this.data.mebnos = no.join(",");
      // wx.showModal({
      //   title: '提示',
      //   content: '请先选择需要分配的会员',
      //   showCancel: false,
      // })
    }
  },
  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;

        wx.getStorage({
          key: 'search_text',
          success: res => {
            if (res.data) {
              this.setData({
                l: []
              })
              this.data.searchfilter = res.data;
              this.data.searchtype = 2;
              wx.setStorage({
                key: 'search_text',
                data: '',
              })
            }
            this.getMemberList();
          },
          fail: () => {
            this.getMemberList();
          }
        })
      },
    })
  },

  getMemberList() {
    wx.showLoading({
      title: '',
    })
    var MD5 = util.md5(`ShopNo&pageIndex&pageSize=${this.data.ShopNo}*${this.data.pageindex}*10`).toUpperCase();
    var data = {
      bNo: 7003,
      ShopNo: this.data.ShopNo,
      pageIndex: this.data.pageindex,
      pageSize: 10,
      searchtype: this.data.searchtype,
      searchfilter: this.data.searchfilter,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        wx.hideLoading();
        this.data.getStatus = true;
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.recdt.length; i++) {
            res.data.recdt[i].checked = false;
            this.data.l.push(res.data.recdt[i])
          }
          var PageRecord = 0;
          var PageCount = 0;
          if (this.data.PageRecord == 0) {
            PageRecord = res.data.PageRecord
          } else {
            PageRecord = this.data.PageRecord;
          }

          if (this.data.PageCount == 0) {
            PageCount = res.data.PageCount
          } else {
            PageCount = this.data.PageCount;
          }

          this.setData({
            l: this.data.l,
            PageCount: this.data.PageCount,
            PageRecord,
            PageCount,
          })
        }
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.pageindex <  this.data.PageCount && this.data.getStatus) {
      this.data.getStatus = false;
      this.data.pageindex++;
      this.getMemberList();
    }
  },
})