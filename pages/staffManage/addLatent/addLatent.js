// pages/staffManage/addLatent/addLatent.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    t1: ["非常有意向", "一般", "有一点意向"],
    indexT1: 0,
    searchStr: "",
    showSearch: false,
    pageindex: 1,
    ShopNo: "",
    opInfo: {},
    Mebdt: [],
    tips: "",
    currentMember: {},
    goodsInfo: {},
    amount: "",
    days: 0,
    potentialdata: {
      C_GoodsName: "",
      C_Remark: "",
      C_Shop_No: "100010",// 店铺编号
      C_MB_NO: "117079",// 会员编号
      N_StaffId: "1646",// 员工编号
      C_GoodsId: "65204",// 项目
      N_Quote: 300,// 报价
      N_IntentionType: "1",// 意向类型 1非常有意向 2一般 3有点意向
      N_Days: 3// 预计几天后消费
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'OP_INFO',
      success: res => {
        this.data.potentialdata.N_StaffId = res.data.n_ss_id;
        this.data.opInfo = res.data;
      },
    })
  },

  setRemark(ev) {
    this.data.potentialdata.C_Remark = ev.detail.value;
  },

  setAmount(ev) {
    this.setData({
      amount: ev.detail.value,
    })

    this.data.potentialdata.N_Quote = ev.detail.value;
  },

  setDays(ev) {
    this.setData({
      days: ev.detail.value,
    })

    this.data.potentialdata.N_Days = ev.detail.value;
  },

  setHeart(ev) {
    this.setData({
      indexT1: ev.currentTarget.dataset.i
    })

    this.data.potentialdata.N_IntentionType = parseInt(ev.currentTarget.dataset.i) + 1;
  },

  setSearchStr(ev) {
    this.data.searchStr = ev.detail.value;
  },

  chooseMember() {
    if (!this.data.currentMember.C_MB_NO) {
      wx.showModal({
        title: '提示',
        content: '请选择会员',
        showCancel: false
      })
    }
  },

  getCustomerInfo() {
    var MD5 = util.md5(`keywords&staffid&ShopNo=${this.data.searchStr}*${this.data.opInfo.n_ss_id}*${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7015,
      ShopNo: this.data.ShopNo,
      keywords: this.data.searchStr,
      staffid: this.data.opInfo.n_ss_id,
      MD5,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        this.data.getStatus = true;
        this.data.Mebdt = [];
        if (res.data.code === "100") {
          for (var i = 0; i < res.data.Mebdt.length; i++) {
            if (res.data.Mebdt[i].C_MB_PIC) {
              res.data.Mebdt[i].C_MB_PIC = config.FilePath + res.data.Mebdt[i].C_MB_PIC;
            } else {
              res.data.Mebdt[i].C_MB_PIC = "/images/d-face.jpg"
            }

            this.setData({
              Mebdt: res.data.Mebdt,
              tips: "没有更多了"
            })
          }
        }

        if (this.data.Mebdt.length == 0) {
          this.setData({
            tips: "没有找到会员"
          })
        }

        this.showSearchList();
      }
    })
  },

  setMember(ev) {
    this.setData({
      currentMember: ev.currentTarget.dataset.d
    })
    this.data.potentialdata.C_MB_NO = ev.currentTarget.dataset.d.C_MB_NO;
    this.hideSearchList();
  },

  showSearchList() {
    this.setData({
      showSearch: true,
    })
  },

  hideSearchList() {
    this.setData({
      showSearch: false,
      searchStr: "",
    })
  },

  sure() {

    if (!this.data.currentMember.C_MB_NO) {
      wx.showModal({
        title: '提示',
        content: '请选择会员',
        showCancel: false,
      })

      return;
    }
    
    if (!this.data.amount) {
      wx.showModal({
        title: '提示',
        content: '请填写报价',
        showCancel: false,
      })

      return;
    }

    if (!this.data.days) {
      wx.showModal({
        title: '提示',
        content: '请填写预期消费日期',
        showCancel: false,
      })

      return;
    }

    var MD5 = util.md5(`potentialdata&ShopNo=${JSON.stringify(this.data.potentialdata)}*${this.data.ShopNo}`).toUpperCase();

    var data = {
      bNo: 7009,
      potentialdata: JSON.stringify(this.data.potentialdata),
      ShopNo: this.data.ShopNo,
      MD5,
    };

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        if (res.data.code === "100" && res.data.Result) {
          wx.showToast({
            title: '添加成功',
            success: res => {
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1
                })
              }, 1000)
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '添加失败，请重试',
            showCancel: false,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  chooseGoods() {
    wx.navigateTo({
      url: '/pages/chooseGoods/chooseGoods',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.getStorage({
      key: 'goods_info',
      success: res => {
        // this.data.goodsInfo = res.data;
        this.data.potentialdata.C_GoodsId = res.data.ID;
        this.data.potentialdata.C_GoodsName  = res.data.GOODS_NAME;
        if (res.data.GOODS_NAME.length > 20) {
          res.data.GOODS_NAME = res.data.GOODS_NAME.substr(0, 20) + "..."
        }
        this.setData({
          goodsInfo: res.data,
        })
      },
    })

    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.potentialdata.C_Shop_No = res.data;
        this.data.ShopNo = res.data;
      },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})