// pages/staffManage/achievementRank/achievementRank.js
var util = require('../../../utils/MD5.js');
var config = require("../../../config.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ShopNo: "",
    rank: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'C_SHOP_NO',
      success: res => {
        this.data.ShopNo = res.data;
        this.getRankData();
      },
    })
  },

  getRankData() {
    var str = util.md5(`ShopNo=${this.data.ShopNo}`).toUpperCase();
    var data = {
      bNo: 7014,
      ShopNo: this.data.ShopNo,
      MD5: str,
    }

    wx.request({
      url: config.requestUrl,
      data,
      success: res => {
        this.setData({
          rank: res.data.Result
        })
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
})