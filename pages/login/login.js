var config = require("../../config.js")
var util = require("../../utils/MD5.js")
var app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        shopNo: "",
        tel: "",
        pwd: "",
        checkData: {
            p2: false,
            p3: false,
        },
        pass: "",
        err: false,
        errMessage: "密码错误",
        loginStatus: true,
        noLogin: true,
        n_yjhid: 2626,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        const shopno = wx.getStorageSync('shopno')
        const shopnoMain = wx.getStorageSync('shopnoMain')
        app.shopno = shopno //当前选择店铺编号
        app.shopnoMain = shopnoMain //总店编号
        wx.getStorage({
            key: 'OP_INFO',
            success: res => {
                if (res.data.n_isservice == 1) {
                    wx.getStorage({
                        key: 'shop_data',
                        success: rs => {
                            this.data.n_yjhid = rs.data.n_yjhid
                            this.routerLink()
                        },
                    })
                } else {
                    wx.reLaunch({
                        url: '/pages/index/index',
                    })
                }
            },
            fail: () => {
                this.setData({
                    noLogin: true,
                })
            }
        })
    },

    registFunc() {
        wx.navigateTo({
            url: '/pages/register/register',
        })
    },

    fpwd() {
        wx.navigateTo({
            url: '/pages/findPassword/findPassword',
        })
    },

    /**
     * 设置店铺号
     */
    setShopNo(ev) {
        if (ev.detail.value.length > 0) {
            this.data.checkData.p1 = true;
        } else {
            this.data.checkData.p1 = false;
        }
        this.setData({
            shopNo: ev.detail.value
        })
        this.checkData()
    },

    /**
     * 设置登录账号
     */
    setUsername(ev) {
        if (ev.detail.value.length > 0) {
            this.data.checkData.p2 = true;
        } else {
            this.data.checkData.p2 = false;
        }
        this.setData({
            tel: ev.detail.value
        })
        this.checkData()
    },

    /**
     * 设置登录密码
     */
    setPassword(ev) {
        if (ev.detail.value.length > 0) {
            this.data.checkData.p3 = true;
        } else {
            this.data.checkData.p3 = false;
        }
        this.setData({
            pwd: ev.detail.value
        })
        this.checkData()
    },

    checkData() {
        var flag = true;
        for (var i in this.data.checkData) {
            if (!this.data.checkData[i]) {
                flag = false;
                break;
            }
        }

        if (flag) {
            this.setData({
                pass: "active",
            })
        } else {
            this.setData({
                pass: "",
            })
        }
    },

    hideDialog() {
        this.setData({
            err: false
        });
    },

    LoginIn() {
        var str = "ShopNo&LogName&LogPwd=" + this.data.shopNo + "*" + this.data.tel + "*" + this.data.pwd,
            md5 = util.md5(str),
            _this = this,
            data = {
                bNo: 1701,
                ShopNo: this.data.shopNo,
                LogName: this.data.tel,
                LogPwd: this.data.pwd,
                MD5: md5.toUpperCase()
            };
        if (this.data.loginStatus) {
            this.data.loginStatus = false;
            this.setData({
                pass: "",
            })
            wx.request({
                url: config.requestUrl,
                data,
                success: res => {
                    var data = res.data.replace(/\(|\)/g, "")
                    var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
                    if (data.code === "100") {
                        if (data.Staff) {
                            wx.setStorage({
                                key: 'OP_INFO',
                                data: data.Staff,
                            })

                            wx.setStorage({
                                key: 'C_SHOP_NO',
                                data: data.Staff.c_shop_no,
                            })

                            wx.setStorage({
                                key: 'MAIN_C_SHOP_NO',
                                data: data.Staff.c_shop_no,
                            })
                            wx.setStorageSync('shopno', data.Staff.c_shop_no)
                            wx.setStorageSync('shopnoMain', data.Staff.c_shop_no)
                            app.shopno = data.Staff.c_shop_no
                            app.shopnoMain = data.Staff.c_shop_no
                            this.GetStorInfo(data.Staff.c_shop_no, data.Staff.n_isservice);

                        } else {
                            _this.setData({
                                err: true,
                                errMessage: data.msg
                            })
                            _this.data.loginStatus = true;
                        }
                    }
                },
                fail() {
                    _this.data.loginStatus = true;
                    _this.setData({
                        pass: "active"
                    })
                }
            })
        }
    },

    GetStorInfo(ShopNo, isService) {
        var str = "ShopNo=" + ShopNo,
            md5 = util.md5(str),
            _this = this,
            data = {
                bNo: "1703",
                ShopNo: ShopNo,
                MD5: md5.toUpperCase()
            };
        wx.request({
            url: config.requestUrl,
            data,
            success: res => {
                var data = JSON.parse(res.data.replace(/\(|\)/g, ""));
                if (data.code === "100") {
                    if (data.Result.c_name.length > 16) {
                        data.Result.c_name = data.Result.c_name.substr(0, 16) + "..."
                    }
                    _this.setData({
                        shopInfo: data.Result,
                    });
                    wx.setStorage({
                        key: 'shop_data',
                        data: data.Result,
                        success: () => {
                            if (isService == 1) {
                                this.data.n_yjhid = data.Result.n_yjhid
                                this.routerLink();
                            } else {
                                wx.showToast({
                                    title: '登录成功',
                                    icon: "success",
                                    success() {
                                        wx.reLaunch({
                                            url: '/pages/index/index',
                                        })
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    },

    routerLink() {
        if (this.data.n_yjhid == 390) {
            wx.reLaunch({
                url: '/pages/staffManage/myAchievement/myAchievement',
            })
        } else if (this.data.n_yjhid == 316) {
            wx.reLaunch({
                url: '/pages/waiter/index/index',
            })
        } else {
            wx.reLaunch({
                url: '/pages/retail/retail-order/retail-order',
            })
        }
    }
})