let app = getApp()
module.exports = {
    prevPage,
    showModal,
    getShopInfo, 
    getTableInfo, 
    getTableDetail,
    getTableGoodsList,
    getConfig,
    getGoodsClassify,
    getGoodsList,
    getOrderStatus,
    getGoodDetail,
    getGoodAttr,
    getGoodsDetailAttr,
    getMemberLevel,
    getOpenid,
    getIsPay,
    getTabList,
    getTabInfo,
    openTab,
    orderFood,
}
function prevPage(num) {
    const pages = getCurrentPages();
    if (!num) {
        num = 2
    }
    return pages[pages.length - num]
}
function showModal(msg,callback){
    wx.showModal({
        title: '提示',
        content: msg,
        showCancel: false,
        success: ()=> {
            if(callback){callback()}
        }
    })
}
// 获取店铺信息
function getShopInfo(shopno,callback){
    const str = `ShopNo`
    let params = {
        bNo: '203'
    }
    if(shopno){params.ShopNo = shopno}
    app.apiGet(str, params, (res) => {
        if (callback) {callback(res)}
    })
}
// 获取桌牌号
function getTableInfo(shopno,tableId,callback) {
    const str = `N_CID&ShopNo`;
    let params = {
        bNo: 2100,
        N_CID: tableId,
    };
    if (shopno) {params.ShopNo = shopno}
    app.apiGet(str, params, (res) => {
        if (callback) {callback(res)}
    })
}
// 获取桌牌信息
function getTableDetail(that,tid, callback){
    var str = `tid&ShopNo`;
    var params = {
        bNo: 2220,
        tid: tid,
    };
    app.apiGet(str, params, (data) => {
        data.TableInfo.StoreCWF = data.StoreCWF ? data.StoreCWF : 0 //餐位费
        var stateClass = "state1";
        if (data.TableInfo.ZTSTATE == "空闲") {
            stateClass = "state1"
        } else if (data.TableInfo.ZTSTATE == "使用中") {
            stateClass = "state2"
        } else if (data.TableInfo.ZTSTATE == "待清理") {
            stateClass = "state3"
        } else if (data.TableInfo.ZTSTATE == "预结帐") {
            stateClass = "state4"
        }
        wx.setStorage({
            key: 'zt-info',
            data: data.TableInfo,
        })
        that.setData({
            ztInfo: data.TableInfo,
            stateClass: stateClass,
        })
        if (callback) { callback(data) }
    })
}
// 获取桌台价格等详细信息
function getTableGoodsList(tid){
    const str = `TID&ShopNo=${this.data.TableId}*${this.data.shopNo}`;
    let params = {
        bNo: 2212,
        TID: tid
    };
    app.apiGet(str, params, (res) => {
        console.log(res)
        if (callback) { callback(res) }
    })
}
// 429 正餐扫码点餐
function getConfig(callback) {
    const str = `Setlx&ShopNo`;
    let params = {
        bNo: 429,
        Setlx: 40
    }
    app.apiGet(str, params, (res) => {
        if (callback) { callback(res) }
    })
}
// 获取餐饮分类
function getGoodsClassify(callback) {
    let params = {
        bNo: 3100,
    }
    app.apiGet('', params, (res) => {
        if (callback) { callback(res) }
    })
}
// 获取商品列表
function getGoodsList(that,cid,callback) {
    const str = `ShopNo&pageindex&pagesize&CateId`;
    let params = {
        bNo: "4005",
        pageindex: that.data.pageindex,
        pagesize: app.pagesize,
        mebno: that.data.mebno || 0,
        CateId: cid,
    }
    let goodsLists = []
    app.apiGet(str, params, (res) => {
        console.log(res)
        if(res.code == '101'){
            wx.showToast({
                title: res.msg,
            })
        }
        if (res.dtResult){
            res.dtResult.map((item, i) => {
                item.listImg = item.C_MAINPICTURE
                item.C_MAINPICTURE = app.filePath + item.C_MAINPICTURE;
                item.N_NUM = 0;
                item.cartNums = 0; //加入购物车数量
            })
            if (res.dtResult.length < app.pagesize) {
                that.data.hasEndPg = true
            }
            goodsLists = res.dtResult
            if (that.data.pageindex > 1) {
                goodsLists = that.data.goodsList.concat(res.dtResult)
            }
            if (that.data.resultList) {//加入购物车缓存
                that.data.resultList.map((item, i) => {
                    goodsLists.map((items, j) => {
                        if (item.ID == items.ID) {
                            items.cartNums = item.cartNums
                        }
                    })
                })
            }
        }
        that.setData({
            goodsList: goodsLists,
            hasEndPg: that.data.hasEndPg,
            hasList: true,
        })
        if (callback) { callback(res) }
    })
}
// 获取订单状态
function getOrderStatus(that, orderNo,callback){
    let str = `ShopNo&OrderNo`
    let params = {
        bNo: 4004,
        OrderNo: orderNo,
    }
    app.apiGet(str, params, (res) => {
        let GZT = false // 是否有待确认菜品   res.Examine //审核状态 0未审1已审2拒绝 
        if (res.gbase) {
            GZT = res.gbase.some((item,i) => {
                return item.GZT == 3 //待确认的菜品
            })
        }
        wx.setStorage({
            key: 'scan-order-status',
            data: GZT ? 0 : res.Examine,
        })
        that.setData({
            orderStatus: GZT ? 0 : res.Examine,
            gbase: res.gbase
        })
        if (callback) { callback(res) }
    })
}
// 获取商品详情
function getGoodDetail(that,callback){
    const str = `gid&ShopNo`;
    let params = {
        bNo: 701,
        gid: that.data.gid, //商品id
        isGoodsImg: 1,
        mebno: that.data.mebno || '',
        isactive: 1,
        isgroupshop: that.data.isgroupshop || "",
        N_LPTCL: that.data.N_LPTC || "",
    };
    app.apiGet(str, params, (res) => {
        if (callback) { callback(res) }
    })
}
// 切换商品规格
function getGoodsDetailAttr(that, callback) {
    const str = `Spv1&GbId&ShopNo`;
    let params = {
        bNo: 4009,
        GbId: that.data.GbId, //goods[0].N_GB_ID主商品编号
        goodsattr: that.data.goodsattr || "",
        Spv1: that.data.Spv1 || 0,
        Spv2: that.data.Spv2 || 0,
        Spv3: that.data.Spv3 || 0,
        mebno: that.data.mebno,
    };
    app.apiGet(str, params, (res) => {
        console.log(res)
        if (callback) { callback(res) }
    })
}
//备注、做法、加料
function getGoodAttr(GoodsID){
    let str = `GoodsID&ShopNo`;
    var params = {
        bNo: 2210,
        GoodsID: GoodsID //N_GB_ID
    };
    app.apiGet(str, params, (res) => {
        console.log(res)
        if (callback) { callback(res) }
    })
}
// 获取会员等级
function getMemberLevel(that,callback) {
    let str = `ShopNo`;
    var params = {
        bNo: '1709g',
    };
    app.apiGet(str, params, (res) => {
        let memberLevelArr = []
        res.data.map((item,i) => {
            memberLevelArr.push(item.C_MG_NAME)
        })
        that.setData({
            memberLevel: res.data,
            memberLevelArr: memberLevelArr
        })
        if (callback) { callback(res) }
    })
}
// 获取openid
function getOpenid(){
    wx.login({
        success: function (_res) {
            console.log(_res)
            if (_res.code) {
                let params = {
                    bNo: '1515',
                    code: _res.code,
                }
                app.apiGet('', params, (res) => {
                    app.openid = res.openid
                    console.log(res)
                },(fail) => {
                    console.log(fail)
                },(com) => {
                    console.log(com)
                })
            } else {
                wx.showToast({
                    title: '登陆失败',
                    icon: 'none'
                })
            }
        },
        fail: function(fail){
            console.log(fail)
        }
    });
}
// 支付接口
function getPayInfo(orderNo, openid, merchantId) {
    var data = {
        bNo: 1501,
        openid: openid,
        orderno: orderNo,
        scene: 1, //使用场景，0：商户自有小程序，1：平台分账小程序， 不填，默认：0,
        type: 1,//1：微信，2：支付宝
    };
    app.apiGet(str, params, (res) => {
        if (callback) { callback(res) }
    }, (err) => {

    })
}
// 扫码点餐是否开启支付
function getIsPay(that){
    const str = `ShopNo`
    let params = {
        bNo: 4011,
    };
    app.apiGet(str, params, (res) => {
        that.setData({
            PaySet: res.PaySet, //支付状态 1开启 2关闭
        })
    }, (err) => {
        console.log(err)
    })
}
// 获取桌台列表
function getTabList(that,callback,comback){
    const str = `RID&State&ShopNo`;
    const params = {
        bNo: '2200b',
        RID: that.data.rid || 0,
        State: that.data.state || 10, //状态（0未使用1已使用2已预订3打扫中 10, 全部） 
    };
    app.apiGet(str, params, (res) => {
        if (callback) { callback(res) }
    }, (err) => {
        console.log(err)
        showModal(err.msg)
    }, (com) => {
        if (comback) { comback() }
    })
} 
// 查询桌台信息
function getTabInfo(that,tid,callback){
    var str = `tid&ShopNo`;
    var params = {
        bNo: '2220b',
        tid: tid,
    };
    app.apiGet(str, params, (data) => {
        data.TableInfo.StoreCWF = data.StoreCWF ? data.StoreCWF : 0 //餐位费
        var stateClass = "state1";
        if (data.TableInfo.ZTSTATE == "空闲") {
            stateClass = "state1"
        } else if (data.TableInfo.ZTSTATE == "使用中") {
            stateClass = "state2"
        } else if (data.TableInfo.ZTSTATE == "待清理") {
            stateClass = "state3"
        } else if (data.TableInfo.ZTSTATE == "预结帐") {
            stateClass = "state4"
        }
        wx.setStorage({
            key: 'zt-info',
            data: data.TableInfo,
        })
        that.setData({
            ztInfo: data.TableInfo,
            stateClass: stateClass,
        })
        if (callback) { callback(data) }
    })
}
// 开桌
function openTab(that,callback,hanlder) {
    const str = `tids&Remark&Ponut&C_MB_NO&C_MB_PHONE&lcodes&lprice&wids&wname&CWF&ShopNo`
    let params = {
        bNo: '2203b',
        tids: that.data.tids, //桌台号
        Remark: that.data.remark || '', // 备注
        Ponut: that.data.num, // 人数
        C_MB_NO: that.data.mebno || '', // 
        C_MB_PHONE: that.data.phone || '', // 
        lcodes: that.data.lcodes || "", // 桌台标签
        lprice: that.data.lprice || 0,  // 桌台价格
        wids: that.data.wids || "", // 服务员id
        wname: that.data.wname || "", // 服务员名称
        CWF: that.data.cwf || 0, // 餐位费
    }
    app.apiGet(str, params, (res) => {
        if (res.code == '101') {
            console.log(res.msg)
            wx.showModal({
                title: '提示',
                content: res.msg,
                showCancel: false,
            })
        } else {
            if (callback) {
                callback(res)
            }
        }
    }, (err) => {
        console.log(err)
        wx.showModal({
            title: '提示',
            content: err.msg,
            showCancel: false,
            success: function(){
                wx.navigateBack()
            }
        })
    })
}
// 点菜、加菜、退菜
function orderFood(that, glists,sublist,callback,comback){
    const str = `ShopNo&mebNo&sublist&opid&OrderNo`;
    const params = {
        bNo: '4003b',
        opid: that.data.opid || 0,
        PeopleCount: that.data.PeopleCount,
        OrderNo: that.data.OrderNo,
        mebNo: that.data.mebno || '',
        glist: JSON.stringify(glists),
        sublist: JSON.stringify(sublist),
        Addstr: that.data.addStr || '',
        Upstr: "",
        // IsExamine: that.data.IsExamine,
        Delstr: that.data.Delstr || '',
        ReDelstr: "",
        TID: that.data.TID,
        SMDC: 1,
        action: that.data.action || 1, // 0下单 1 加菜 2 退菜 
    };
    app.apiPost(str, params, (res) => {
        console.log('res---------------')
        console.log(res)
        if (callback) { callback(res)}
    }, (err) => {
        console.log(err)
        wx.showModal({
            title: '提示',
            content: err.msg,
            showCancel: false,
        })
    },(com) => {
        if (comback) { comback()}
    })
}



